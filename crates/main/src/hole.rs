use std::fmt;
use itertools::Itertools;

use crate::point::*;
use crate::error::*;
use crate::internal::*;
use crate::mesh;
use mesh::HasFaces;
use mesh::HasVerts;
use mesh::Vert;
use mesh::VertPos;
use mesh::VertID;
use mesh::FaceVerts;

mod meta_slice;
use meta_slice::*;

mod error;
mod refhole;
mod side;
mod side_float;
mod side_full;
mod sidemap;
mod meta;
mod meta_float;
mod holesize;
mod reshape;
pub use error::*;
pub use refhole::*;
pub use side::*;
pub use side_float::*;
pub use side_full::*;
pub use sidemap::*;
pub use meta::*;
pub use meta_float::*;
pub use holesize::*;
pub use reshape::*;

#[cfg(test)]
mod test;


///
#[repr(transparent)]
pub struct Hole {
    points: [Point]
}
impl Hole {

    /// # Errors
    ///
    /// - WBErrorHole::InvalidLength:
    ///     `points` contains less then 3 Points.
    ///
    /// # Examples
    ///
    /// ```
    /// use world_builder::*;
    ///
    /// Hole::new([
    ///     Point::new(1.,0.,0.),
    ///     Point::new(2.,0.,0.),
    ///     Point::new(3.,0.,0.),
    ///     Point::new(4.,0.,0.),
    /// ].into_iter()).unwrap();
    ///
    /// Hole::new([
    ///     Point::new(1.,0.,0.),
    ///     Point::new(2.,0.,0.),
    /// ].into_iter()).expect_err("");
    /// ```
    pub fn new<I>(points: I) -> Result<Box<Self>, WBErrorHole>
    where I: Iterator<Item=Point> {
        Self::from_box(points.collect::<Box<[Point]>>())
    }

    ///
    pub fn from_box(points: Box<[Point]>) -> Result<Box<Self>, WBErrorHole> {
        if points.len() < 3 || points.len() > usize::from(HoleSize::MAX) -1 {
            return Err(WBErrorHole::InvalidLength(points))
        }
        unsafe {
            Ok(Self::from_box_unchecked(points))
        }
    }

    ///
    pub unsafe fn from_box_unchecked(points: Box<[Point]>) -> Box<Self> {
        Box::from_raw(Box::into_raw(points) as *mut Hole)
    }

    /// # Examples
    ///
    /// ```
    /// use world_builder::*;
    ///
    /// let hole = Hole::new([
    ///     Point::new(1.,0.,0.),
    ///     Point::new(2.,0.,0.),
    ///     Point::new(3.,0.,0.),
    ///     Point::new(4.,0.,0.),
    /// ].into_iter()).unwrap();
    ///
    /// assert!(hole.length() == HoleSize::new(4));
    /// ```
    pub fn length(&self) -> HoleSize {
        self.points.len().into()
    }

    /// # Examples
    ///
    /// ```
    /// use world_builder::*;
    ///
    /// let hole = Hole::new([
    ///     Point::new(0.,0.,0.),
    ///     Point::new(1.,0.,0.),
    ///     Point::new(1.,1.,0.),
    ///     Point::new(0.,1.,0.),
    /// ].into_iter()).unwrap();
    ///
    /// let direction = hole.normal();
    /// assert!(direction.normal() == [0.,0.,1.]);
    /// ```
    pub fn normal(&self) -> Direction {
        Direction::face_normal(self.points().iter())
    }

    /// # Examples
    ///
    /// ```
    /// use world_builder::*;
    ///
    /// let mut points = vec![
    ///     Point::new(1.,0.,0.),
    ///     Point::new(2.,0.,0.),
    ///     Point::new(3.,0.,0.),
    ///     Point::new(4.,0.,0.),
    /// ];
    ///
    /// let hole1 = Hole::new(points.clone().into_iter()).unwrap();
    /// let hole2 = Hole::new(points.clone().into_iter()).unwrap();
    ///
    /// points.push(Point::new(0.5,0.,0.));
    ///
    /// let hole3 = Hole::new(points.into_iter()).unwrap();
    ///
    /// assert!(hole2.duplicate(&hole1));
    /// assert!(!hole3.duplicate(&hole1));
    /// ```
    pub fn duplicate(&self, other: &Hole) -> bool {
        compare_iter_loops(
            self.points().iter(),
            other.points().iter()
        )
    }

    /// # Examples
    ///
    /// ```
    /// use world_builder::*;
    ///
    /// let mut points = vec![
    ///     Point::new(1.,0.,0.),
    ///     Point::new(2.,0.,0.),
    ///     Point::new(3.,0.,0.),
    ///     Point::new(4.,0.,0.),
    /// ];
    /// let mut points_r = points.clone();
    /// points_r.reverse();
    ///
    /// let hole1 = Hole::new(points.into_iter()).unwrap();
    /// let hole2 = Hole::new(points_r.into_iter()).unwrap();
    ///
    /// assert!(hole2.covers(&hole1));
    /// ```
    pub fn covers(&self, other: &Hole) -> bool {
        compare_iter_loops(
            self.points().iter(),
            other.points().iter().rev()
        )
    }

    /// # Examples
    ///
    /// ```
    /// use world_builder::*;
    ///
    /// let (p1,p2,p3,p4) = (
    ///     Point::new(1.,0.,0.),
    ///     Point::new(2.,0.,0.),
    ///     Point::new(3.,0.,0.),
    ///     Point::new(4.,0.,0.),
    /// );
    ///
    /// let hole1 = Hole::new([p1,p2.clone(),p3.clone()].into_iter()).unwrap();
    /// let hole2 = Hole::new([p2,p3,p4].into_iter()).unwrap();
    ///
    /// assert!(hole1.stacked(&hole2));
    /// assert!(!hole1.touching(&hole2));
    /// ```
    pub fn stacked(&self, other: &Hole) -> bool {
        compare_iter_pairs(
            self.points().iter(),
            other.points().iter()
        )
    }

    /// # Examples
    ///
    /// ```
    /// use world_builder::*;
    ///
    /// let (p1,p2,p3,p4) = (
    ///     Point::new(1.,0.,0.),
    ///     Point::new(2.,0.,0.),
    ///     Point::new(3.,0.,0.),
    ///     Point::new(4.,0.,0.),
    /// );
    ///
    /// let hole1 = Hole::new([p1,p2.clone(),p3.clone()].into_iter()).unwrap();
    /// let hole2 = Hole::new([p4,p3,p2].into_iter()).unwrap();
    ///
    /// assert!(!hole1.stacked(&hole2));
    /// assert!(hole1.touching(&hole2));
    /// ```
    pub fn touching(&self, other: &Hole) -> bool {
        compare_iter_pairs(
            self.points().iter(),
            other.points().iter().rev()
        )
    }

    ///
    pub fn reshape(&self) -> HoleReshape<&Self> {
        HoleReshape::new(self)
    }

    ///
    pub fn debug_mesh(&self) -> mesh::Result<mesh::Mesh<'static>> {
        mesh::IntoDebugMesh::into_debugmesh(self)
    }
}
impl fmt::Display for Hole {
    #[allow(unstable_name_collisions)]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut point_iter = self.points().into_iter();
        f.write_str("Hole(")?;
        point_iter.next().unwrap().fmt(f)?;
        for point in point_iter {
            f.write_str(", ")?;
            point.fmt(f)?;
        }
        f.write_str(")")
    }
}
impl fmt::Debug for Hole {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_tuple("Hole")
            .field(&(self as *const _))
            .field(&&self.points)
            .finish()
    }
}
impl<'a> SideFull<'a, &'a Hole> for Hole {
    fn points(&'a self) -> &'a [Point] {
        &self.points
    }
    fn get_hole_ref(&'a self) -> &'a Hole {
        self
    }
}
impl PartialEq for Hole {
    fn eq(&self, other: &Self) -> bool {
        std::ptr::addr_eq(self as *const _, other as *const _)
    }
}
impl AsRef<Hole> for &Hole {
    fn as_ref(&self) -> &Hole {
        self
    }
}
impl<T> mesh::IntoPolygons<T> for &Hole 
where T: mesh::MesherVerts + mesh::MesherFaces,
      for<'a> <T as HasFaces<'a>>::Face: FaceVerts,
      for<'a> <T as HasVerts<'a>>::Vert: VertPos {
    fn poly(self, mut mesher: T) -> mesh::Result<()> {
        let verts = self.points.into_iter().map(|point| {
            mesher.vert().pos(point.pos()).finish()
        }).collect::<mesh::Result<Vec<VertID>>>()?.into_iter();
        mesher.polygon(verts)
    }
}
impl<'external> mesh::IntoDebugMesh<'external> for &Hole {
    fn msh<'mesh>(self, mut m: mesh::BaseMesher<'mesh, 'external>) -> mesh::Result<()> {
        let mesher = m.shader(&mesh::DebugShader::RED)?;
        mesh::IntoPolygons::poly(self, mesher)
    }
}



fn compare_iter_loops<A, B>(mut a: A, b: B) -> bool
where A: ExactSizeIterator,
      B: ExactSizeIterator + Clone,
      A::Item: PartialEq<B::Item> {
    if a.len() != b.len() {
        return false;
    }
    let first = &a.next().unwrap();
    let mut leftover = b.clone().skip_while(|o| first != o);
    if leftover.next().is_none() {
        return false;
    }
    for (a, b) in a.zip(leftover.chain(b)) {
        if a != b {return false;}
    };
    true
}

fn compare_iter_pairs<A, B>(a: A, b: B) -> bool
where A: ExactSizeIterator + Clone,
      B: ExactSizeIterator + Clone,
      A::Item: PartialEq<B::Item> + Clone,
      B::Item: Clone {
    let firsts = a
        .circular_tuple_windows::<(_,_)>()
        .collect_vec();
    b
        .circular_tuple_windows::<(_,_)>()
        .any(|(b1, b2)| firsts.iter().any(|(a1, a2)| *a1 == b1 && *a2 == b2))
}
