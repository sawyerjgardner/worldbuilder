use std::fmt::Debug;


///
pub trait WBError: Debug {}
impl<T> From<T> for WBErrorAny
where T: WBError + 'static {
    fn from(value: T) -> Self {
        Self::new(Box::new(value))
    }
}


///
pub struct WBErrorAny{
    data: Box<dyn Debug>
}
impl WBErrorAny {
    pub fn new(string: Box<dyn Debug>) -> WBErrorAny {
        WBErrorAny{data: string}
    }
}
impl Debug for WBErrorAny {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.data.fmt(f)
    }
}
