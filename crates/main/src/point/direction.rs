use std::borrow::Borrow;
use itertools::Itertools;
use glam::Vec3;
use world_builder_macros::preprocess_pymethods;

use super::*;


#[cfg_attr(feature="pyo3", pyo3::pyclass(frozen))]
#[derive(Clone)]
pub struct Direction {
    data: Vec3
}
#[preprocess_pymethods(feature="pyo3")]
impl Direction {

    /// # Examples
    ///
    /// ```
    /// use world_builder::Direction;
    ///
    /// Direction::new(1.,0.,0.);
    /// Direction::new(2.,1.,0.);
    /// Direction::new(0.,0.,0.);
    /// ```
    #[new]
    pub fn new(x:f32, y:f32, z:f32) -> Self {
        Self::from_vec(Vec3::new(x,y,z))
    }

    /// # Examples
    ///
    /// ```
    /// use world_builder::Direction;
    /// use world_builder::Point;
    ///
    /// let p1 = Point::new(1.,5.,6.);
    /// let p2 = Point::new(4.,8.,6.);
    ///
    /// let dir = Direction::pointing(&p1,&p2);
    ///
    /// # let norm = dir.normal();
    /// # let sqr2 = f32::sqrt(2.)/2.;
    /// # assert!((
    /// #     (norm[0]-sqr2).abs() < 0.001 &&
    /// #     (norm[1]-sqr2).abs() < 0.001 &&
    /// #     norm[2].abs()        < 0.001
    /// # ), "normal out of range: {:?}", norm);
    /// ```
    #[staticmethod]
    pub fn pointing(from: &Point, to: &Point) -> Self {
        Self::from_vec(*to.pos_vec() -*from.pos_vec())
    }

    /// # Examples
    ///
    /// ```
    /// use world_builder::Direction;
    ///
    /// let sqr2 = f32::sqrt(2.)/2.;
    ///
    /// assert!(Direction::new(1.,0.,0.).normal() == [1.,0.,0.]);
    /// assert!(Direction::new(4.,0.,0.).normal() == [1.,0.,0.]);
    /// assert!(Direction::new(1.,1.,0.).normal() == [sqr2,sqr2,0.]);
    /// assert!(Direction::new(4.,0.,-4.).normal() == [sqr2,0.,-sqr2]);
    /// assert!(Direction::new(-0.5,0.,0.5).normal() == [-sqr2,0.,sqr2]);
    /// ```
    pub fn normal(&self) -> [f32;3] {
        self.data.into()
    }

    /// # Examples
    ///
    /// ```
    /// use world_builder::Direction;
    ///
    /// let a = Direction::new(1.,0.,0.);
    /// let b = Direction::new(0.,1.,0.);
    ///
    /// assert!(a.cross(&b).normal() == [0.,0.,1.]);
    /// assert!(b.cross(&a).normal() == [0.,0.,-1.]);
    /// ```
    pub fn cross(&self, other: &Self) -> Self {
        Self::from_vec(self.data.cross(other.data))
    }

    #[staticmethod]
    #[pyo3(name="average")]
    fn py_average(dirs: Vec<Self>) -> Self {
        Self::average(dirs.into_iter())
    }

    #[staticmethod]
    #[pyo3(name="face_normal")]
    fn py_face_normal(points: Vec<Point>) -> Self {
        Self::face_normal(points.iter())
    }
}
impl Direction {
    /// # Examples
    ///
    /// ```
    /// use world_builder::Direction;
    ///
    /// let dirs = [
    ///     Direction::new( 1., 1., 1.),
    ///     Direction::new( 4., 4.,-4.),
    ///     Direction::new(-3., 3.,-3.),
    ///     Direction::new(-1., 1., 1.),
    /// ].into_iter();
    ///
    /// let avrg = Direction::average(dirs).normal();
    ///
    /// assert!((
    ///     avrg[0].abs() < 0.001 &&
    ///     avrg[1]       > 0.999 &&
    ///     avrg[2].abs() < 0.001
    /// ), "average out of range: {:?}", avrg);
    /// ```
    pub fn average<'a, I>(dir_iter: I) -> Self
    where I: Iterator,
          I::Item: Borrow<Direction> {
        Self::from_vec(dir_iter.map(|o| o.borrow().data).sum())
    }

    /// # Examples
    ///
    /// ```
    /// use world_builder::Direction;
    /// use world_builder::Point;
    ///
    /// let points = [
    ///     Point::new(0.,0.,0.),
    ///     Point::new(0.,1.,0.),
    ///     Point::new(1.,1.,0.),
    ///     Point::new(1.,0.,0.),
    /// ];
    ///
    /// let normal = Direction::face_normal(points.iter()).normal();
    ///
    /// assert!(normal == [0.,0.,-1.], "Incorrect face normal: {:?}", normal);
    /// ```
    pub fn face_normal<'a, T>(mut points: T) -> Self
    where T: Iterator<Item=&'a Point> {
        let mut total = Vec3::ZERO;
        let (point_first, mut point_back) = points.next_tuple().unwrap();
        let dir_first = Direction::pointing(point_first, point_back);
        let mut dir_back = dir_first.clone();
        for point_next in points {
            let dir_next = Direction::pointing(point_back, point_next);
            total += dir_next.data.cross(-dir_back.data);
            dir_back = dir_next;
            point_back = point_next;
        }
        let dir_around = Direction::pointing(point_back, point_first);
        total += dir_around.data.cross(-dir_back.data);
        total += dir_first.data.cross(-dir_around.data);
        Self::from_vec(total)
    }

    fn from_vec(dir: Vec3) -> Self {
        Self {data: dir.normalize()}
    }
}
impl fmt::Display for Direction {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_list()
            .entries(&self.normal())
            .finish()
    }
}
impl fmt::Debug for Direction {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_tuple("Direction")
            .field(&self.normal())
            .finish()
    }
}
