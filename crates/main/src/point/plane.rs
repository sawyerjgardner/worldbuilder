use itertools::enumerate;

use super::*;


#[derive(Debug)]
pub struct Plane {
    normal: Direction,
    offset: f32
}
impl Plane {

    /// # Examples
    ///
    /// ```
    /// use world_builder::Plane;
    /// use world_builder::Direction;
    ///
    /// Plane::new(Direction::new(1.,0.,0.), 1.);
    /// Plane::new(Direction::new(1.,-1.,0.), 0.);
    /// Plane::new(Direction::new(-1.,0.,-1.), 100.5);
    /// ```
    pub fn new(normal: Direction, offset: f32) -> Self {
        Self {normal, offset}
    }

    /// # Examples
    ///
    /// ```
    /// use world_builder::Plane;
    /// use world_builder::Direction;
    /// use world_builder::Point;
    ///
    /// Plane::from_point(Direction::new(1.,0.,0.), &Point::new(1.,100.,0.));
    /// Plane::from_point(Direction::new(1.,-1.,0.), &Point::new(0.,0.,0.));
    /// Plane::from_point(Direction::new(0.,0.,1.), &Point::new(-5.,0.,-3.));
    /// ```
    pub fn from_point(normal: Direction, point: &Point) -> Self {
        Self {
            offset: point.pos_vec().dot(normal.normal().into()),
            normal,
        }
    }

    /// # Examples
    ///
    /// ```
    /// use world_builder::Plane;
    /// use world_builder::Direction;
    /// use world_builder::Point;
    ///
    /// let plane = Plane::new(Direction::new(1.,0.,0.),   1.);
    /// assert_eq!(plane.distance(&Point::new(0.,0.,0.)), -1.);
    /// assert_eq!(plane.distance(&Point::new(1.,0.,0.)),  0.);
    /// assert_eq!(plane.distance(&Point::new(5.,3.,-6.)), 4.);
    ///
    /// let plane = Plane::new(Direction::new(0.,0.,-1.), 1.);
    /// assert_eq!(plane.distance(&Point::new(0.,0.,0.)), -1.);
    /// assert_eq!(plane.distance(&Point::new(1.,0.,0.)), -1.);
    /// assert_eq!(plane.distance(&Point::new(1.,0.,1.)), -2.);
    /// assert_eq!(plane.distance(&Point::new(5.,3.,-6.)), 5.);
    ///
    /// let plane = Plane::new(Direction::new(0.,0.,-2.), -4.5);
    /// assert_eq!(plane.distance(&Point::new(0.,0.,0.)),  4.5);
    /// assert_eq!(plane.distance(&Point::new(1.,0.,0.)),  4.5);
    /// assert_eq!(plane.distance(&Point::new(1.,0.,1.)),  3.5);
    /// assert_eq!(plane.distance(&Point::new(5.,3.,-6.)), 10.5);
    /// ```
    pub fn distance(&self, point: &Point) -> f32 {
        point.pos_vec().dot(self.normal.normal().into()) -self.offset
    }

    /// # Examples
    ///
    /// ```
    /// use world_builder::Plane;
    /// use world_builder::Direction;
    /// use world_builder::Point;
    ///
    /// let plane = Plane::new(Direction::new(1.,0.,0.),   2.);
    /// let new_p = &Point::new;
    /// assert_eq!(plane.collision(&new_p(3.,0.,0.), &new_p(1.,0.,0.)), Some(0.5));
    /// assert_eq!(plane.collision(&new_p(3.,0.,0.), &new_p(1.,0.,0.)), Some(0.5));
    /// assert_eq!(plane.collision(&new_p(1.,0.,0.), &new_p(3.,0.,0.)), None);
    /// assert_eq!(plane.collision(&new_p(3.,0.,0.), &new_p(2.,0.,0.)), None);
    /// assert_eq!(plane.collision(&new_p(5.,0.,0.), &new_p(1.,0.,0.)), Some(0.75));
    /// assert_eq!(plane.collision(&new_p(8.,0.,0.), &new_p(9.,0.,0.)), None);
    /// ```
    pub fn collision(&self, p1: &Point, p2: &Point) -> Option<f32> {
        let v1 = self.distance(p1);
        if v1 >= 0. {
            let v2 = self.distance(p2);
            if v2 < 0. {return Some(v1/(v1-v2))
            }
        }
        None
    }

    ///
    pub fn collision_iter<'a, T>(&self, points: T) -> Option<(usize, f32)>
    where T: Iterator<Item=&'a Point> {
        let mut points = points.map(|o| self.distance(o)).skip_while(|o| *o < 0.);
        if let Some(mut last_v) = points.next() {
            for (i, v) in enumerate(points) {
                if v < 0. {
                    return Some((i, last_v/(last_v-v)))
                }
                last_v = v;
            }
        }
        None
    }
}
