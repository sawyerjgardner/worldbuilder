use super::*;
pub use world_builder_macros::RefHole;


///
pub trait RefHole {

    ///
    type HoleT: AsRef<Hole>;

    ///
    type Unref;

    ///
    fn as_unref(&self) -> &Self::Unref;

    ///
    unsafe fn as_unref_mut(&mut self) -> &mut Self::Unref;

    ///
    fn into_unref(self) -> Self::Unref;

    ///
    fn as_hole(&self) -> &Self::HoleT;

    ///
    unsafe fn as_hole_mut(&mut self) -> &mut Self::HoleT;

    ///
    fn into_hole(self) -> Self::HoleT;

    /// # Safety:
    ///
    /// Unrefed data should only be paired with the hole it was built to be
    /// paired with. pairing data with the wrong hole causes undefined behavior.
    unsafe fn from_unref(hole: Self::HoleT, unref: Self::Unref) -> Self;

    ///
    fn unref(&self) -> Self::Unref
    where Self::Unref: Clone {
        self.as_unref().clone()
    }

    ///
    fn hole(&self) -> Self::HoleT
    where Self::HoleT: Clone {
        self.as_hole().clone()
    }

    /// # Errors:
    ///
    /// - WBErrorHole::NotEqual:
    ///     `hole` dose not refer to the same hole data as `self`.
    fn reref_hole<T, H2>(&self, hole: H2) -> Result<T, WBErrorHole>
    where Self: Sized,
          Self::Unref: Clone,
          T: RefHole<HoleT=H2,Unref=Self::Unref>,
          H2: AsRef<Hole> {
        WBErrorHole::test_holes_equal(self.as_hole().as_ref(), hole.as_ref())?;
        Ok(unsafe {T::from_unref(hole, self.unref())})
    }

    /// # Errors:
    ///
    /// - WBErrorHole::NotEqual:
    ///     `hole` dose not refer to the same hole data as `self`.
    fn into_reref_hole<T, H2>(self, hole: H2) -> Result<T, WBErrorHole>
    where Self: Sized,
          T: RefHole<HoleT=H2,Unref=Self::Unref>,
          H2: AsRef<Hole> {
        WBErrorHole::test_holes_equal(self.as_hole().as_ref(), hole.as_ref())?;
        Ok(unsafe {T::from_unref(hole, self.into_unref())})
    }


    /// # Safety:
    ///
    /// Unrefed data should only be paired with the hole it was built to be
    /// paired with. pairing data with the wrong hole causes undefined behavior.
    unsafe fn reref_hole_unchecked<T, H2>(&self, hole: H2) -> T
    where Self: Sized,
          Self::Unref: Clone,
          T: RefHole<HoleT=H2,Unref=Self::Unref>,
          H2: AsRef<Hole> {
        #[cfg(debug_assertions)]
        WBErrorHole::test_holes_equal(self.as_hole().as_ref(), hole.as_ref()).unwrap();
        T::from_unref(hole, self.unref())
    }

    /// # Safety:
    ///
    /// Unrefed data should only be paired with the hole it was built to be
    /// paired with. pairing data with the wrong hole causes undefined behavior.
    unsafe fn into_reref_hole_unchecked<T, H2>(self, hole: H2) -> T
    where Self: Sized,
          T: RefHole<HoleT=H2,Unref=Self::Unref>,
          H2: AsRef<Hole> {
        #[cfg(debug_assertions)]
        WBErrorHole::test_holes_equal(self.as_hole().as_ref(), hole.as_ref()).unwrap();
        T::from_unref(hole, self.into_unref())
    }

    ///
    fn reref_hole_base<'a, T>(&'a self) -> T
    where Self: Sized,
          Self::Unref: Clone,
          T: RefHole<HoleT=&'a Hole,Unref=Self::Unref> {
        unsafe {
            self.reref_hole_unchecked(self.as_hole().as_ref())
        }
    }
}
