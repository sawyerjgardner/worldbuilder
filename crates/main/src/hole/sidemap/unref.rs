use super::*;

use std::collections::TryReserveError;


///
#[derive(Clone,Default,PartialEq,Eq)]
pub struct SideMapUnref<T> {
    pub(super) vec: Vec<(SideUnref, T)>
}
impl<T> SideMapUnref<T> {
    ///
    pub fn new() -> Self {
        Self {vec: Vec::new()}
    }

    ///
    pub fn new_capacity(capacity: HoleSize) -> Self {
        Self {vec: Vec::with_capacity(capacity.into())}
    }

    ///
    pub fn len(&self) -> HoleSize {
        self.vec.len().into()
    }

    ///
    pub fn is_empty(&self) -> bool {
        self.vec.is_empty()
    }

    ///
    pub unsafe fn is_full(&self, hole: &Hole) -> bool {
        let mut iter = self.sides();
        let Some(side_first) = iter.next() else {
            return false
        };
        let mut side_last = side_first;
        for side_next in iter {
            if side_last.start() +side_last.length() != side_next.start() {
                return false
            }
            side_last = side_next;
        }
        if side_last.end(hole) != side_first.start() {
            return false
        }
        true
    }

    ///
    pub fn capacity(&self) -> HoleSize {
        let cap = self.vec.capacity();
        if cap >= usize::from(HoleSize::MAX) {
            return HoleSize::MAX
        }
        cap.into()
    }

    ///
    pub fn reserve(&mut self, additional: HoleSize) {
        self.vec.reserve(additional.into())
    }

    ///
    pub fn try_reserve(&mut self, additional: HoleSize) -> Result<(), TryReserveError> {
        self.vec.try_reserve(additional.into())
    }

    ///
    pub fn shrink_to(&mut self, min_capacity: HoleSize) {
        self.vec.shrink_to(min_capacity.into())
    }

    ///
    pub fn shrink_to_fit(&mut self) {
        self.vec.shrink_to_fit()
    }

    ///
    pub unsafe fn insert(&mut self, hole: &Hole, side: SideUnref, value: T) -> Option<HoleSize> {
        if self.overlapping(hole, side) {
            return None
        }
        let index = self.position(side.start());
        self.vec.insert(index, (side, value));
        Some(index.into())
    }

    ///
    pub fn remove(&mut self, index: HoleSize) -> Option<(SideUnref, T)> {
        if index < self.len() {
            return Some(self.vec.remove(index.into()))
        }
        None
    }

    ///
    pub fn clear(&mut self) {
        self.vec.clear()
    }

    ///
    pub fn pop(&mut self) -> Option<(SideUnref, T)> {
        self.vec.pop()
    }

    ///
    pub fn pop_value(&mut self) -> Option<T> {
        self.vec.pop().map(|(_, value)| value)
    }

    ///
    pub fn side_value(&self, index: HoleSize) -> Option<(SideUnref, &T)> {
        self.vec.get(usize::from(index)).map(|(side, data)| (*side, data))
    }

    ///
    pub fn side_value_mut(&mut self, index: HoleSize) -> Option<(SideUnref, &mut T)> {
        self.vec.get_mut(usize::from(index)).map(|(side, data)| (*side, data))
    }

    ///
    pub fn side(&self, index: HoleSize) -> Option<SideUnref> {
        self.vec.get(usize::from(index)).map(|o| o.0)
    }

    ///
    pub fn value(&self, index: HoleSize) -> Option<&T> {
        self.vec.get(usize::from(index)).map(|o| &o.1)
    }

    ///
    pub fn value_mut(&mut self, index: HoleSize) -> Option<&mut T> {
        self.vec.get_mut(usize::from(index)).map(|o| &mut o.1)
    }

    ///
    pub unsafe fn gaps(&self, hole: &Hole) -> SideMapUnref<HoleSize> {
        let mut iter = self.sides();
        let Some(side_first) = iter.next() else {
            return SideMapUnref::new_full(hole, HoleSize::new(0))
        };
        let mut gaps = SideMapUnref::new();
        let mut side_last = side_first;
        let mut index = HoleSize::new(1);
        for side_next in iter {
            let last_end = side_last.start()+side_last.length();
            if last_end != side_next.start() {
                gaps.push_unchecked(
                    SideUnref::new(last_end, side_next.start() -last_end),
                    index
                )
            }
            index += HoleSize::new(1);
            side_last = side_next;
        }
        let last_end = side_last.end(hole);
        if last_end < side_first.start() {
            gaps.insert_at(
                HoleSize::new(0),
                SideUnref::new(last_end, side_first.start()-last_end),
                HoleSize::new(0)
            )
        } else if last_end > side_first.start() {
            let length = (hole.length()-last_end) +side_first.start();
            gaps.push_unchecked(
                SideUnref::new(last_end, length),
                self.len()
            )
        }
        gaps
    }


    ///
    pub fn map<T2, F>(&self, mut func: F) -> SideMapUnref<T2>
    where F: FnMut(&T) -> T2 {
        SideMapUnref {
            vec: self.vec.iter().map(|(side, data)| {
                (*side, func(data))
            }).collect()
        }
    }

    ///
    pub fn try_map<T2, F, E>(&self, mut func: F) -> Result<SideMapUnref<T2>, E>
    where F: FnMut(&T) -> Result<T2, E> {
        Ok(SideMapUnref {
            vec: self.vec.iter().map(|(side, data)| {
                Ok((*side, func(data)?))
            }).collect::<Result<_, E>>()?
        })
    }

    ///
    pub fn into_map<T2, F>(self, mut func: F) -> SideMapUnref<T2>
    where F: FnMut(T) -> T2 {
        SideMapUnref {
            vec: self.vec.into_iter().map(|(side, data)| {
                (side, func(data))
            }).collect()
        }
    }

    ///
    pub fn try_into_map<T2, F, E>(self, mut func: F) -> Result<SideMapUnref<T2>, E>
    where F: FnMut(T) -> Result<T2, E>{
        Ok(SideMapUnref {
            vec: self.vec.into_iter().map(|(side, data)| {
                Ok((side, func(data)?))
            }).collect::<Result<_, E>>()?
        })
    }


    ///
    pub fn iter(&self) -> SideMapUnrefIter<T> {
        SideMapUnrefIter {iter: self.vec.iter()}
    }

    ///
    pub fn iter_mut(&mut self) -> SideMapUnrefIterMut<T> {
        SideMapUnrefIterMut {iter: self.vec.iter_mut()}
    }

    ///
    pub fn sides(&self) -> SideMapUnrefSides<T> {
        SideMapUnrefSides {iter: self.vec.iter()}
    }

    ///
    pub fn values(&self) -> SideMapValues<T> {
        SideMapValues {iter: self.vec.iter()}
    }

    ///
    pub fn values_mut(&mut self) -> SideMapValuesMut<T> {
        SideMapValuesMut {iter: self.vec.iter_mut()}
    }

    ///
    pub fn into_values(self) -> IntoSideMapValues<T> {
        IntoSideMapValues {iter: self.vec.into_iter()}
    }

    ///
    pub fn into_sides(self) -> IntoSideMapUnrefSides<T> {
        IntoSideMapUnrefSides {iter: self.vec.into_iter()}
    }

    ///
    pub unsafe fn insert_unchecked(&mut self, side: SideUnref, value: T) -> HoleSize {
        #[cfg(debug_assertions)]
        if self.overlapping_debug(side) {
            panic!("SideMap insert overlapping Side! {:?}", side)
        }
        let index = self.position(side.start());
        self.vec.insert(index, (side, value));
        index.into()
    }

    ///
    pub unsafe fn remove_unchecked(&mut self, index: HoleSize) -> (SideUnref, T) {
        self.remove(index).unwrap()
    }

    ///
    pub unsafe fn push_unchecked(&mut self, side: SideUnref, value: T) {
        #[cfg(debug_assertions)]
        if self.overlapping_debug(side) {
            panic!("SideMap push overlapping Side! {:?}", side)
        }
        #[cfg(debug_assertions)]
        if self.position(side.start()) != usize::from(self.len()) {
            panic!("SideMap push out of order! {:?}", side)
        }
        self.vec.push((side, value))
    }

    ///
    pub unsafe fn side_value_unchecked(&self, index: HoleSize) -> (SideUnref, &T) {
        let (side, data) = self.vec.get_unchecked(usize::from(index));
        (*side, data)
    }

    ///
    pub unsafe fn side_value_unchecked_mut(&mut self, index: HoleSize) -> (SideUnref, &mut T) {
        let (side, data) = self.vec.get_unchecked_mut(usize::from(index));
        (*side, data)
    }

    ///
    pub unsafe fn side_unchecked(&self, index: HoleSize) -> SideUnref {
        self.vec.get_unchecked(usize::from(index)).0
    }

    ///
    pub unsafe fn value_unchecked(&self, index: HoleSize) -> &T {
        &self.vec.get_unchecked(usize::from(index)).1
    }

    ///
    pub unsafe fn value_unchecked_mut(&mut self, index: HoleSize) -> &mut T {
        &mut self.vec.get_unchecked_mut(usize::from(index)).1
    }


    ///
    pub unsafe fn into_meta(self, hole: &Hole) -> HoleMetaUnref<Option<T>> {
        let mut gaps = self.gaps(hole);
        let mut positions = self.into_iter().map(|(side, data)| {
            (side.start(), Some(data))
        }).collect_vec();
        while let Some((side, index)) = gaps.pop() {
            positions.insert(index.into(), (side.start(), None))
        }
        HoleMetaUnref::from_positions_unchecked(positions.into_iter())
    }

    ///
    pub unsafe fn into_meta_full(self, hole: &Hole) -> HoleMetaUnref<T> {
        if !self.is_full(hole) {
            panic!("SideMap not full")
        }
        HoleMetaUnref::from_positions_unchecked(
            self.into_iter().map(|(side, data)| {
                (side.start(), data)
            })
        )
    }

    ///
    pub unsafe fn into_meta_full_unchecked(self, hole: &Hole) -> HoleMetaUnref<T> {
        #[cfg(debug_assertions)]
        if !self.is_full(hole) {
            panic!("SideMap not full")
        }
        HoleMetaUnref::from_positions_unchecked(
            self.into_iter().map(|(side, data)| {
                (side.start(), data)
            })
        )
    }

    fn insert_at(&mut self, index: HoleSize, side: SideUnref, value: T) {
        #[cfg(debug_assertions)]
        if self.position(side.start()) != usize::from(index) {
            panic!(
                "SideMap insert_at out of order! {:?} {} ({})",
                side,
                index,
                self.position(side.start()),
            )
        }
        #[cfg(debug_assertions)]
        if self.overlapping_debug(side) {
            panic!("SideMap insert_at overlapping Side! {:?}", side)
        }
        self.vec.insert(index.into(), (side, value))
    }

    fn new_full(hole: &Hole, value: T) -> Self {
        Self {vec: vec![(SideUnref::new(HoleSize::new(0), hole.length()), value)]}
    }

    fn overlapping(&self, hole: &Hole, side: SideUnref) -> bool {
        for other_side in self.sides() {
            if unsafe {other_side.overlapping(&side, hole)} {
                return true
            }
        }
        false
    }

    fn overlapping_debug(&self, side: SideUnref) -> bool {
        for other_side in self.sides() {
            if other_side.overlapping_debug(&side) {
                return true
            }
        }
        false
    }

    fn position(&self, start: HoleSize) -> usize {
        self.sides().position(|o| {
            o.start() > start
        }).unwrap_or(self.vec.len())
    }
}
impl<T> fmt::Debug for SideMapUnref<T>
where T: fmt::Debug {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str("SideMapUnref {")?;
        for i in 0..self.len().into() {
            let (side, value) = self.side_value(i.into()).unwrap();
            side.fmt(f)?;
            f.write_str(": ")?;
            value.fmt(f)?;
            if i != usize::from(self.len()) -1 {
                f.write_str(", ")?;
            }
        }
        f.write_str(" }")
    }
}
impl<T> IntoIterator for SideMapUnref<T> {
    type IntoIter = IntoSideMapUnrefIter<T>;
    type Item = (SideUnref, T);

    fn into_iter(self) -> Self::IntoIter {
        IntoSideMapUnrefIter {iter: self.vec.into_iter()}
    }
}
impl<'a, T> IntoIterator for &'a SideMapUnref<T> {
    type IntoIter = SideMapUnrefIter<'a, T>;
    type Item = (SideUnref, &'a T);

    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}
impl<'a, T> IntoIterator for &'a mut SideMapUnref<T> {
    type IntoIter = SideMapUnrefIterMut<'a, T>;
    type Item = (SideUnref, &'a mut T);

    fn into_iter(self) -> Self::IntoIter {
        self.iter_mut()
    }
}
