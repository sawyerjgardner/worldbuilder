use super::*;
use std::slice::*;
use std::vec::IntoIter;


///
pub struct SideMapIter<'a, H, T>
where H: AsRef<Hole> + Clone {
    pub(super) hole: &'a H,
    pub(super) iter: Iter<'a, (SideUnref, T)>
}
impl<'a, H, T> Iterator for SideMapIter<'a, H, T>
where H: AsRef<Hole> + Clone {
    type Item = (Side<H>, &'a T);
    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next().map(|(side, data)| {
            (unsafe {Side::from_unref(self.hole.clone(), *side)}, data)
        })
    }
}


///
pub struct SideMapIterMut<'a, H, T>
where H: AsRef<Hole> + Clone {
    pub(super) hole: &'a H,
    pub(super) iter: IterMut<'a, (SideUnref, T)>
}
impl<'a, H, T> Iterator for SideMapIterMut<'a, H, T>
where H: AsRef<Hole> + Clone {
    type Item = (Side<H>, &'a mut T);
    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next().map(|(side, data)| {
            (unsafe {Side::from_unref(self.hole.clone(), *side)}, data)
        })
    }
}


///
pub struct SideMapSides<'a, H, T>
where H: AsRef<Hole> + Clone {
    pub(super) hole: &'a H,
    pub(super) iter: Iter<'a, (SideUnref, T)>
}
impl<H, T> Iterator for SideMapSides<'_, H, T>
where H: AsRef<Hole> + Clone {
    type Item = Side<H>;
    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next().map(|(side, _data)| {
            unsafe {Side::from_unref(self.hole.clone(), *side)}
        })
    }
}


///
pub struct SideMapValues<'a, T> {
    pub(super) iter: Iter<'a, (SideUnref, T)>
}
impl<'a, T> Iterator for SideMapValues<'a, T> {
    type Item = &'a T;
    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next().map(|(_side, data)| data)
    }
}


///
pub struct SideMapValuesMut<'a, T> {
    pub(super) iter: IterMut<'a, (SideUnref, T)>
}
impl<'a, T> Iterator for SideMapValuesMut<'a, T> {
    type Item = &'a mut T;
    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next().map(|(_side, data)| data)
    }
}


///
pub struct IntoSideMapIter<H, T>
where H: AsRef<Hole> + Clone {
    pub(super) hole: H,
    pub(super) iter: IntoIter<(SideUnref, T)>
}
impl<H, T> Iterator for IntoSideMapIter<H, T>
where H: AsRef<Hole> + Clone {
    type Item = (Side<H>, T);
    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next().map(|(side, data)| {
            (unsafe {Side::from_unref(self.hole.clone(), side)}, data)
        })
    }
}


///
pub struct IntoSideMapSides<H, T>
where H: AsRef<Hole> + Clone {
    pub(super) hole: H,
    pub(super) iter: IntoIter<(SideUnref, T)>
}
impl<H, T> Iterator for IntoSideMapSides<H, T>
where H: AsRef<Hole> + Clone {
    type Item = Side<H>;
    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next().map(|(side, _data)| {
            unsafe {Side::from_unref(self.hole.clone(), side)}
        })
    }
}


///
pub struct IntoSideMapValues<T> {
    pub(super) iter: IntoIter<(SideUnref, T)>
}
impl<'a, T> Iterator for IntoSideMapValues<T> {
    type Item = T;
    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next().map(|o| o.1)
    }
}



///
pub struct SideMapUnrefIter<'a, T> {
    pub(super) iter: Iter<'a, (SideUnref, T)>
}
impl<'a, T> Iterator for SideMapUnrefIter<'a, T> {
    type Item = (SideUnref, &'a T);
    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next().map(|(side, data)| (side.clone(), data))
    }
}


///
pub struct SideMapUnrefIterMut<'a, T> {
    pub(super) iter: IterMut<'a, (SideUnref, T)>
}
impl<'a, T> Iterator for SideMapUnrefIterMut<'a, T> {
    type Item = (SideUnref, &'a mut T);
    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next().map(|(side, data)| (side.clone(), data))
    }
}


///
pub struct SideMapUnrefSides<'a, T> {
    pub(super) iter: Iter<'a, (SideUnref, T)>
}
impl<'a, T> Iterator for SideMapUnrefSides<'a, T> {
    type Item = SideUnref;
    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next().map(|(side, _data)| side.clone())
    }
}


///
pub struct IntoSideMapUnrefIter<T> {
    pub(super) iter: IntoIter<(SideUnref, T)>
}
impl<'a, T> Iterator for IntoSideMapUnrefIter<T> {
    type Item = (SideUnref, T);
    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}


///
pub struct IntoSideMapUnrefSides<T> {
    pub(super) iter: IntoIter<(SideUnref, T)>
}
impl<'a, T> Iterator for IntoSideMapUnrefSides<T> {
    type Item = SideUnref;
    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next().map(|o| o.0)
    }
}
