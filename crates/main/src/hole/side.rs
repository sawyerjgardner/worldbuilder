use super::*;

mod unref;
pub(crate) mod side_iter;
pub use unref::*;
use side_iter::*;


///
#[derive(Clone,RefHole)]
pub struct Side<H>
where H: AsRef<Hole>{
    hole: H,
    unref: SideUnref,
}
impl<H> Side<H>
where H: AsRef<Hole> {

    /// # Errors
    ///
    /// - WBErrorHole::InvalidSide:
    ///     the values of `start` or `length` are larger then the length of `hole`
    ///     or `length` is 0.
    ///
    /// # Examples
    ///
    /// ```
    /// use world_builder::*;
    ///
    /// let hole = Hole::new([
    ///     Point::new(0.,0.,0.),
    ///     Point::new(1.,0.,0.),
    ///     Point::new(2.,0.,0.),
    /// ].into_iter()).unwrap();
    ///
    /// Side::new(&hole, HoleSize::new(0), HoleSize::new(2)).unwrap();
    /// Side::new(&hole, HoleSize::new(4), HoleSize::new(2)).expect_err("");
    /// Side::new(&hole, HoleSize::new(1), HoleSize::new(5)).expect_err("");
    /// ```
    pub fn new(hole: H,
               start: HoleSize,
               length: HoleSize)
               -> Result<Self, WBErrorHole> {
        let out = Self {hole, unref: SideUnref::new(start, length)};
        out.valid()?;
        Ok(out)
    }

    /// # Safety
    ///
    /// Calling this method is undefined behavior if `start` or `length` is
    /// out-of-bounds, even if the resulting side is never used.
    ///
    /// # Examples
    ///
    /// ```
    /// use world_builder::*;
    ///
    /// let hole = Hole::new([
    ///     Point::new(0.,0.,0.),
    ///     Point::new(1.,0.,0.),
    ///     Point::new(2.,0.,0.),
    /// ].into_iter()).unwrap();
    ///
    /// unsafe {
    ///     Side::new_unchecked(&hole, HoleSize::new(0), HoleSize::new(2));
    /// }
    /// ```
    pub unsafe fn new_unchecked(hole: H, start: HoleSize, length: HoleSize)
                                -> Self {
        let out = Self {hole, unref: SideUnref::new(start, length)};
        #[cfg(debug_assertions)]
        out.valid().unwrap();
        out
    }

    /// # Examples
    ///
    /// ```
    /// use world_builder::*;
    ///
    /// let hole = Hole::new([
    ///     Point::new(0.,0.,0.),
    ///     Point::new(1.,0.,0.),
    ///     Point::new(2.,0.,0.),
    /// ].into_iter()).unwrap();
    ///
    /// assert!(Side::new(&hole, HoleSize::new(0), HoleSize::new(2)).unwrap().start() == HoleSize::new(0));
    /// assert!(Side::new(&hole, HoleSize::new(1), HoleSize::new(3)).unwrap().start() == HoleSize::new(1));
    /// assert!(Side::new(&hole, HoleSize::new(2), HoleSize::new(1)).unwrap().start() == HoleSize::new(2));
    /// ```
    pub fn start(&self) -> HoleSize {
        self.unref.start()
    }

    /// # Examples
    ///
    /// ```
    /// use world_builder::*;
    ///
    /// let hole = Hole::new([
    ///     Point::new(0.,0.,0.),
    ///     Point::new(1.,0.,0.),
    ///     Point::new(2.,0.,0.),
    /// ].into_iter()).unwrap();
    ///
    /// assert!(Side::new(&hole, HoleSize::new(0), HoleSize::new(2)).unwrap().end() == HoleSize::new(2));
    /// assert!(Side::new(&hole, HoleSize::new(1), HoleSize::new(3)).unwrap().end() == HoleSize::new(1));
    /// assert!(Side::new(&hole, HoleSize::new(2), HoleSize::new(1)).unwrap().end() == HoleSize::new(0));
    /// ```
    pub fn end(&self) -> HoleSize {
        unsafe {
            self.unref.end(self.hole.as_ref())
        }
    }

    /// # Examples
    ///
    /// ```
    /// use world_builder::*;
    ///
    /// let hole = Hole::new([
    ///     Point::new(0.,0.,0.),
    ///     Point::new(1.,0.,0.),
    ///     Point::new(2.,0.,0.),
    /// ].into_iter()).unwrap();
    ///
    /// assert!(Side::new(&hole, HoleSize::new(0), HoleSize::new(2)).unwrap().length() == HoleSize::new(2));
    /// assert!(Side::new(&hole, HoleSize::new(1), HoleSize::new(3)).unwrap().length() == HoleSize::new(3));
    /// assert!(Side::new(&hole, HoleSize::new(2), HoleSize::new(1)).unwrap().length() == HoleSize::new(1));
    /// ```
    pub fn length(&self) -> HoleSize {
        self.unref.length()
    }

    /// # Examples
    ///
    /// ```
    /// use world_builder::*;
    ///
    /// let (p1,p2,p3,p4) = (
    ///     Point::new(1.,0.,0.),
    ///     Point::new(2.,0.,0.),
    ///     Point::new(3.,0.,0.),
    ///     Point::new(4.,0.,0.),
    /// );
    ///
    /// let hole = Hole::new([p1.clone(),p2.clone(),p3.clone(),p4.clone()].into_iter()).unwrap();
    /// let side1 = Side::new(&hole, HoleSize::new(0), HoleSize::new(1)).unwrap();
    /// let side2 = Side::new(&hole, HoleSize::new(2), HoleSize::new(2)).unwrap();
    /// let side3 = Side::new(&hole, HoleSize::new(0), HoleSize::new(4)).unwrap();
    /// let side4 = Side::new(&hole, HoleSize::new(3), HoleSize::new(4)).unwrap();
    ///
    /// assert!(side1.all_points().eq([&p1,&p2]));
    /// assert!(side2.all_points().eq([&p3,&p4,&p1]));
    /// assert!(side3.all_points().eq([&p1,&p2,&p3,&p4,&p1]));
    /// assert!(side4.all_points().eq([&p4,&p1,&p2,&p3,&p4]));
    /// ```
    pub fn all_points(&self) -> SideIter {
        unsafe {
            self.unref.all_points(self.hole.as_ref())
        }
    }


    /// # Examples
    ///
    /// ```
    /// use world_builder::*;
    ///
    /// let (p1,p2,p3,p4) = (
    ///     Point::new(1.,0.,0.),
    ///     Point::new(2.,0.,0.),
    ///     Point::new(3.,0.,0.),
    ///     Point::new(4.,0.,0.),
    /// );
    ///
    /// let hole = Hole::new([p1.clone(),p2.clone(),p3.clone(),p4.clone()].into_iter()).unwrap();
    /// let side1 = Side::new(&hole, HoleSize::new(0), HoleSize::new(2)).unwrap();
    /// let side2 = Side::new(&hole, HoleSize::new(3), HoleSize::new(3)).unwrap();
    ///
    /// assert!(side1.start_points().eq([&p1,&p2]));
    /// assert!(side2.start_points().eq([&p4,&p1,&p2]));
    /// ```
    pub fn start_points(&self) -> SideIter {
        unsafe {
            self.unref.start_points(self.hole.as_ref())
        }
    }

    /// # Examples
    ///
    /// ```
    /// use world_builder::*;
    ///
    /// let (p1,p2,p3,p4) = (
    ///     Point::new(1.,0.,0.),
    ///     Point::new(2.,0.,0.),
    ///     Point::new(3.,0.,0.),
    ///     Point::new(4.,0.,0.),
    /// );
    ///
    /// let hole = Hole::new([p1.clone(),p2.clone(),p3.clone(),p4.clone()].into_iter()).unwrap();
    /// let side1 = Side::new(&hole, HoleSize::new(0), HoleSize::new(2)).unwrap();
    /// let side2 = Side::new(&hole, HoleSize::new(3), HoleSize::new(3)).unwrap();
    ///
    /// assert!(side1.end_points().eq([&p2,&p3]));
    /// assert!(side2.end_points().eq([&p1,&p2,&p3]));
    /// ```
    pub fn end_points(&self) -> SideIter {
        unsafe {
            self.unref.end_points(self.hole.as_ref())
        }
    }

    /// # Examples
    ///
    /// ```
    /// use world_builder::*;
    ///
    /// let (p1,p2,p3,p4) = (
    ///     Point::new(1.,0.,0.),
    ///     Point::new(2.,0.,0.),
    ///     Point::new(3.,0.,0.),
    ///     Point::new(4.,0.,0.),
    /// );
    ///
    /// let hole = Hole::new([p1.clone(),p2.clone(),p3.clone(),p4.clone()].into_iter()).unwrap();
    /// let side1 = Side::new(&hole, HoleSize::new(0), HoleSize::new(2)).unwrap();
    /// let side2 = Side::new(&hole, HoleSize::new(3), HoleSize::new(3)).unwrap();
    ///
    /// assert_eq!(side1.first_point(), &p1);
    /// assert_eq!(side2.first_point(), &p4);
    /// ```
    pub fn first_point(&self) -> &Point {
        unsafe {
            self.unref.first_point(self.hole.as_ref())
        }
    }

    /// # Examples
    ///
    /// ```
    /// use world_builder::*;
    ///
    /// let (p1,p2,p3,p4) = (
    ///     Point::new(1.,0.,0.),
    ///     Point::new(2.,0.,0.),
    ///     Point::new(3.,0.,0.),
    ///     Point::new(4.,0.,0.),
    /// );
    ///
    /// let hole = Hole::new([p1.clone(),p2.clone(),p3.clone(),p4.clone()].into_iter()).unwrap();
    /// let side1 = Side::new(&hole, HoleSize::new(0), HoleSize::new(2)).unwrap();
    /// let side2 = Side::new(&hole, HoleSize::new(3), HoleSize::new(4)).unwrap();
    ///
    /// assert_eq!(side1.last_point(), &p3);
    /// assert_eq!(side2.last_point(), &p4);
    /// ```
    pub fn last_point(&self) -> &Point {
        unsafe {
            self.unref.last_point(self.hole.as_ref())
        }
    }

    /// # Examples
    ///
    /// ```
    /// use world_builder::*;
    ///
    /// let hole = Hole::new([
    ///     Point::new(0.,0.,0.),
    ///     Point::new(1.,0.,0.),
    ///     Point::new(2.,0.,0.),
    ///     Point::new(3.,0.,0.),
    /// ].into_iter()).unwrap();
    ///
    /// let side = Side::new(&hole, HoleSize::new(0), HoleSize::new(3)).unwrap();
    /// let inverse = side.clone().inverse().unwrap();
    /// assert_eq!(side.start(), inverse.end());
    /// assert_eq!(side.end(), inverse.start());
    ///
    /// let side = Side::new(&hole, HoleSize::new(3), HoleSize::new(1)).unwrap();
    /// let inverse = side.clone().inverse().unwrap();
    /// assert_eq!(side.start(), inverse.end());
    /// assert_eq!(side.end(), inverse.start());
    ///
    /// let side = Side::new(&hole, HoleSize::new(2), HoleSize::new(3)).unwrap();
    /// let inverse = side.clone().inverse().unwrap();
    /// assert_eq!(side.start(), inverse.end());
    /// assert_eq!(side.end(), inverse.start());
    ///
    /// let side = Side::new(&hole, HoleSize::new(1), HoleSize::new(4)).unwrap();
    /// assert!(side.inverse().is_none());
    /// ```
    pub fn inverse(self) -> Option<Self> {
        let hole = self.hole;
        unsafe {
            self.unref.inverse(hole.as_ref()).map(|unref| Self {hole,unref})
        }
    }

    /// # Errors
    ///
    /// - WBErrorHole::NotEqual:
    ///     Both sides refrence diffrent holes.
    ///
    /// # Examples
    ///
    /// ```
    /// use world_builder::*;
    ///
    /// let hole = Hole::new([
    ///     Point::new(0.,0.,0.),
    ///     Point::new(1.,0.,0.),
    ///     Point::new(2.,0.,0.),
    ///     Point::new(3.,0.,0.),
    /// ].into_iter()).unwrap();
    ///
    /// assert!( Side::new(&hole, HoleSize::new(0), HoleSize::new(2)).unwrap().overlapping(&Side::new(&hole, HoleSize::new(0), HoleSize::new(2)).unwrap()).unwrap());
    /// assert!( Side::new(&hole, HoleSize::new(1), HoleSize::new(2)).unwrap().overlapping(&Side::new(&hole, HoleSize::new(0), HoleSize::new(2)).unwrap()).unwrap());
    /// assert!(!Side::new(&hole, HoleSize::new(2), HoleSize::new(2)).unwrap().overlapping(&Side::new(&hole, HoleSize::new(0), HoleSize::new(2)).unwrap()).unwrap());
    /// assert!( Side::new(&hole, HoleSize::new(3), HoleSize::new(2)).unwrap().overlapping(&Side::new(&hole, HoleSize::new(0), HoleSize::new(2)).unwrap()).unwrap());
    ///
    /// assert!( Side::new(&hole, HoleSize::new(0), HoleSize::new(1)).unwrap().overlapping(&Side::new(&hole, HoleSize::new(3), HoleSize::new(2)).unwrap()).unwrap());
    /// assert!(!Side::new(&hole, HoleSize::new(1), HoleSize::new(1)).unwrap().overlapping(&Side::new(&hole, HoleSize::new(3), HoleSize::new(2)).unwrap()).unwrap());
    /// assert!(!Side::new(&hole, HoleSize::new(2), HoleSize::new(1)).unwrap().overlapping(&Side::new(&hole, HoleSize::new(3), HoleSize::new(2)).unwrap()).unwrap());
    /// assert!( Side::new(&hole, HoleSize::new(3), HoleSize::new(1)).unwrap().overlapping(&Side::new(&hole, HoleSize::new(3), HoleSize::new(2)).unwrap()).unwrap());
    /// ```
    pub fn overlapping<H2>(&self, other: &Side<H2>) -> Result<bool, WBErrorHole>
    where H2: AsRef<Hole> {
        let hole = self.hole.as_ref();
        WBErrorHole::test_holes_equal(hole, other.hole.as_ref())?;
        unsafe {Ok(self.unref.overlapping(&other.unref, hole))}
    }

    /// # Safety
    ///
    /// Calling this method with sides that do not reference the same hole
    /// will cause undefined behavior.
    pub unsafe fn overlapping_unchecked<H2>(&self, other: &Side<H2>) -> bool
    where H2: AsRef<Hole> {
        let hole = self.hole.as_ref();
        #[cfg(debug_assertions)]
        WBErrorHole::test_holes_equal(hole, other.hole.as_ref()).unwrap();
        self.unref.overlapping(&other.unref, hole)
    }

    fn valid(&self) -> Result<(), WBErrorHole> {
        let max = self.hole.as_ref().length();
        if self.start() >= max
            || self.length() > max
            || self.length() == HoleSize::new(0) {
            return Err(WBErrorHole::InvalidSide(
                max,
                self.start().into(),
                self.length().into())
            )
        }
        Ok(())
    }
}
impl<H> fmt::Display for Side<H>
where H: AsRef<Hole> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut point_iter = self.all_points();
        f.write_str("Side(")?;
        point_iter.next().unwrap().fmt(f)?;
        for point in point_iter {
            f.write_str(", ")?;
            point.fmt(f)?;
        }
        f.write_str(")")
    }
}
impl<H> fmt::Debug for Side<H>
where H: AsRef<Hole> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Side")
            .field("start", &self.start())
            .field("length", &self.length())
            .field("hole", &self.hole.as_ref())
            .finish()
    }
}
impl<H> PartialEq for Side<H>
where H: AsRef<Hole> {
    fn eq(&self, other: &Self) -> bool {
        self.hole.as_ref() == other.hole.as_ref()
            && self.start() == other.start()
            && self.length() == other.length()
    }
}
