use std::fmt::{Debug, Display};

use super::*;

pub(super) type ErrorM = WBErrorInvalidHoleMeta;
pub(super) type ErrorMB<E> = WBErrorBuildingHoleMeta<E>;

pub(super) type ResultM<T> = Result<T, ErrorM>;
pub(super) type ResultMB<T, E> = Result<T, ErrorMB<E>>;


pub(super) struct HoleMetaSlice<'a,S,T>(pub &'a [MetaData<S,T>])
where S: AnyHoleSize;
impl<'a,S,T> HoleMetaSlice<'a,S,T>
where S: AnyHoleSize {

    /// # Errors
    ///
    /// - WBErrorInvalidHoleMeta::InvalidLength
    ///     `positions` extend outside the length of `hole`.
    ///
    /// - WBErrorInvalidHoleMeta::InvalidOrder
    ///     `positions` must be in order.
    ///
    /// - WBErrorInvalidHoleMeta::EmptyData
    ///     `positions` is empty.
    pub fn from_positions<C,I>(hole: &Hole, positions: I) -> ResultM<C>
    where C: FromIterator<MetaData<S,T>> + AsRef<[MetaData<S,T>]>,
          I: Iterator<Item=(S, T)> {
        let out: C = positions.map(|(start, data)| {
            MetaData::new(start, data)
        }).collect();
        let slice = HoleMetaSlice(out.as_ref());
        slice.valid()?;
        slice.valid_unref(hole)?;
        Ok(out)
    }

    pub unsafe fn from_positions_unchecked<C,I>(positions: I) -> C
    where C: FromIterator<MetaData<S,T>> + AsRef<[MetaData<S,T>]>,
          I: Iterator<Item=(S, T)> {
        let out: C = positions.map(|(start, data)| {
            MetaData::new(start, data)
        }).collect();
        #[cfg(debug_assertions)]
        if let Err(err) = HoleMetaSlice(out.as_ref()).valid() {
            panic!("{:?} ({:?})", err, HoleMetaSlice(out.as_ref()))
        }
        out
    }

    /// # Errors
    ///
    /// - WBErrorInvalidHoleMeta::InvalidLength
    ///     `lengths` dos not add up to the length of `hole`.
    ///
    /// - WBErrorInvalidHoleMeta::InvalidOrder
    ///     'lengths' contains a value of 0.
    ///
    /// - WBErrorInvalidHoleMeta::EmptyData
    ///     `lengths` is empty.
    pub fn from_lengths<C,I>(hole: &Hole, start: S, lengths: I) -> ResultM<C>
    where C: FromIterator<MetaData<S,T>>,
          I: Iterator<Item=(S, T)> {
        let mut total = S::from(0u8);
        let out = lengths.map(|(length, t)| {
            if length == S::from(0u8) {
                return Err(ErrorM::InvalidOrder);
            }
            let data = MetaData::new(start +total, t);
            total = total.checked_add(length).ok_or(
                ErrorM::InvalidLength(hole.length(), HoleSizeFloat::MAX)
            )?;
            Ok(data)
        }).collect::<Result<C, _>>()?;
        if total != hole.length().into() {
            return Err(ErrorM::InvalidLength(hole.length(), total.into()))
        }
        Ok(out)
    }

    pub unsafe fn from_lengths_unchecked<C,I>(start: S, lengths: I) -> C
    where C: FromIterator<MetaData<S,T>>,
          I: Iterator<Item=(S, T)> {
        let mut total = S::from(0);
        lengths.map(|(length, t)| {
            #[cfg(debug_assertions)]
            assert!(length != S::from(0), "{:?}", ErrorM::InvalidOrder);
            let out = MetaData::new(start +total, t);
            total += length;
            out
        }).collect()
    }

    pub fn side<F>(&self, index: usize, get_max: F) -> S::SIDE
    where F: FnOnce() -> HoleSize {
        let start = self.0[index].start;
        S::make_side(
            start,
            if index +1 == self.len() {
                S::from(get_max()) -start +self.0[0].start
            } else {
                self.0[index+1].start -start
            }
        )
    }

    pub fn data(&self, index: usize) -> &'a T {
        &self.0[index].data
    }

    pub fn len(&self) -> usize {
        self.0.len()
    }

    pub fn valid(&self) -> ResultM<()> {
        let mut iter = self.0.iter();
        let mut last = iter.next().ok_or(ErrorM::EmptyData)?;
        for n in iter {
            if last.start >= n.start {
                return Err(ErrorM::InvalidOrder)
            }
            last = n;
        }
        Ok(())
    }
    pub fn valid_unref(&self, hole: &Hole) -> ResultM<()> {
        let last_start = self.0.last().unwrap().start;
        if last_start >= hole.length().into() {
            return Err(ErrorM::InvalidLength(
                hole.length(),
                last_start.into() +HoleSizeFloat::from(1u8)
            ))
        }
        Ok(())
    }
}
impl<'a,S,T> Debug for HoleMetaSlice<'a,S,T>
where S: AnyHoleSize {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str("HoleMeta(")?;
        f.write_str(&self.0.iter().map(|o| format!("{}", o.start)).join(","))?;
        f.write_str(")")
    }
}


pub(super) struct MetaData<S,T> {
    pub start: S,
    pub data: T
}
impl<S,T> MetaData<S,T> {
    pub fn new(start: S, data: T) -> Self {
        Self {start, data}
    }
}
impl<S,T> Clone for MetaData<S,T>
where S: Clone,
      T: Clone {
    fn clone(&self) -> Self {
        Self {
            start: self.start.clone(),
            data: self.data.clone()
        }
    }
}


pub(super) trait AnyHoleSize: Copy
    + std::ops::Add<Self,Output=Self>
    + std::ops::Sub<Self,Output=Self>
    + std::ops::AddAssign
    + PartialOrd
    + From<HoleSize>
    + From<u8>
    + Into<HoleSizeFloat>
    + Display
    + Debug
{
    type SIDE;
    fn make_side(start: Self, length: Self) -> Self::SIDE;
    fn checked_add(self, other: Self) -> Option<Self>;
}
impl AnyHoleSize for HoleSize {
    type SIDE = SideUnref;
    fn make_side(start: Self, length: Self) -> Self::SIDE {
        Self::SIDE::new(start, length)
    }
    fn checked_add(self, other: Self) -> Option<Self> {
        self.checked_add(other)
    }
}
impl AnyHoleSize for HoleSizeFloat {
    type SIDE = SideFloatUnref;
    fn make_side(start: Self, length: Self) -> Self::SIDE {
        Self::SIDE::new(start, length)
    }
    fn checked_add(self, other: Self) -> Option<Self> {
        self.checked_add(other)
    }
}
