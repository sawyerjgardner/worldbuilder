use super::*;

///
pub struct HoleMetaFloatUnrefed<T> {
    metadata: Box<[MetaData<HoleSizeFloat,T>]>
}
impl<T> HoleMetaFloatUnrefed<T> {

    /// # Errors
    ///
    /// - WBErrorInvalidHoleMeta::InvalidOrder
    ///     `positions` must be in order.
    ///
    /// - WBErrorInvalidHoleMeta::EmptyData
    ///     `positions` is empty.
    pub fn from_positions<I>(hole: &Hole, positions: I) -> ResultM<Self>
    where I: Iterator<Item=(HoleSizeFloat, T)> {
        Ok(Self {metadata: HoleMetaSlice::from_positions(hole, positions)?})
    }

    /// # Safety
    ///
    /// this method assumes `positions` are sorted and in bounds.
    pub unsafe fn from_positions_unchecked<I>(positions: I) -> Self
    where I: Iterator<Item=(HoleSizeFloat, T)> {
        Self {metadata: HoleMetaSlice::from_positions_unchecked(positions)}
    }

    /// # Errors
    ///
    /// - WBErrorInvalidHoleMeta::InvalidLength
    ///     `lengths` dos not add up to the length of `hole`.
    ///
    /// - WBErrorInvalidHoleMeta::InvalidOrder
    ///     'lengths' contains a value of 0.
    ///
    /// - WBErrorInvalidHoleMeta::EmptyData
    ///     `lengths` is empty.
    pub fn from_lengths<I>(hole: &Hole, start: HoleSizeFloat, lengths: I)
                           -> ResultM<Self>
    where I: Iterator<Item=(HoleSizeFloat, T)> {
        Ok(Self {metadata: HoleMetaSlice::from_lengths(hole, start, lengths)?})
    }

    /// # Safety
    ///
    /// this method assumes `positions` are sorted and in bounds.
    pub unsafe fn from_lengths_unchecked<I>(start: HoleSizeFloat, lengths: I)
                                            -> Self
    where I: Iterator<Item=(HoleSizeFloat, T)> {
        Self {metadata: HoleMetaSlice::from_lengths_unchecked(start, lengths)}
    }

    /// # Safety:
    ///
    /// `hole` must be the Hole this data refers to.
    pub unsafe fn side(&self, hole: &Hole, index: usize)
                -> SideFloatUnref {
        self.slice().side(index, || hole.length())
    }

    ///
    pub fn data(&self, index: usize) -> &T {
        self.slice().data(index)
    }

    ///
    pub fn len(&self) -> usize {
        self.slice().len()
    }

    pub(super) fn slice(&self) -> HoleMetaSlice<HoleSizeFloat,T> {
        HoleMetaSlice(self.metadata.as_ref())
    }
}
