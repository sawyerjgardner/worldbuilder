use super::*;


#[test]
fn stacked_single() {
    let (a1,p1,p2,p3,b1,b2) = (
        Point::new(0.,1.,0.),
        Point::new(1.,0.,0.),
        Point::new(2.,0.,0.),
        Point::new(3.,0.,0.),
        Point::new(4.,0.,1.),
        Point::new(5.,0.,1.),
    );
    let hole1 = Hole::new([p1.clone(), p2.clone(), p3.clone(), a1].into_iter()).unwrap();
    let hole2 = Hole::new([b1, b2, p1.clone(), p2.clone(), p3.clone()].into_iter()).unwrap();

    assert!(hole1.stacked(&hole2));
    assert!(hole2.stacked(&hole1));
    assert!(!hole1.touching(&hole2));
    assert!(!hole2.touching(&hole1));

    let touching1 = hole1.sides_touching(&&hole2);
    let touching2 = hole1.sides_touching(&&hole2);
    assert!(touching1.is_empty(), "touching1: {:?}", touching1);
    assert!(touching2.is_empty(), "touching2: {:?}", touching2);
    let stacked1 = hole1.sides_stacked(&&hole2).into_iter().exactly_one();
    let stacked2 = hole2.sides_stacked(&&hole1).into_iter().exactly_one();
    let (sides1a, sides1b) = stacked1.unwrap();
    let (sides2a, sides2b) = stacked2.unwrap();

    let points = [&p1, &p2, &p3];
    assert!(sides1a.all_points().eq(points), "{:?}", sides1a.length());
    assert!(sides1b.all_points().eq(points));
    assert!(sides2a.all_points().eq(points));
    assert!(sides2b.all_points().eq(points));
}


#[test]
fn stacked_single2() {
    let (a1,p1,p2,p3,p4,b1,b2) = (
        Point::new(0.,1.,0.),
        Point::new(1.,0.,0.),
        Point::new(2.,0.,0.),
        Point::new(3.,0.,0.),
        Point::new(4.,0.,0.),
        Point::new(5.,0.,2.),
        Point::new(6.,0.,2.),
    );
    let hole1 = Hole::new([p3.clone(), p4.clone(), a1, p1.clone(), p2.clone()].into_iter()).unwrap();
    let hole2 = Hole::new([p2.clone(), p3.clone(), p4.clone(), b1, b2, p1.clone()].into_iter()).unwrap();

    assert!(hole1.stacked(&hole2));
    assert!(hole2.stacked(&hole1));
    assert!(!hole1.touching(&hole2));
    assert!(!hole2.touching(&hole1));

    let touching1 = hole1.sides_touching(&&hole2);
    let touching2 = hole1.sides_touching(&&hole2);
    assert!(touching1.is_empty(), "touching1: {:?}", touching1);
    assert!(touching2.is_empty(), "touching2: {:?}", touching2);

    let stacked1 = hole1.sides_stacked(&&hole2);
    let stacked2 = hole2.sides_stacked(&&hole1);
    assert_eq!(stacked1.len(), 1, "stacked1: {:?}", stacked1);
    assert_eq!(stacked2.len(), 1, "stacked2: {:?}", stacked2);
    let (sides1a, sides1b) = stacked1.into_iter().exactly_one().unwrap();
    let (sides2a, sides2b) = stacked2.into_iter().exactly_one().unwrap();

    let points = [&p1, &p2, &p3, &p4];
    assert!(sides1a.all_points().eq(points), "1a");
    assert!(sides1b.all_points().eq(points), "1b");
    assert!(sides2a.all_points().eq(points), "2a");
    assert!(sides2b.all_points().eq(points), "2b");
}


#[test]
fn stacked_single3() {
    let (a1,p1,p2,p3,p4,b1,b2) = (
        Point::new(0.,1.,0.),
        Point::new(1.,0.,0.),
        Point::new(2.,0.,0.),
        Point::new(3.,0.,0.),
        Point::new(4.,0.,0.),
        Point::new(5.,0.,2.),
        Point::new(6.,0.,2.),
    );
    let hole1 = Hole::new([p2.clone(), p3.clone(), p4.clone(), a1, p1.clone()].into_iter()).unwrap();
    let hole2 = Hole::new([p3.clone(), p4.clone(), b1, b2, p1.clone(), p2.clone()].into_iter()).unwrap();

    assert!(hole1.stacked(&hole2));
    assert!(hole2.stacked(&hole1));
    assert!(!hole1.touching(&hole2));
    assert!(!hole2.touching(&hole1));

    let touching1 = hole1.sides_touching(&&hole2);
    let touching2 = hole1.sides_touching(&&hole2);
    assert!(touching1.is_empty(), "touching1: {:?}", touching1);
    assert!(touching2.is_empty(), "touching2: {:?}", touching2);

    let stacked1 = hole1.sides_stacked(&&hole2);
    let stacked2 = hole2.sides_stacked(&&hole1);
    assert_eq!(stacked1.len(), 1, "stacked1: {:?}", stacked1);
    assert_eq!(stacked2.len(), 1, "stacked2: {:?}", stacked2);
    let (sides1a, sides1b) = stacked1.into_iter().exactly_one().unwrap();
    let (sides2a, sides2b) = stacked2.into_iter().exactly_one().unwrap();

    let points = [&p1, &p2, &p3, &p4];
    assert!(sides1a.all_points().eq(points), "1a");
    assert!(sides1b.all_points().eq(points), "1b");
    assert!(sides2a.all_points().eq(points), "2a");
    assert!(sides2b.all_points().eq(points), "2b");
}


#[test]
fn stacked_double1() {
    let (a1,p1,p2,a2,p3,p4,b1,b2) = (
        Point::new(0.,1.,0.),
        Point::new(1.,0.,0.),
        Point::new(2.,0.,0.),
        Point::new(3.,1.,0.),
        Point::new(4.,0.,0.),
        Point::new(5.,0.,0.),
        Point::new(6.,0.,2.),
        Point::new(7.,0.,2.),
    );
    let hole1 = Hole::new([p4.clone(), a1, p1.clone(), p2.clone(), a2, p3.clone()].into_iter()).unwrap();
    let hole2 = Hole::new([p1.clone(), p2.clone(), b1, b2, p3.clone(), p4.clone()].into_iter()).unwrap();

    assert!(hole1.stacked(&hole2));
    assert!(hole2.stacked(&hole1));
    assert!(!hole1.touching(&hole2));
    assert!(!hole2.touching(&hole1));

    let touching1 = hole1.sides_touching(&&hole2);
    let touching2 = hole1.sides_touching(&&hole2);
    assert!(touching1.is_empty(), "touching1: {:?}", touching1);
    assert!(touching2.is_empty(), "touching2: {:?}", touching2);

    let stacked1 = hole1.sides_stacked(&&hole2);
    let stacked2 = hole2.sides_stacked(&&hole1);
    assert_eq!(stacked1.len(), 2, "stacked1: {:?}", stacked1);
    assert_eq!(stacked2.len(), 2, "stacked2: {:?}", stacked2);

    let (sides1a, sides1b) = &stacked1[0];
    let (sides2a, sides2b) = &stacked2[0];
    let points = [&p1, &p2];
    assert!(sides1a.all_points().eq(points), "1a");
    assert!(sides1b.all_points().eq(points), "1b");
    assert!(sides2a.all_points().eq(points), "2a");
    assert!(sides2b.all_points().eq(points), "2b");

    let (sides1a, sides1b) = &stacked1[1];
    let (sides2a, sides2b) = &stacked2[1];
    let points = [&p3, &p4];
    assert!(sides1a.all_points().eq(points), "1a");
    assert!(sides1b.all_points().eq(points), "1b");
    assert!(sides2a.all_points().eq(points), "2a");
    assert!(sides2b.all_points().eq(points), "2b");
}


#[test]
fn stacked_double2() {
    let (a1,p1,p2,a2,p3,p4,b1,b2) = (
        Point::new(0.,1.,0.),
        Point::new(1.,0.,0.),
        Point::new(2.,0.,0.),
        Point::new(3.,1.,0.),
        Point::new(4.,0.,0.),
        Point::new(5.,0.,0.),
        Point::new(6.,0.,2.),
        Point::new(7.,0.,2.),
    );
    let hole1 = Hole::new([a1, p1.clone(), p2.clone(), a2, p3.clone(), p4.clone()].into_iter()).unwrap();
    let hole2 = Hole::new([p2.clone(), p3.clone(), p4.clone(), b1, b2, p1.clone()].into_iter()).unwrap();

    assert!(hole1.stacked(&hole2));
    assert!(hole2.stacked(&hole1));
    assert!(!hole1.touching(&hole2));
    assert!(!hole2.touching(&hole1));

    let touching1 = hole1.sides_touching(&&hole2);
    let touching2 = hole1.sides_touching(&&hole2);
    assert!(touching1.is_empty(), "touching1: {:?}", touching1);
    assert!(touching2.is_empty(), "touching2: {:?}", touching2);

    let stacked1 = hole1.sides_stacked(&&hole2);
    let stacked2 = hole2.sides_stacked(&&hole1);
    assert_eq!(stacked1.len(), 2, "stacked1: {:?}", stacked1);
    assert_eq!(stacked2.len(), 2, "stacked2: {:?}", stacked2);

    let (sides1a, sides1b) = &stacked1[0];
    let (sides2a, sides2b) = &stacked2[1];
    let points = [&p1, &p2];
    assert!(sides1a.all_points().eq(points), "1a");
    assert!(sides1b.all_points().eq(points), "1b");
    assert!(sides2a.all_points().eq(points), "2a");
    assert!(sides2b.all_points().eq(points), "2b");

    let (sides1a, sides1b) = &stacked1[1];
    let (sides2a, sides2b) = &stacked2[0];
    let points = [&p3, &p4];
    assert!(sides1a.all_points().eq(points), "1a");
    assert!(sides1b.all_points().eq(points), "1b");
    assert!(sides2a.all_points().eq(points), "2a");
    assert!(sides2b.all_points().eq(points), "2b");
}


#[test]
fn touching_double() {
    let (a1,p1,p2,a2,p3,p4,b1,b2) = (
        Point::new(0.,1.,0.),
        Point::new(1.,0.,0.),
        Point::new(2.,0.,0.),
        Point::new(3.,1.,0.),
        Point::new(4.,0.,0.),
        Point::new(5.,0.,0.),
        Point::new(6.,0.,2.),
        Point::new(7.,0.,2.),
    );
    let hole1 = Hole::new([a1, p1.clone(), p2.clone(), a2, p3.clone(), p4.clone()].into_iter()).unwrap();
    let hole2 = Hole::new([p2.clone(), p1.clone(), b1, b2, p4.clone(), p3.clone()].into_iter()).unwrap();

    assert!(!hole1.stacked(&hole2));
    assert!(!hole2.stacked(&hole1));
    assert!(hole1.touching(&hole2));
    assert!(hole2.touching(&hole1));

    let touching1 = hole1.sides_touching(&&hole2);
    let touching2 = hole1.sides_touching(&&hole2);
    assert_eq!(touching1.len(), 2, "touching1: {:?}", touching1);
    assert_eq!(touching2.len(), 2, "touching2: {:?}", touching2);

    let stacked1 = hole1.sides_stacked(&&hole2);
    let stacked2 = hole2.sides_stacked(&&hole1);
    assert!(stacked1.is_empty(), "stacked1: {:?}", stacked1);
    assert!(stacked2.is_empty(), "stacked2: {:?}", stacked2);

    let (sides1a, sides1b) = &touching1[0];
    let (sides2a, sides2b) = &touching2[0];
    assert!(sides1a.all_points().eq([&p1, &p2]), "1a");
    assert!(sides1b.all_points().eq([&p2, &p1]), "1b");
    assert!(sides2a.all_points().eq([&p1, &p2]), "2a");
    assert!(sides2b.all_points().eq([&p2, &p1]), "2b");

    let (sides1a, sides1b) = &touching1[1];
    let (sides2a, sides2b) = &touching2[1];
    assert!(sides1a.all_points().eq([&p3, &p4]), "1a");
    assert!(sides1b.all_points().eq([&p4, &p3]), "1b");
    assert!(sides2a.all_points().eq([&p3, &p4]), "2a");
    assert!(sides2b.all_points().eq([&p4, &p3]), "2b");
}


#[test]
fn touching_double2() {
    let (a1,p1,p2,a2,p3,p4,b1,b2) = (
        Point::new(0.,1.,0.),
        Point::new(1.,0.,0.),
        Point::new(2.,0.,0.),
        Point::new(3.,1.,0.),
        Point::new(4.,0.,0.),
        Point::new(5.,0.,0.),
        Point::new(6.,0.,2.),
        Point::new(7.,0.,2.),
    );
    let hole1 = Hole::new([a1, p1.clone(), p2.clone(), a2, p3.clone(), p4.clone()].into_iter()).unwrap();
    let hole2 = Hole::new([p3.clone(), p2.clone(), p1.clone(), b1, b2, p4.clone()].into_iter()).unwrap();

    assert!(!hole1.stacked(&hole2));
    assert!(!hole2.stacked(&hole1));
    assert!(hole1.touching(&hole2));
    assert!(hole2.touching(&hole1));

    let touching1 = hole1.sides_touching(&&hole2);
    let touching2 = hole1.sides_touching(&&hole2);
    assert_eq!(touching1.len(), 2, "touching1: {:?}", touching1);
    assert_eq!(touching2.len(), 2, "touching2: {:?}", touching2);

    let stacked1 = hole1.sides_stacked(&&hole2);
    let stacked2 = hole2.sides_stacked(&&hole1);
    assert!(stacked1.is_empty(), "stacked1: {:?}", stacked1);
    assert!(stacked2.is_empty(), "stacked2: {:?}", stacked2);

    let (sides1a, sides1b) = &touching1[0];
    let (sides2a, sides2b) = &touching2[0];
    assert!(sides1a.all_points().eq([&p1, &p2]), "1a");
    assert!(sides1b.all_points().eq([&p2, &p1]), "1b");
    assert!(sides2a.all_points().eq([&p1, &p2]), "2a");
    assert!(sides2b.all_points().eq([&p2, &p1]), "2b");

    let (sides1a, sides1b) = &touching1[1];
    let (sides2a, sides2b) = &touching2[1];
    assert!(sides1a.all_points().eq([&p3, &p4]), "1a");
    assert!(sides1b.all_points().eq([&p4, &p3]), "1b");
    assert!(sides2a.all_points().eq([&p3, &p4]), "2a");
    assert!(sides2b.all_points().eq([&p4, &p3]), "2b");
}
