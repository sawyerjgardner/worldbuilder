use super::*;


///
pub trait SideFull<'a, H: AsRef<Hole> +'a> {

    ///
    fn points(&'a self) -> &'a [Point];

    ///
    fn get_hole_ref(&'a self) -> H;

    /// # Examples
    ///
    /// ```
    /// use world_builder::*;
    ///
    /// let hole = Hole::new([
    ///     Point::new(1.,0.,0.),
    ///     Point::new(2.,0.,0.),
    ///     Point::new(3.,0.,0.),
    ///     Point::new(4.,0.,0.),
    /// ].into_iter()).unwrap();
    ///
    /// let side = hole.side_full();
    ///
    /// assert!(side.start_points().eq(hole.points().iter()));
    /// ```
    fn side_full(&'a self) -> Side<H> {
        unsafe {
            Side::new_unchecked(
                self.get_hole_ref(),
                HoleSize::new(0),
                HoleSize::from(self.points().len())
            )
        }
    }

    /// # Examples
    ///
    /// ```
    /// use world_builder::*;
    ///
    /// let (p1, p2, p3, p4) = (
    ///     Point::new(1.,0.,0.),
    ///     Point::new(2.,0.,0.),
    ///     Point::new(3.,0.,0.),
    ///     Point::new(4.,0.,0.),
    /// );
    ///
    /// let hole1 = Hole::new([p1.clone(),p2.clone(),p3.clone()].into_iter()).unwrap();
    /// let hole2 = Hole::new([p1.clone(),p2.clone(),p4.clone()].into_iter()).unwrap();
    /// let sides1 = hole1.sides_stacked(&&hole2);
    /// let sides2 = hole1.sides_stacked(&&hole1);
    /// let sides3 = hole1.sides_touching(&&hole2);
    ///
    /// assert_eq!(sides1.len(), 1);
    /// assert_eq!(sides2.len(), 1);
    /// assert_eq!(sides3.len(), 0);
    ///
    /// let (a, b) = &sides1[0];
    /// let (a2, b2) = &sides2[0];
    /// assert!(a.all_points().eq(vec![&p1,&p2]));
    /// assert!(a.all_points().eq(b.all_points()));
    /// assert!(a2.all_points().eq(vec![&p1,&p2,&p3,&p1]));
    /// assert!(a2.all_points().eq(b2.all_points()));
    /// ```
    fn sides_stacked<'b, H2, T>(&'a self, other: &'b T)
                                -> Vec<(Side<H>, Side<H2>)>
    where H2: AsRef<Hole> +Clone +'b,
          T: SideFull<'b, H2> +?Sized {
        let self_points = self.points();
        let other_points = other.points();
        let mut segments = CommonSegment::segments_loop(
            self_points.iter(),
            other_points.iter()
        );
        segments.retain(|o| o.length != 1);
        if segments.len() == 1 {
            let common = &segments[0];
            if common.length == self_points.len()
                && common.length == other_points.len() {
                    unsafe {
                        return vec![(
                            Side::new_unchecked(
                                self.get_hole_ref(),
                                HoleSize::from(common.a_start),
                                HoleSize::from(common.length)
                            ),
                            Side::new_unchecked(
                                other.get_hole_ref(),
                                HoleSize::from(common.b_start),
                                HoleSize::from(common.length)
                            )
                        )]
                    }
            }
        }
        segments.into_iter().map(|common| unsafe {(
            Side::new_unchecked(
                self.get_hole_ref(),
                HoleSize::from(common.a_start),
                HoleSize::from(common.length-1)
            ),
            Side::new_unchecked(
                other.get_hole_ref(),
                HoleSize::from(common.b_start),
                HoleSize::from(common.length-1)
            )
        )}).collect_vec()
    }

    /// # Examples
    ///
    /// ```
    /// use world_builder::*;
    ///
    /// let (p1, p2, p3, p4) = (
    ///     Point::new(1.,0.,0.),
    ///     Point::new(2.,0.,0.),
    ///     Point::new(3.,0.,0.),
    ///     Point::new(4.,0.,0.),
    /// );
    ///
    /// let hole1 = Hole::new([p1.clone(),p2.clone(),p3.clone()].into_iter()).unwrap();
    /// let hole2 = Hole::new([p2.clone(),p1.clone(),p4.clone()].into_iter()).unwrap();
    /// let hole3 = Hole::new([p3.clone(),p2.clone(),p1.clone()].into_iter()).unwrap();
    /// let sides1 = hole1.sides_touching(&&hole2);
    /// let sides2 = hole1.sides_stacked(&&hole2);
    /// let sides3 = hole1.sides_touching(&&hole3);
    ///
    /// assert_eq!(sides1.len(), 1, "{:?}", sides1);
    /// assert_eq!(sides2.len(), 0, "{:?}", sides2);
    /// assert_eq!(sides3.len(), 1, "{:?}", sides3);
    ///
    /// let (a, b) = &sides1[0];
    /// let (a2, b2) = &sides3[0];
    /// assert!(a.all_points().eq(vec![&p1,&p2]));
    /// assert!(b.all_points().eq(vec![&p2,&p1]));
    /// assert!(a2.all_points().eq(vec![&p1,&p2,&p3,&p1]));
    /// assert!(b2.all_points().eq(vec![&p3,&p2,&p1,&p3]));
    /// ```
    fn sides_touching<'b, H2, T>(&'a self, other: &'b T)
                                 -> Vec<(Side<H>, Side<H2>)>
    where H2: AsRef<Hole> +Clone +'b,
          T: SideFull<'b, H2> +?Sized {
        let self_points = self.points();
        let other_points = other.points();
        let mut segments = CommonSegment::segments_loop(
            self_points.iter(),
            other_points.iter().rev()
        );
        segments.retain(|o| o.length != 1);
        let other_len = other_points.len();
        if segments.len() == 1 {
            let common = &segments[0];
            if common.length == self_points.len() && common.length == other_len {
                let b_start = (other_len -common.b_start).rem_euclid(other_len);
                return unsafe {vec![(
                    Side::new_unchecked(
                        self.get_hole_ref(),
                        HoleSize::from(common.a_start),
                        HoleSize::from(common.length)
                    ),
                    Side::new_unchecked(
                        other.get_hole_ref(),
                        HoleSize::from(b_start),
                        HoleSize::from(common.length)
                    )
                )]}
            }
        }
        segments.into_iter().map(|common| {
            //FIXME: this can couse problems if b_start + length >= isize::MAX
            let b_start = (-((common.b_start +common.length) as isize))
                .rem_euclid(other_len as isize) as usize;
            unsafe {(
                Side::new_unchecked(
                    self.get_hole_ref(),
                    HoleSize::from(common.a_start),
                    HoleSize::from(common.length-1)
                ),
                Side::new_unchecked(
                    other.get_hole_ref(),
                    HoleSize::from(b_start),
                    HoleSize::from(common.length-1)
                )
            )}
        }).collect_vec()
    }
}
impl<'a, H> SideFull<'a, H> for H
where H: AsRef<Hole> +Clone +'a{
    fn points(&'a self) -> &'a [Point] {
        self.as_ref().points()
    }
    fn get_hole_ref(&'a self) -> H {
        self.clone()
    }
}
