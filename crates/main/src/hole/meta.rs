use super::*;

mod error;
mod sidesort;
mod unref;
pub use error::*;
pub use unref::*;
use sidesort::*;


///
#[derive(RefHole)]
pub struct HoleMeta<H, T>
where H: AsRef<Hole> {
    hole: H,
    unref: HoleMetaUnref<T>
}
impl<H, T> HoleMeta<H, T>
where H: AsRef<Hole> {

    /// # Errors
    ///
    /// - WBErrorBuildingHoleMeta::BuilderError:
    ///     `hole_builder` returned an error.
    ///
    /// - WBErrorBuildingHoleMeta::Meta:
    ///     the sides in `other_side_data` could not be connect end to end in
    ///     closed loops.
    pub fn from_sides<I, H2, F, E>(other_side_data: I, hole_builder: F)
                                   -> ResultMB<Vec<Self>, E>
    where I: Iterator<Item=(Side<H2>, T)>,
          H2: AsRef<Hole>,
          F: FnMut(Vec<Point>) -> Result<H, E> {
        SortSidesTouching::new(other_side_data).into_metas(hole_builder)
    }

    /// # Errors
    ///
    /// - WBErrorInvalidHoleMeta::InvalidLength
    ///     `positions` extend outside the length of `hole`.
    ///
    /// - WBErrorInvalidHoleMeta::InvalidOrder
    ///     `positions` must be in order.
    ///
    /// - WBErrorInvalidHoleMeta::EmptyData
    ///     `positions` is empty.
    pub fn from_positions<I>(hole: H, positions: I) -> ResultM<Self>
    where I: Iterator<Item=(HoleSize, T)> {
        let unref = HoleMetaUnref::from_positions(hole.as_ref(), positions)?;
        Ok(Self {hole, unref})
    }

    /// # Safety
    ///
    /// this method assumes `positions` are sorted and in bounds.
    pub unsafe fn from_positions_unchecked<I>(hole: H, positions: I) -> Self
    where I: Iterator<Item=(HoleSize, T)> {
        let unref = HoleMetaUnref::from_positions_unchecked(positions);
        #[cfg(debug_assertions)]
        unref.slice().valid_unref(hole.as_ref()).unwrap();
        Self {hole, unref}
    }

    /// # Errors
    ///
    /// - WBErrorInvalidHoleMeta::InvalidLength
    ///     `lengths` dos not add up to the length of `hole`.
    ///
    /// - WBErrorInvalidHoleMeta::InvalidOrder
    ///     'lengths' contains a value of 0.
    ///
    /// - WBErrorInvalidHoleMeta::EmptyData
    ///     `lengths` is empty.
    pub fn from_lengths<I>(hole: H, start: HoleSize, lengths: I) -> ResultM<Self>
    where I: Iterator<Item=(HoleSize, T)> {
        let unref = HoleMetaUnref::from_lengths(hole.as_ref(), start, lengths)?;
        Ok(Self {hole, unref})
    }

    /// # Safty
    ///
    /// this method assumes the sum of `lengths` is equal to the length of `hole`.
    pub unsafe fn from_lengths_unchecked<I>(hole: H, start: HoleSize, lengths: I)
                                            -> Self
    where I: Iterator<Item=(HoleSize, T)> {
        let unref = HoleMetaUnref::from_lengths_unchecked(start, lengths);
        #[cfg(debug_assertions)]
        unref.slice().valid_unref(hole.as_ref()).unwrap();
        Self {hole, unref}
    }

    ///
    pub fn side(&self, index: usize) -> Side<H> 
    where H: Clone {
        unsafe {
            Side::from_unref(
                self.hole(),
                self.unref.slice().side(index, || self.hole.as_ref().length())
            )
        }
    }

    ///
    pub fn data(&self, index: usize) -> &T {
        self.unref.data(index)
    }

    ///
    pub fn len(&self) -> usize {
        self.unref.len()
    }
}
