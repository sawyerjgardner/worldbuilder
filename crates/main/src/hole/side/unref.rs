use super::*;


///
#[derive(Debug,Clone,Copy,PartialEq,Eq)]
pub struct SideUnref {
    start: HoleSize,
    length: HoleSize,
}
impl SideUnref {

    ///
    pub fn new(start: HoleSize, length: HoleSize) -> Self {
        Self {start, length}
    }

    ///
    pub fn start(&self) -> HoleSize {
        self.start
    }

    ///
    pub fn length(&self) -> HoleSize {
        self.length
    }

    /// # Safety:
    ///
    /// `hole` must be the Hole this data refers to.
    pub unsafe fn end(&self, hole: &Hole) -> HoleSize {
        _add_wrap_at1(self.start(), self.length(), hole.length())
    }

    /// # Safety:
    ///
    /// `hole` must be the Hole this data refers to.
    pub unsafe fn all_points<'a>(&self, hole: &'a Hole) -> SideIter<'a> {
        SideIter::new(
            hole.points(),
            self.start(),
            self.length() +HoleSize::new(1)
        )
    }

    /// # Safety:
    ///
    /// `hole` must be the Hole this data refers to.
    pub unsafe fn start_points<'a>(&self, hole: &'a Hole) -> SideIter<'a> {
        SideIter::new(
            hole.points(),
            self.start(),
            self.length()
        )
    }

    /// # Safety:
    ///
    /// `hole` must be the Hole this data refers to.
    pub unsafe fn end_points<'a>(&self, hole: &'a Hole) -> SideIter<'a> {
        SideIter::new(
            hole.points(),
            (self.start() +HoleSize::new(1)).rem_euclid(hole.length()),
            self.length()
        )
    }

    /// # Safety:
    ///
    /// `hole` must be the Hole this data refers to.
    pub unsafe fn first_point<'a>(&self, hole: &'a Hole) -> &'a Point {
        &hole.points()[self.start()]
    }

    /// # Safety:
    ///
    /// `hole` must be the Hole this data refers to.
    pub unsafe fn last_point<'a>(&self, hole: &'a Hole) -> &'a Point {
        &hole.points()[self.end(hole)]
    }

    /// # Safety:
    ///
    /// `hole` must be the Hole this data refers to.
    pub unsafe fn inverse(self, hole: &Hole) -> Option<Self> {
        let new_length = hole.length() -self.length();
        if new_length == HoleSize::new(0) {
            return None
        }
        let new_start = self.end(hole);
        Some(Self::new(new_start, new_length))
    }

    /// # Safety:
    ///
    /// `self` and `other` must both be refering to `hole`.
    pub unsafe fn overlapping(&self, other: &SideUnref, hole: &Hole)
                              -> bool {
        self.contains(hole, other.start()) || other.contains(hole, self.start())
    }

    pub(in super::super) fn overlapping_debug(&self, other: &SideUnref) -> bool {
        self.contains_debug(other.start()) || other.contains_debug(self.start())
    }

    fn contains(&self, hole: &Hole, position: HoleSize) -> bool {
        let max = hole.length();
        let start = self.start();
        let (end, overflow) = start.overflowing_add(self.length()) ;
        if overflow || end > max {
            start <= position || position < end.wrapping_sub(max)
        } else {
            start <= position && position < end
        }
    }

    fn contains_debug(&self, position: HoleSize) -> bool {
        self.start() <= position
            && position < self.start().saturating_add(self.length())
    }
}
