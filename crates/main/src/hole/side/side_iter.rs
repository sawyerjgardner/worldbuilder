use std::iter::FusedIterator;

use super::*;


#[derive(Clone)]
pub struct SideIter<'a> {
    points: &'a [Point],
    index: HoleSize,
    length: HoleSize
}
impl<'a> SideIter<'a> {
    pub fn new(points: &'a [Point], index: HoleSize, length: HoleSize) -> Self {
        Self {points, index, length}
    }

    fn rem_max(&self, val: HoleSize) -> HoleSize {
        val.rem_euclid(self.points.len().into())
    }
    fn try_shrink(&mut self) -> bool {
        match self.length.checked_sub(HoleSize::new(1)) {
            Some(val) => {self.length = val; true},
            None => false
        }
    }
    fn shift_forword(&mut self) {
        self.index = self.rem_max(self.index +HoleSize::new(1));
    }
    fn point_first(&self) -> &'a Point {
        &self.points[self.index]
    }
    fn point_after(&self) -> &'a Point {
        &self.points[self.rem_max(self.index +self.length +HoleSize::new(1))]
    }
}
impl<'a> Iterator for SideIter<'a> {
    type Item = &'a Point;
    fn next(&mut self) -> Option<Self::Item> {
        if self.try_shrink() {
            let out = Some(self.point_first());
            self.shift_forword();
            return out
        }
        None
    }
    fn size_hint(&self) -> (usize, Option<usize>) {
        let size = self.length.into();
        (size, Some(size))
    }
}
impl<'a> FusedIterator for SideIter<'a> {}
impl<'a> ExactSizeIterator for SideIter<'a> {}
impl<'a> DoubleEndedIterator for SideIter<'a> {
    fn next_back(&mut self) -> Option<Self::Item> {
        if self.try_shrink() {
            return Some(self.point_after())
        }
        None
    }
}
impl<'a> fmt::Debug for SideIter<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_list()
            .entries(self.clone())
            .finish()
    }
}
