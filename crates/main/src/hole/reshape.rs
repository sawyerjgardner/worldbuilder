use super::*;


///
#[derive(RefHole)]
pub struct HoleReshape<H>
where H: AsRef<Hole> {
    hole: H,
    unref: HoleReshapeUnref
}
impl<H> HoleReshape<H>
where H: AsRef<Hole> {

    ///
    pub fn new(hole: H) -> Self {
        Self {hole, unref: HoleReshapeUnref::new()}
    }

    ///
    pub fn is_empty(&self) -> bool {
        self.unref.is_empty()
    }

    ///
    pub fn insert<H2>(&mut self, side: SideFloat<H2>, points: Vec<Point>)
                      -> Result<&mut Self,()>
    where H2: AsRef<Hole> {
        let hole = self.hole.as_ref();
        WBErrorHole::test_holes_equal(hole, side.as_hole().as_ref()).unwrap();
        unsafe {
            self.unref.insert(hole, side.into_unref(), points)?;
        }
        Ok(self)
    }

    ///
    pub unsafe fn insert_unchecked(&mut self,
                                   side: SideFloatUnref,
                                   points: Vec<Point>) -> &mut Self {
        self.unref.insert_unchecked(side, points);
        self
    }

    ///
    pub fn apply(self) -> Vec<Point> {
        unsafe {
            self.unref.apply(self.hole.as_ref())
        }
    }
}


///
#[derive(Clone)]
pub struct HoleReshapeUnref {
    sidedata: Vec<(SideFloatUnref, Vec<Point>)>
}
impl HoleReshapeUnref {

    ///
    pub fn new() -> Self {
        Self {sidedata: Vec::new()}
    }

    ///
    pub fn is_empty(&self) -> bool {
        self.sidedata.is_empty()
    }

    /// # Safety:
    ///
    /// `hole` must be the Hole this data refers to.
    pub unsafe fn insert(&mut self,
                         hole: &Hole,
                         side: SideFloatUnref,
                         points: Vec<Point>) -> Result<&mut Self,()> {
        if self.is_empty() {
            self.insert_first_time(side, points);
            return Ok(self)
        }
        let pos = self.position(&side);
        unsafe {
            let pos_back = pos.rem_euclid(self.sidedata.len());
            let pos_next = pos.checked_sub(1).unwrap_or(self.sidedata.len()-1);
            if side.overlapping(hole, &self.sidedata[pos_back].0)
                || side.overlapping(hole, &self.sidedata[pos_next].0) {
                return Err(())
            }
        }
        self.sidedata.insert(pos, (side, points));
        return Ok(self)
    }

    ///
    pub unsafe fn insert_unchecked(&mut self,
                                   side: SideFloatUnref,
                                   points: Vec<Point>) -> &mut Self {
        //FIXME: debug assert
        self.sidedata.insert(self.position(&side), (side, points));
        self
    }

    /// # Safety:
    ///
    /// `hole` must be the Hole this data refers to.
    pub unsafe fn apply(self, hole: &Hole) -> Vec<Point> {
        if self.sidedata.is_empty() {
            return hole.points.into()
        }
        let mut iter = self.sidedata.into_iter();
        let (first_side, mut out_points) = iter.next().unwrap();
        let first_start = first_side.start().to_int_unchecked();
        // add sides and spaces between
        let mut last_end = first_side.end(hole).ceil().to_int_unchecked();
        for (next_side, points) in iter {
            out_points.extend(hole.points()[
                usize::from(last_end)
                    .. usize::from(next_side.start().to_int_unchecked())+1
            ].iter().cloned());
            out_points.extend(points.into_iter());
            last_end = next_side.end(hole).ceil().to_int_unchecked();
        }
        // add space between last and first side
        last_end = last_end.rem_euclid(hole.length());
        if last_end <= first_start {
            out_points.extend(hole.points()[
                usize::from(last_end) .. usize::from(first_start)+1
            ].iter().cloned());
        } else {
            out_points.extend(hole.points()[usize::from(last_end)..].iter().cloned());
            out_points.extend(hole.points()[..usize::from(first_start)+1].iter().cloned());
        }
        out_points
    }


    fn insert_first_time(&mut self, side: SideFloatUnref, points: Vec<Point>) {
        self.sidedata = vec![(side, points)]
    }

    fn position(&self, side: &SideFloatUnref) -> usize {
        let start = side.start();
        self.sidedata.iter()
                     .position(|o| o.0.start() >= start)
                     .unwrap_or(self.sidedata.len())
    }
}
