use std::collections::TryReserveError;

use super::*;

mod unref;
pub(crate) mod sidemap_iter;
pub use unref::*;
use sidemap_iter::*;



///
#[derive(RefHole)]
pub struct SideMap<H, T>
where H: AsRef<Hole> {
    hole: H,
    unref: SideMapUnref<T>
}
impl<H, T> SideMap<H, T>
where H: AsRef<Hole>{
    /// Constructs an empty `SideMap<H, T>`.
    pub fn new(hole: H) -> Self {
        Self {hole, unref: SideMapUnref::new()}
    }

    /// Creates an empty `SideMap<H, T>` with at least the specified capacity.
    /// 
    /// The `SideMap` will be able to hold at least `capacity` elements without 
    /// reallocating. This method is allowed to allocate for more elements than 
    /// `capacity``. If `capacity` is 0, nothing will be allocated.
    pub fn new_capacity(hole: H, capacity: HoleSize) -> Self {
        Self {hole, unref: SideMapUnref::new_capacity(capacity)}
    }

    /// Returns the number of elements in the map.
    pub fn len(&self) -> HoleSize {
        self.unref.len()
    }

    /// Returns true if this map is empty.
    pub fn is_empty(&self) -> bool {
        self.unref.is_empty()
    }

    /// Returns true if all available sides have a value.
    pub fn is_full(&self) -> bool {
        unsafe {self.unref.is_full(self.hole.as_ref())}
    }

    /// Returns the number of elements the map can hold without reallocating.
    ///
    /// This number is a lower bound; the `SideMap<H, T>` might be able to hold
    /// more, but is guaranteed to be able to hold at least this many.
    pub fn capacity(&self) -> HoleSize {
        self.unref.capacity()
    }

    /// Reserves capacity for at least `additional` more elements to be inserted
    /// in the map.
    ///
    /// The collection may reserve more space to speculatively avoid frequent
    /// reallocations. After calling `reserve`, capacity will be greater than or
    /// equal to `self.len() + additional`. Does nothing if capacity is already
    /// sufficient.
    pub fn reserve(&mut self, additional: HoleSize) {
        self.unref.reserve(additional)
    }

    /// Tries to reserve capacity for at least `additional` more elements to be
    /// inserted in the `SideMap`.
    ///
    /// The collection may reserve more space to speculatively avoid frequent
    /// reallocations. After calling `try_reserve`, capacity will be greater than
    /// or equal to `self.len() + additional` if it returns `Ok(())`. Does nothing
    /// if capacity is already sufficient.
    ///
    /// # Errors:
    ///
    /// If the capacity overflows, or the allocator reports a failure, then an error is returned.
    pub fn try_reserve(&mut self, additional: HoleSize) -> Result<(), TryReserveError> {
        self.unref.try_reserve(additional)
    }

    /// Shrinks the capacity of the map with a lower limit.
    /// It will drop down no lower than the supplied limit while maintaining the
    /// internal rules and possibly leaving some space in accordance with the
    /// resize policy.
    pub fn shrink_to(&mut self, min_capacity: HoleSize) {
        self.unref.shrink_to(min_capacity)
    }

    /// Shrinks the capacity of the map as much as possible.
    ///
    /// It will drop down as much as possible while maintaining the internal
    /// rules and possibly leaving some space in accordance with the resize
    /// policy.
    pub fn shrink_to_fit(&mut self) {
        self.unref.shrink_to_fit()
    }

    /// Inserts a side-value pair into the map.
    ///
    /// returens the index the value was placed at.
    ///
    /// # Errors
    /// 
    /// - WBErrorHole::NotEqual:
    ///     `side` is not from the hole `self` refers to.
    /// 
    /// - WBErrorHole::SideCollision:
    ///     `side` is overlapping with a side already defined in `self`.
    pub fn insert<H2>(&mut self, side: Side<H2>, value: T) -> Result<HoleSize,WBErrorHole>
    where H2: AsRef<Hole> {
        let hole = self.hole.as_ref();
        WBErrorHole::test_holes_equal(hole, side.as_hole().as_ref())?;
        unsafe {
            self.unref.insert(hole, side.into_unref(), value).ok_or(
                //FIXME: get point of collision
                WBErrorHole::SideCollision(hole, HoleSize::new(0))
            )
        }
    }

    /// Removes and returns a side-value pair from the map.
    pub fn remove(&mut self, index: HoleSize) -> Option<(Side<H>, T)>
    where H: Clone {
        self.unref.remove(index).map(|(side, data)| {
            (unsafe {Side::from_unref(self.hole.clone(), side)}, data)
        })
    }

    /// Clears the map, removing all values. Keeps the allocated memory for reuse.
    pub fn clear(&mut self) {
        self.unref.clear()
    }

    /// Removes and returns the last side-value pair in the map.
    pub fn pop(&mut self) -> Option<(Side<H>, T)>
    where H: Clone {
        self.unref.pop().map(|(side, data)| {
            (unsafe {Side::from_unref(self.hole.clone(), side)}, data)
        })
    }

    /// Removes and returns the last value in the map.
    pub fn pop_value(&mut self) -> Option<T> {
        self.unref.pop_value()
    }

    /// Resturns the side at `index` and a refrence to its value.
    pub fn side_value(&self, index: HoleSize) -> Option<(Side<H>, &T)>
    where H: Clone {
        self.unref.side_value(index).map(|(side, data)| {
            (unsafe {Side::from_unref(self.hole(), side)}, data)
        })
    }

    /// Resturns the side at `index` and a mutable refrence to its value.
    pub fn side_value_mut(&mut self, index: HoleSize) -> Option<(Side<H>, &mut T)>
    where H: Clone {
        self.unref.side_value_mut(index).map(|(side, data)| {
            (unsafe {Side::from_unref(self.hole.clone(), side)}, data)
        })
    }

    /// Resturns the side at `index`.
    pub fn side(&self, index: HoleSize) -> Option<Side<H>>
    where H: Clone {
        self.unref.side(index).map(|side| {
            unsafe {Side::from_unref(self.hole.clone(), side)}
        })
    }

    /// Resturns a refrence to the value at `index`.
    pub fn value(&self, index: HoleSize) -> Option<&T> {
        self.unref.value(index)
    }

    /// Resturns a mutable refrence to the value at `index`.
    pub fn value_mut(&mut self, index: HoleSize) -> Option<&mut T> {
        self.unref.value_mut(index)
    }

    /// TODO:
    pub fn gaps(&self) -> SideMap<H, HoleSize>
    where H: Clone {
        unsafe {
            let unref = self.unref.gaps(self.hole.as_ref());
            SideMap::from_unref(self.hole.clone(), unref)
        }
    }

    /// Create a new `SideMap` mapping the values in `self` with `func`.
    pub fn map<F, T2>(&self, func: F) -> SideMap<H, T2>
    where H: Clone,
          F: FnMut(&T) -> T2 {
        SideMap {
            unref: self.unref.map(func),
            hole: self.hole.clone()
        }
    }

    /// Creates a new `SideMap` by mapping the values in `self` with `func`.
    ///
    /// if `func` returns and error, the process is aborted and the error is returned.
    pub fn try_map<F, T2, E>(&self, func: F) -> Result<SideMap<H, T2>, E>
    where H: Clone,
          F: FnMut(&T) -> Result<T2, E> {
        Ok(SideMap {
            unref: self.unref.try_map(func)?,
            hole: self.hole.clone()
        })
    }

    /// Applys `func` to each value in the map.
    pub fn into_map<F, T2>(self, func: F) -> SideMap<H, T2>
    where F: FnMut(T) -> T2 {
        SideMap {
            hole: self.hole,
            unref: self.unref.into_map(func)
        }
    }

    /// Applys `func` to each value in the map.
    ///
    /// if `func` returns and error, the process is aborted and the error is returned.
    pub fn try_into_map<F, T2, E>(self, func: F) -> Result<SideMap<H, T2>, E>
    where F: FnMut(T) -> Result<T2, E> {
        Ok(SideMap {
            unref: self.unref.try_into_map(func)?,
            hole: self.hole
        })
    }

    /// An iterator visiting all side-value pairs in arbitrary order.
    /// 
    /// The iterator element type is `(Side<H>, &'a T)`.
    pub fn iter(&self) -> SideMapIter<H,T>
    where H: Clone {
        SideMapIter {hole: &self.hole, iter: self.unref.vec.iter()}
    }

    /// An iterator visiting all side-value pairs in arbitrary order.
    /// 
    /// The iterator element type is `(Side<H>, &'a mut T)`.
    pub fn iter_mut(&mut self) -> SideMapIterMut<H,T>
    where H: Clone {
        SideMapIterMut {hole: &self.hole, iter: self.unref.vec.iter_mut()}
    }

    /// An iterator visiting all sides in arbitrary order.
    /// 
    /// The iterator element type is `Side<H>`.
    pub fn sides(&self) -> SideMapSides<H, T>
    where H: Clone {
        SideMapSides { hole: &self.hole, iter: self.unref.vec.iter() }
    }

    /// An iterator visiting all values in arbitrary order. 
    /// 
    /// The iterator element type is `&'a T`.
    pub fn values(&self) -> SideMapValues<T> {
        self.unref.values()
    }

    /// An iterator visiting all values mutably in arbitrary order. 
    /// 
    /// The iterator element type is `&'a mut T`.
    pub fn values_mut(&mut self) -> SideMapValuesMut<T> {
        self.unref.values_mut()
    }

    /// Creates a consuming iterator returning all the values in arbitrary order. 
    /// 
    /// The map cannot be used after calling this. 
    /// The iterator element type is `T`.
    pub fn into_values(self) -> IntoSideMapValues<T>
    where H: Clone {
        self.unref.into_values()
    }

    /// Creates a consuming iterator returning every defined side in arbitrary order. 
    /// 
    /// The map cannot be used after calling this. 
    /// The iterator element type is `Side<H>`.
    pub fn into_sides(self) -> IntoSideMapSides<H, T>
    where H: Clone {
        IntoSideMapSides {hole: self.hole, iter: self.unref.vec.into_iter()}
    }


    /// Inserts a side-value pair into the map.
    ///
    /// returens the index the value was placed at.
    ///
    /// # Safety:
    ///
    /// This method dose not check whether `side` is already defined in the map.
    pub unsafe fn insert_unchecked(&mut self, side: Side<H>, value: T) -> HoleSize {
        #[cfg(debug_assertions)]
        WBErrorHole::test_holes_equal(
            self.hole.as_ref(),
            side.as_hole().as_ref()
        ).unwrap();
        self.unref.insert_unchecked(side.into_unref(), value)
    }

    /// Removes and returns a side-value pair from the map.
    ///
    /// # Safety:
    ///
    /// This method dose not perform any bounds checking on `index`. `index`
    /// should be less then `self.len()`.
    pub unsafe fn remove_unchecked(&mut self, index: HoleSize) -> (Side<H>, T)
    where H: Clone {
        let (side, data) = self.unref.remove_unchecked(index);
        (Side::from_unref(self.hole.clone(), side), data)
    }

    /// Inserts a side-value pair to the end of the map.
    ///
    /// # Safety:
    ///
    /// This bypasses all internal safety checks, it is almost garenteed to cause
    /// undefined behavior.
    pub unsafe fn push_unchecked(&mut self, side: Side<H>, value: T) {
        #[cfg(debug_assertions)]
        WBErrorHole::test_holes_equal(
            self.hole.as_ref(),
            side.as_hole().as_ref()
        ).unwrap();
        self.unref.push_unchecked(side.into_unref(), value)
    }

    /// Resturns the side at `index` and a refrence to its value.
    ///
    /// # Safety:
    ///
    /// This method dose not perform any bounds checking on `index`. `index`
    /// should be less then `self.len()`.
    pub unsafe fn side_value_unchecked(&self, index: HoleSize) -> (Side<H>, &T)
    where H: Clone {
        let (side, data) = self.unref.side_value_unchecked(index);
        (Side::from_unref(self.hole.clone(), side), data)
    }

    /// Resturns the side at `index` and a mutable refrence to its value.
    ///
    /// # Safety:
    ///
    /// This method dose not perform any bounds checking on `index`. `index`
    /// should be less then `self.len()`.
    pub unsafe fn side_value_unchecked_mut(&mut self, index: HoleSize) -> (Side<H>, &mut T)
    where H: Clone {
        let (side, data) = self.unref.side_value_unchecked_mut(index);
        (Side::from_unref(self.hole.clone(), side), data)
    }

    /// Resturns the side at `index`.
    ///
    /// # Safety:
    ///
    /// This method dose not perform any bounds checking on `index`. `index`
    /// should be less then `self.len()`.
    pub unsafe fn side_unchecked(&mut self, index: HoleSize) -> Side<H>
    where H: Clone {
        let side = self.unref.side_unchecked(index);
        Side::from_unref(self.hole.clone(), side)
    }

    /// Resturns a refrence to the value at `index`.
    ///
    /// # Safety:
    ///
    /// This method dose not perform any bounds checking on `index`. `index`
    /// should be less then `self.len()`.
    pub unsafe fn value_unchecked(&self, index: HoleSize) -> &T {
        self.unref.value_unchecked(index)
    }

    /// Resturns a mutable refrence to the value at `index`.
    ///
    /// # Safety:
    ///
    /// This method dose not perform any bounds checking on `index`. `index`
    /// should be less then `self.len()`.
    pub unsafe fn value_unchecked_mut(&mut self, index: HoleSize) -> &mut T {
        self.unref.value_unchecked_mut(index)
    }

    ///
    pub fn into_meta(self) -> HoleMeta<H, Option<T>>
    where H: Clone {
        unsafe {
            let unref = self.unref.into_meta(self.hole.as_ref());
            HoleMeta::from_unref(self.hole, unref)
        }
    }

    ///
    pub fn into_meta_full(self) -> HoleMeta<H, T>
    where H: Clone {
        unsafe {
            let unref = self.unref.into_meta_full(self.hole.as_ref());
            HoleMeta::from_unref(self.hole, unref)
        }
    }

    ///
    pub unsafe fn into_meta_full_unchecked(self) -> HoleMeta<H, T>
    where H: Clone {
        let unref = self.unref.into_meta_full_unchecked(self.hole.as_ref());
        HoleMeta::from_unref(self.hole, unref)
    }
}
impl<H, T> Clone for SideMap<H, T>
where H: AsRef<Hole> + Clone,
      T: Clone {
    fn clone(&self) -> Self {
        Self {hole: self.hole.clone(), unref: self.unref.clone()}
    }
}
impl<H, T> IntoIterator for SideMap<H, T>
where H: AsRef<Hole> + Clone {
    type IntoIter = IntoSideMapIter<H, T>;
    type Item = (Side<H>, T);

    fn into_iter(self) -> Self::IntoIter {
        IntoSideMapIter {hole: self.hole, iter: self.unref.vec.into_iter()}
    }
}
impl<'a, H, T> IntoIterator for &'a SideMap<H, T>
where H: AsRef<Hole> + Clone {
    type IntoIter = SideMapIter<'a, H, T>;
    type Item = (Side<H>, &'a T);

    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}
impl<'a, H, T> IntoIterator for &'a mut SideMap<H, T>
where H: AsRef<Hole> + Clone {
    type IntoIter = SideMapIterMut<'a, H, T>;
    type Item = (Side<H>, &'a mut T);

    fn into_iter(self) -> Self::IntoIter {
        self.iter_mut()
    }
}
