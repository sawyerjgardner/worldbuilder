use super::*;

///
#[derive(Debug,Clone)]
pub struct SideFloatUnref {
    start: HoleSizeFloat,
    length: HoleSizeFloat,
}
impl SideFloatUnref {

    ///
    pub fn new(start: HoleSizeFloat, length: HoleSizeFloat) -> Self {
        Self {start, length}
    }

    ///
    pub fn start(&self) -> HoleSizeFloat {
        self.start
    }

    ///
    pub fn length(&self) -> HoleSizeFloat {
        self.length
    }

    /// # Safety:
    ///
    /// `hole` must be the Hole this data refers to.
    pub unsafe fn end(&self, hole: &Hole) -> HoleSizeFloat {
        _add_wrap_atf1(self.start(), self.length(), hole.length().into())
    }

    /// # Safety:
    ///
    /// `hole` must be the Hole this data refers to.
    pub unsafe fn round_in(self, hole: &Hole) -> Option<SideUnref> {
        let start_frac = self.start().fract();
        let length_int = (self.length() +start_frac).to_int_unchecked();
        if length_int == HoleSize::new(0) {
            return None
        }
        let mut start = self.start().to_int_unchecked();
        if start_frac == HoleSizeFloat::from(0u8) {
            return Some(SideUnref::new(start, length_int))
        }
        if length_int == HoleSize::new(1) {
            return None
        }
        start = (start +HoleSize::new(1)).rem_euclid(hole.length());
        Some(SideUnref::new(start, length_int -HoleSize::new(1)))
    }

    /// # Safety:
    ///
    /// `hole` must be the Hole this data refers to.
    pub unsafe fn round_out(self, hole: &Hole) -> SideUnref {
        let start = self.start().to_int_unchecked();
        let length = HoleSize::min(
            (self.length() +self.start().fract()).ceil().to_int_unchecked(),
            hole.length()
        );
        SideUnref::new(start, length)
    }

    /// # Safety:
    ///
    /// `hole` must be the Hole this data refers to.
    pub unsafe fn round(self, hole: &Hole) -> Option<SideUnref> {
        let half = HoleSizeFloat::from(1u8).checked_shr(1).unwrap();
        let max = hole.length();
        let mut start = self.start().to_int_unchecked();
        let mut length_f = self.length() +self.start().fract();
        if self.start().fract() >= half {
            if length_f < HoleSizeFloat::from(1u8) {
                return None
            }
            length_f -= HoleSizeFloat::from(1u8);
            start = (start +HoleSize::new(1)).rem_euclid(max);
        }
        let mut length = length_f.to_int_unchecked();
        if length_f.fract() >= half {
            length += HoleSize::new(1);
        } else if length == HoleSize::new(0) {
            return None
        }
        Some(SideUnref::new(start, HoleSize::min(length, max)))
    }

    /// # Safety:
    ///
    /// `hole` must be the Hole this data refers to.
    pub unsafe fn point_start(&self, hole: &Hole) -> Point {
        let index = self.start().to_int_unchecked();
        let max = hole.length();
        Point::lerp(
            &hole.points()[index],
            &hole.points()[(index +HoleSize::new(1)).rem_euclid(max)],
            self.start().fract().to_f32()
        )
    }

    /// # Safety:
    ///
    /// `hole` must be the Hole this data refers to.
    pub unsafe fn point_end(&self, hole: &Hole) -> Point {
        let end = self.end(hole);
        let index = end.to_int_unchecked();
        let max = hole.length();
        Point::lerp(
            &hole.points()[index],
            &hole.points()[(index +HoleSize::new(1)).rem_euclid(max)],
            end.fract().to_f32()
        )
    }

    /// # Safety:
    ///
    /// `hole` must be the Hole this data refers to.
    pub unsafe fn make_point_start(&self, hole: &Hole) -> Point {
        let index = self.start().to_int_unchecked();
        let max = hole.length();
        Point::new_lerp(
            &hole.points()[index],
            &hole.points()[(index +HoleSize::new(1)).rem_euclid(max)],
            self.start().fract().to_f32()
        )
    }

    /// # Safety:
    ///
    /// `hole` must be the Hole this data refers to.
    pub unsafe fn make_point_end(&self, hole: &Hole) -> Point {
        let end = self.end(hole);
        let index = end.to_int_unchecked();
        let max = hole.length();
        Point::new_lerp(
            &hole.points()[index],
            &hole.points()[(index +HoleSize::new(1)).rem_euclid(max)],
            end.fract().to_f32()
        )
    }

    /// # Safety:
    ///
    /// `hole` must be the Hole this data refers to.
    pub unsafe fn inverse(self, hole: &Hole) -> Option<Self> {
        let new_length = HoleSizeFloat::from(hole.length()) -self.length();
        if new_length == HoleSizeFloat::from(0u8) {
            return None
        }
        let new_start = self.end(hole);
        Some(Self::new(new_start, new_length))
    }

    /// # Safety:
    ///
    /// `self` and `other` must both be refering to `hole`.
    pub unsafe fn overlapping(&self, hole: &Hole, other: &SideFloatUnref)
                              -> bool {
        self.contains(hole, other.start()) || other.contains(hole, self.start())
    }

    fn contains(&self, hole: &Hole, position: HoleSizeFloat) -> bool {
        let max = hole.length().into();
        let start = self.start();
        let (end, overflow) = start.overflowing_add(self.length()) ;
        if overflow || end > max {
            start <= position || position < end.wrapping_sub(max)
        } else {
            start <= position && position < end
        }
    }
}
