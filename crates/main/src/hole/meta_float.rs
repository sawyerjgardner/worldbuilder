use super::*;

mod unref;
pub use unref::*;


///
#[derive(RefHole)]
pub struct HoleMetaFloat<H, T>
where H: AsRef<Hole> {
    hole: H,
    unref: HoleMetaFloatUnrefed<T>
}
impl<H, T> HoleMetaFloat<H, T>
where H: AsRef<Hole> {

    /// # Errors
    ///
    /// - WBErrorInvalidHoleMeta::InvalidLength
    ///     `positions` extend outside the length of `hole`.
    ///
    /// - WBErrorInvalidHoleMeta::InvalidOrder
    ///     `positions` must be in order.
    ///
    /// - WBErrorInvalidHoleMeta::EmptyData
    ///     `positions` is empty.
    pub fn from_positions<I>(hole: H, positions: I) -> ResultM<Self>
    where I: Iterator<Item=(HoleSizeFloat, T)> {
        let unref = HoleMetaFloatUnrefed::from_positions(
            hole.as_ref(),
            positions
        )?;
        Ok(Self {hole, unref})
    }

    /// # Safety
    ///
    /// this method assumes `positions` are sorted and in bounds.
    pub unsafe fn from_positions_unchecked<I>(hole: H, positions: I) -> Self
    where I: Iterator<Item=(HoleSizeFloat, T)> {
        let unref = HoleMetaFloatUnrefed::from_positions_unchecked(positions);
        #[cfg(debug_assertions)]
        unref.slice().valid_unref(hole.as_ref()).unwrap();
        Self {hole, unref}
    }

    /// # Errors
    ///
    /// - WBErrorInvalidHoleMeta::InvalidLength
    ///     `lengths` dos not add up to the length of `hole`.
    ///
    /// - WBErrorInvalidHoleMeta::InvalidOrder
    ///     'lengths' contains a value of 0.
    ///
    /// - WBErrorInvalidHoleMeta::EmptyData
    ///     `lengths` is empty.
    pub fn from_lengths<I>(hole: H, start: HoleSizeFloat, lengths: I)
                           -> ResultM<Self>
    where I: Iterator<Item=(HoleSizeFloat, T)> {
        let unref = HoleMetaFloatUnrefed::from_lengths(
            hole.as_ref(),
            start,
            lengths
        )?;
        Ok(Self {hole, unref})
    }

    /// # Safty
    ///
    /// this method assumes the sum of `lengths` is equal to the length of `hole`.
    pub unsafe fn from_lengths_unchecked<I>(hole: H,
                                            start: HoleSizeFloat,
                                            lengths: I) -> Self
    where I: Iterator<Item=(HoleSizeFloat, T)> {
        let unref = HoleMetaFloatUnrefed::from_lengths_unchecked(start, lengths);
        #[cfg(debug_assertions)]
        unref.slice().valid_unref(hole.as_ref()).unwrap();
        Self {hole, unref}
    }

    ///
    pub fn side(&self, index: usize) -> SideFloat<H> 
    where H: Clone {
        unsafe {
            SideFloat::from_unref(
                self.hole(),
                self.unref.slice().side(index, || self.hole.as_ref().length().into())
            )
        }
    }

    ///
    pub fn data(&self, index: usize) -> &T {
        self.unref.data(index)
    }

    ///
    pub fn len(&self) -> usize {
        self.unref.len()
    }
}
