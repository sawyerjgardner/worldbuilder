use super::*;


///
pub enum WBErrorHole {
    /// "attempt to construct a hole with an invalid number of points {:?}",
    InvalidLength(Box<[Point]>),

    /// "hole side out of bounds (max: {}, start: {}, len: {})",
    InvalidSide(HoleSize, HoleSizeFloat, HoleSizeFloat),

    /// "holes do not match ({:?}, {:?})",
    NotEqual(*const Hole, *const Hole),

    /// "hole size out of range (value: {}, max: {})",
    OutOfRange(usize),

    /// "hole sides overlapping ({:?}, hint: {})",
    SideCollision(*const Hole, HoleSize),

    /// "hole data missing"
    NoData
}
impl WBErrorHole {
    /// Quickly assert that two Holes have the same id.
    ///
    /// # Errors
    ///
    /// - WBErrorHole::NotEqual:
    ///     `a` and `b` do not refer to the same Hole.
    pub fn test_holes_equal(a: &Hole, b: &Hole) -> Result<(), WBErrorHole> {
        if a != b {
            return Err(WBErrorHole::NotEqual(a as *const _, b as *const _))
        }
        Ok(())
    }
}
impl WBError for WBErrorHole {}
impl fmt::Debug for WBErrorHole {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use WBErrorHole::*;
        f.write_str(&match self {
            InvalidLength(points) => format!(
                "attempt to construct a hole with an invalid number of points {:?}",
                points
            ),
            InvalidSide(max, start, length) => format!(
                "hole side out of bounds (max: {}, start: {}, len: {})",
                max,
                start,
                length
            ),
            NotEqual(id_1, id_2) => format!(
                "holes do not match ({:?}, {:?})",
                id_1,
                id_2
            ),
            OutOfRange(value) => format!(
                "hole size out of range (value: {}, max: {})",
                value,
                HoleSize::MAX -HoleSize::new(1)
            ),
            SideCollision(hole, hint) => format!(
                "hole sides overlapping ({:?}, hint: {})",
                hole,
                hint
            ),
            NoData => format!(
                "hole data missing"
            )
        })
    }
}
