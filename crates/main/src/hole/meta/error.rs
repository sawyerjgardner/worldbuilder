use std::fmt::Debug;

use super::*;


///
#[derive(Clone)]
pub enum WBErrorInvalidHoleMeta {

    /// "hole is length {}, expected {}",
    InvalidLength(HoleSize, HoleSizeFloat),

    /// "can not build loop from sides (confused at {:?})",
    InvalidLoop(Point),

    /// "side data given in an invalid order"
    InvalidOrder,

    /// "not all sides are from the same Hole"
    MismatchingHoles,

    /// "no data given"
    EmptyData,
}
impl WBError for WBErrorInvalidHoleMeta {}
impl Debug for WBErrorInvalidHoleMeta {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use WBErrorInvalidHoleMeta::*;
        f.write_str(&match self {
            InvalidLength(value, expected) => format!(
                "hole is length {}, expected {}",
                value,
                expected
            ),
            InvalidLoop(p1) => format!(
                "can not build loop from sides (confused at {:?})",
                p1
            ),
            InvalidOrder => format!(
                "side data given in an invalid order"
            ),
            MismatchingHoles => format!(
                "not all sides are from the same Hole"
            ),
            EmptyData => format!(
                "no data given"
            ),
        })
    }
}


///
pub enum WBErrorBuildingHoleMeta<E> {
    ///
    BuilderError(E),

    ///
    Meta(WBErrorInvalidHoleMeta)
}
impl<E> WBErrorBuildingHoleMeta<E> {
    pub fn wrap<T>(r: Result<T,E>) -> Result<T, WBErrorBuildingHoleMeta<E>> {
        r.map_err(|err| WBErrorBuildingHoleMeta::BuilderError(err))
    }
}
impl<E> Clone for WBErrorBuildingHoleMeta<E>
where E: Clone {
    fn clone(&self) -> Self {
        match self {
            Self::BuilderError(x) => Self::BuilderError(x.clone()),
            Self::Meta(x) => Self::Meta(x.clone())
        }
    }
}
impl<E> WBError for WBErrorBuildingHoleMeta<E>
where E: Debug {}
impl<E> Debug for WBErrorBuildingHoleMeta<E>
where E: Debug {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use WBErrorBuildingHoleMeta::*;
        match self {
            BuilderError(x) => Debug::fmt(x,f),
            Meta(x) => Debug::fmt(x,f)
        }
    }
}
impl<T> From<WBErrorInvalidHoleMeta> for WBErrorBuildingHoleMeta<T> {
    fn from(value: WBErrorInvalidHoleMeta) -> Self {
        Self::Meta(value)
    }
}
