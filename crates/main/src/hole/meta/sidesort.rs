use super::*;


pub struct SortSidesTouching<I, H, T>
where I: Iterator<Item=(Side<H>, T)>,
      H: AsRef<Hole> {
    datamap: SimpleMap<usize, (Vec<Point>, T), LineKeyMapper<I,H,T>>,
}
impl<I, H, T> SortSidesTouching<I, H, T>
where I: Iterator<Item=(Side<H>, T)>,
      H: AsRef<Hole> {
    pub fn new(iter: I) -> Self {
        Self {datamap: SimpleMap::from_iter(LineKeyMapper::new(iter))}
    }
    pub fn pop_meta<H2, F, E>(&mut self, hole_builder: F) -> ResultMB<Option<HoleMeta<H2, T>>, E>
    where H2: AsRef<Hole>,
          F: FnOnce(Vec<Point>) -> Result<H2, E> {
        if let Some((first_key, (mut new_points, mut data))) = self.datamap.remove_any_entry() {
            let mut lengths = Vec::new();
            let mut points = ShiftCollector::new();
            loop {
                let next_key = new_points.last().unwrap().uid();
                lengths.push((new_points.len().into(), data));
                points.extend(new_points.into_iter());
                match self.datamap.remove(&next_key) {
                    Some(value) => {
                        (new_points, data) = value
                    },
                    None => break
                }
            };
            let last = points.last().unwrap();
            if last.uid() != first_key {
                return Err(ErrorM::InvalidLoop(last.clone()).into())
            }
            return Ok(Some(HoleMeta::from_lengths(
                hole_builder(points.into_vec()).map_err(|err| {
                    ErrorMB::BuilderError(err)
                })?,
                HoleSize::new(0),
                lengths.into_iter()
            )?));
        }
        Ok(None)
    }
    pub fn into_metas<H2, F, E>(mut self, mut hole_builder: F) -> ResultMB<Vec<HoleMeta<H2, T>>, E>
    where H2: AsRef<Hole>,
          F: FnMut(Vec<Point>) -> Result<H2, E> {
        let mut out = vec![];
        while let Some(f) = self.pop_meta(&mut hole_builder)? {
            out.push(f);
        }
        Ok(out)
    }
}


struct LineKeyMapper<I,H,T>
where I: Iterator<Item=(Side<H>, T)>,
      H: AsRef<Hole> {
    iter: std::iter::Map<I, fn((Side<H>, T)) -> (usize, (Vec<Point>, T))>,
}
impl<I,H,T> LineKeyMapper<I,H,T>
where I: Iterator<Item=(Side<H>, T)>,
      H: AsRef<Hole> {
    fn new(iter: I) -> Self {
        Self {iter: iter.map(Self::side_to_keyline)}
    }
    fn side_to_keyline((side, data): (Side<H>, T)) -> (usize, (Vec<Point>, T)) {
        let mut points = side.all_points();
        (
            points.next().unwrap().uid(),
            (
                points.cloned().collect_vec(),
                data
            )
        )
    }
}
impl<I,H,T> Iterator for LineKeyMapper<I,H,T>
where I: Iterator<Item=(Side<H>, T)>,
      H: AsRef<Hole> {
    type Item = (usize, (Vec<Point>, T));
    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}
