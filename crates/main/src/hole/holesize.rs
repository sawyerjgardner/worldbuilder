use std::ops::{Index, IndexMut};
use std::slice::SliceIndex;
use fixed::*;
use fixed::types::extra::*;
use world_builder_macros::*;

cfg_if::cfg_if!{
    if #[cfg(feature = "holesize-64")] {
        type HoleSizeType = u64;
        type HoleSizeType2 = FixedU128<U64>;
    } else if #[cfg(feature = "holesize-32")] {
        type HoleSizeType = u32;
        type HoleSizeType2 = FixedU64<U32>;
    } else if #[cfg(feature = "holesize-16")] {
        type HoleSizeType = u16;
        type HoleSizeType2 = FixedU32<U16>;
    } else if #[cfg(feature = "holesize-8")] {
        type HoleSizeType = u8;
        type HoleSizeType2 = FixedU16<U8>;
    } else {
        type HoleSizeType = u32;
        type HoleSizeType2 = FixedU64<U32>;
    }
}


/// Represents the length or index into a `Hole`.
///
/// This acts as an unsigned int, just big enough to store the length of a hole.
/// The size of this type can be configured at compile time using the feature 
/// flags `holesize-n`, where n represents the number of bits to use (e.g., 
/// `holesize-16`). Currently, the default is 32 bits, but this may change in 
/// future releases.
#[derive(NewInt, Clone, Copy, Default, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct HoleSize(HoleSizeType);
impl HoleSize {

    ///
    pub fn new(value: u8) -> Self {
        value.into()
    }
}
impl<T> Index<HoleSize> for [T] {
    type Output = <usize as SliceIndex<[T]>>::Output;
    fn index(&self, index: HoleSize) -> &Self::Output {
        self.index(usize::from(index))
    }
}
impl<T> IndexMut<HoleSize> for [T] {
    fn index_mut(&mut self, index: HoleSize) -> &mut Self::Output {
        self.index_mut(usize::from(index))
    }
}
impl TryFrom<HoleSizeFloat> for HoleSize {
    type Error = ();
    fn try_from(value: HoleSizeFloat) -> Result<Self, Self::Error> {
        if value.fract() != HoleSizeFloat::from(0u8) {
            return Err(())
        }
        Ok(value.to_int_unchecked())
    }
}


/// Represents the length or index into a `Hole` as a float.
#[derive(NewFixed, Clone, Copy, Default, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct HoleSizeFloat(HoleSizeType2);
impl HoleSizeFloat {

    ///
    pub fn to_int_unchecked(self) -> HoleSize {
        HoleSize(self.0.to_num())
    }

    ///
    pub fn to_f32(self) -> f32 {
        self.0.to_num()
    }

    ///
    pub fn to_f64(self) -> f64 {
        self.0.to_num()
    }

    ///
    pub fn from_f32(value: f32) -> Self {
        Self(HoleSizeType2::from_num(value))
    }

    ///
    pub fn from_f64(value: f64) -> Self {
        Self(HoleSizeType2::from_num(value))
    }

    /// FIXME: error
    pub fn try_from_f32(value: f32) -> Result<Self, ()> {
        match HoleSizeType2::checked_from_num(value) {
            Some(o) => Ok(Self(o)),
            None => Err(())
        }
    }

    ///
    pub fn try_from_f64(value: f64) -> Result<Self, ()> {
        match HoleSizeType2::checked_from_num(value) {
            Some(o) => Ok(Self(o)),
            None => Err(())
        }
    }
}
impl From<HoleSize> for HoleSizeFloat {
    fn from(value: HoleSize) -> Self {
        Self(HoleSizeType2::from_num(value.0))
    }
}


const _: () = {
    assert!(HoleSize::BITS == HoleSizeFloat::BITS/2);
};



/// add `a` and `b` wrapping at the value of `wrap`
pub(crate) fn _add_wrap_at(a: HoleSize, b: HoleSize, wrap: HoleSize) -> HoleSize {
    let (mut value, overflow) = a.overflowing_add(b);
    if overflow {
        value = value.rem_euclid(wrap).wrapping_sub(wrap)
    }
    value.rem_euclid(wrap)
}


/// add `a` and `b` wrapping at the value of `wrap`
pub(crate) fn _add_wrap_at1(a: HoleSize, b: HoleSize, wrap: HoleSize) -> HoleSize {
    let (mut value, overflow) = a.overflowing_add(b);
    if overflow || value >= wrap {
        value = value.wrapping_sub(wrap)
    }
    value
}


/// add `a` and `b` wrapping at the value of `wrap`
pub(crate) fn _add_wrap_atf(a: HoleSizeFloat, b: HoleSizeFloat, wrap: HoleSizeFloat)
                     -> HoleSizeFloat {
    let (mut value, overflow) = a.overflowing_add(b);
    if overflow {
        value = value.rem_euclid(wrap).wrapping_sub(wrap)
    }
    value.rem_euclid(wrap)
}


/// add `a` and `b` wrapping at the value of `wrap`
pub(crate) fn _add_wrap_atf1(a: HoleSizeFloat, b: HoleSizeFloat, wrap: HoleSizeFloat)
                      -> HoleSizeFloat {
    let (mut value, overflow) = a.overflowing_add(b);
    if overflow || value >= wrap {
        value = value.wrapping_sub(wrap)
    }
    value
}


cfg_if::cfg_if!{ if #[cfg(feature = "pyo3")] {
    use pyo3::*;
    use pyo3::exceptions::PyValueError;
    use pyo3::types::PyAnyMethods;

    impl IntoPy<PyObject> for HoleSize {
        fn into_py(self, py: pyo3::prelude::Python<'_>) -> PyObject {
            usize::from(self).into_py(py)
        }
    }

    impl FromPyObject<'_> for HoleSize {
        fn extract_bound(ob: &Bound<'_, PyAny>) -> PyResult<Self> {
            Self::try_from(ob.extract::<u64>()?).map_err(|err| err.into())
        }
    }

    impl IntoPy<PyObject> for HoleSizeFloat {
        fn into_py(self, py: pyo3::prelude::Python<'_>) -> PyObject {
            self.to_f64().into_py(py)
        }
    }

    impl FromPyObject<'_> for HoleSizeFloat {
        fn extract_bound(ob: &Bound<'_, PyAny>) -> PyResult<Self> {
            Self::try_from_f64(ob.extract()?).map_err(|_| {
                PyValueError::new_err("HoleSizeFloat out of bounds")
            })
        }
    }
}}
