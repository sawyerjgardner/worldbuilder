use super::*;

mod unref;
pub use unref::*;


///
#[derive(Clone,RefHole)]
pub struct SideFloat<H>
where H: AsRef<Hole> {
    hole: H,
    unref: SideFloatUnref,
}
impl<H> SideFloat<H>
where H: AsRef<Hole> {

    /// # Errors
    ///
    /// - WBErrorHole::InvalidSide:
    ///     the values of `start` or `length` are larger then the length of `hole`
    ///     OR `length` is 0.
    ///
    /// # Examples
    ///
    /// ```
    /// use world_builder::*;
    ///
    /// let hole = Hole::new([
    ///     Point::new(0.,0.,0.),
    ///     Point::new(1.,0.,0.),
    ///     Point::new(2.,0.,0.),
    ///     Point::new(3.,0.,0.),
    /// ].into_iter()).unwrap();
    ///
    /// let hs = &HoleSizeFloat::from_f32;
    ///
    /// SideFloat::new(&hole, hs(0.0), hs(1.0)).unwrap();
    /// SideFloat::new(&hole, hs(3.5), hs(2.7)).unwrap();
    /// ```
    pub fn new(hole: H,
               start: HoleSizeFloat,
               length: HoleSizeFloat) -> Result<Self, WBErrorHole> {
        let out = Self {hole, unref: SideFloatUnref::new(start, length)};
        out.valid()?;
        Ok(out)
    }

    /// # Safety
    ///
    /// Calling this method is undefined behavior if `start` or `length` are
    /// out-of-bounds, even if the resulting side is never used.
    pub unsafe fn new_unchecked(hole: H,
                                start: HoleSizeFloat,
                                length: HoleSizeFloat) -> Self {
        let out = Self {hole, unref: SideFloatUnref::new(start, length)};
        #[cfg(debug_assertions)]
        out.valid().unwrap();
        out
    }

    ///
    pub fn start(&self) -> HoleSizeFloat {
        self.unref.start()
    }

    ///
    pub fn end(&self) -> HoleSizeFloat {
        unsafe {
            self.unref.end(self.hole.as_ref())
        }
    }

    ///
    pub fn length(&self) -> HoleSizeFloat {
        self.unref.length()
    }

    /// # Examples
    ///
    /// ```
    /// use world_builder::*;
    ///
    /// let hole = Hole::new([
    ///     Point::new(1.,0.,0.),
    ///     Point::new(2.,0.,0.),
    ///     Point::new(3.,0.,0.),
    ///     Point::new(4.,0.,0.),
    /// ].into_iter()).unwrap();
    ///
    /// let hsf = &HoleSizeFloat::from_f32;
    /// let hs  = &HoleSize::new;
    /// let side1 = SideFloat::new(&hole, hsf(1.0), hsf(2.0)).unwrap();
    /// let side2 = SideFloat::new(&hole, hsf(2.0), hsf(4.0)).unwrap();
    /// let side3 = SideFloat::new(&hole, hsf(2.2), hsf(4.0)).unwrap();
    /// let side4 = SideFloat::new(&hole, hsf(1.7), hsf(2.7)).unwrap();
    /// let side5 = SideFloat::new(&hole, hsf(3.4), hsf(2.1)).unwrap();
    /// assert_eq!(side1.round_in().unwrap(), Side::new(&hole, hs(1), hs(2)).unwrap());
    /// assert_eq!(side2.round_in().unwrap(), Side::new(&hole, hs(2), hs(4)).unwrap());
    /// assert_eq!(side3.round_in().unwrap(), Side::new(&hole, hs(3), hs(3)).unwrap());
    /// assert_eq!(side4.round_in().unwrap(), Side::new(&hole, hs(2), hs(2)).unwrap());
    /// assert_eq!(side5.round_in().unwrap(), Side::new(&hole, hs(0), hs(1)).unwrap());
    /// assert!(SideFloat::new(&hole, hsf(1.0), hsf(0.1)).unwrap().round_in().is_none());
    /// assert!(SideFloat::new(&hole, hsf(2.1), hsf(1.0)).unwrap().round_in().is_none());
    /// assert!(SideFloat::new(&hole, hsf(3.4), hsf(1.5)).unwrap().round_in().is_none());
    /// ```
    pub fn round_in(self) -> Option<Side<H>> {
        unsafe {
            self.unref.round_in(self.hole.as_ref()).map(|side_unref| {
                Side::from_unref(self.hole, side_unref)
            })
        }
    }


    /// # Examples
    ///
    /// ```
    /// use world_builder::*;
    ///
    /// let hole = Hole::new([
    ///     Point::new(1.,0.,0.),
    ///     Point::new(2.,0.,0.),
    ///     Point::new(3.,0.,0.),
    ///     Point::new(4.,0.,0.),
    /// ].into_iter()).unwrap();
    ///
    /// let hsf = &HoleSizeFloat::from_f32;
    /// let hs  = &HoleSize::new;
    /// let side1 = SideFloat::new(&hole, hsf(1.0), hsf(2.0)).unwrap();
    /// let side2 = SideFloat::new(&hole, hsf(2.3), hsf(1.8)).unwrap();
    /// let side3 = SideFloat::new(&hole, hsf(3.9), hsf(0.2)).unwrap();
    /// let side4 = SideFloat::new(&hole, hsf(2.3), hsf(0.1)).unwrap();
    /// let side5 = SideFloat::new(&hole, hsf(1.4), hsf(2.8)).unwrap();
    /// let side6 = SideFloat::new(&hole, hsf(2.5), hsf(4.0)).unwrap();
    /// assert_eq!(side1.round_out(), Side::new(&hole, hs(1), hs(2)).unwrap());
    /// assert_eq!(side2.round_out(), Side::new(&hole, hs(2), hs(3)).unwrap());
    /// assert_eq!(side3.round_out(), Side::new(&hole, hs(3), hs(2)).unwrap());
    /// assert_eq!(side4.round_out(), Side::new(&hole, hs(2), hs(1)).unwrap());
    /// assert_eq!(side5.round_out(), Side::new(&hole, hs(1), hs(4)).unwrap());
    /// assert_eq!(side6.round_out(), Side::new(&hole, hs(2), hs(4)).unwrap());
    /// ```
    pub fn round_out(self) -> Side<H> {
        unsafe {
            let side_unref = self.unref.round_out(self.hole.as_ref());
            Side::from_unref(
                self.hole,
                side_unref
            )
        }
    }

    /// # Examples
    ///
    /// ```
    /// use world_builder::*;
    ///
    /// let hole = Hole::new([
    ///     Point::new(1.,0.,0.),
    ///     Point::new(2.,0.,0.),
    ///     Point::new(3.,0.,0.),
    ///     Point::new(4.,0.,0.),
    /// ].into_iter()).unwrap();
    ///
    /// let hsf = &HoleSizeFloat::from_f32;
    /// let hs  = &HoleSize::new;
    /// let side1 = SideFloat::new(&hole, hsf(1.0), hsf(2.0)).unwrap();
    /// let side2 = SideFloat::new(&hole, hsf(2.2), hsf(1.4)).unwrap();
    /// let side3 = SideFloat::new(&hole, hsf(2.7), hsf(1.0)).unwrap();
    /// let side4 = SideFloat::new(&hole, hsf(3.0), hsf(3.1)).unwrap();
    /// let side5 = SideFloat::new(&hole, hsf(2.5), hsf(4.0)).unwrap();
    /// let side6 = SideFloat::new(&hole, hsf(1.0), hsf(0.5)).unwrap();
    /// assert_eq!(side1.round().unwrap(), Side::new(&hole, hs(1), hs(2)).unwrap());
    /// assert_eq!(side2.round().unwrap(), Side::new(&hole, hs(2), hs(2)).unwrap());
    /// assert_eq!(side3.round().unwrap(), Side::new(&hole, hs(3), hs(1)).unwrap());
    /// assert_eq!(side4.round().unwrap(), Side::new(&hole, hs(3), hs(3)).unwrap());
    /// assert_eq!(side5.round().unwrap(), Side::new(&hole, hs(3), hs(4)).unwrap());
    /// assert_eq!(side6.round().unwrap(), Side::new(&hole, hs(1), hs(1)).unwrap());
    /// assert!(SideFloat::new(&hole, hsf(1.0), hsf(0.4)).unwrap().round().is_none())
    /// ```
    pub fn round(self) -> Option<Side<H>> {
        unsafe {
            self.unref.round(self.hole.as_ref()).map(|side_unref| {
                Side::from_unref(self.hole, side_unref)
            })
        }
    }

    ///
    pub fn point_start(&self) -> Point {
        unsafe {
            self.unref.point_start(self.hole.as_ref())
        }
    }

    ///
    pub fn point_end(&self) -> Point {
        unsafe {
            self.unref.point_end(self.hole.as_ref())
        }
    }

    ///
    pub fn make_point_start(&self) -> Point {
        unsafe {
            self.unref.make_point_start(self.hole.as_ref())
        }
    }

    ///
    pub fn make_point_end(&self) -> Point {
        unsafe {
            self.unref.make_point_end(self.hole.as_ref())
        }
    }

    /// # Examples
    ///
    /// ```
    /// use world_builder::*;
    ///
    /// let hole = Hole::new([
    ///     Point::new(1.,0.,0.),
    ///     Point::new(2.,0.,0.),
    ///     Point::new(3.,0.,0.),
    ///     Point::new(4.,0.,0.),
    /// ].into_iter()).unwrap();
    ///
    /// let hs = &HoleSizeFloat::from_f32;
    ///
    /// let side1 = SideFloat::new(&hole, hs(0.0), hs(1.0)).unwrap();
    /// let side2 = SideFloat::new(&hole, hs(3.5), hs(0.5)).unwrap();
    /// let side3 = SideFloat::new(&hole, hs(2.5), hs(3.0)).unwrap();
    /// let side4 = SideFloat::new(&hole, hs(2.25), hs(0.25)).unwrap();
    /// assert!(SideFloat::new(&hole, hs(0.0), hs(4.0)).unwrap().inverse().is_none());
    /// assert!(SideFloat::new(&hole, hs(2.2), hs(4.0)).unwrap().inverse().is_none());
    /// assert!(side1.clone().inverse().unwrap() == SideFloat::new(&hole, hs(1.0), hs(3.0)).unwrap());
    /// assert!(side2.clone().inverse().unwrap() == SideFloat::new(&hole, hs(0.0), hs(3.5)).unwrap());
    /// assert!(side3.clone().inverse().unwrap() == SideFloat::new(&hole, hs(1.5), hs(1.0)).unwrap());
    /// assert!(side4.clone().inverse().unwrap() == SideFloat::new(&hole, hs(2.5), hs(3.75)).unwrap());
    /// assert!(!side1.clone().inverse().unwrap().overlapping(&side1).unwrap());
    /// assert!(!side2.clone().inverse().unwrap().overlapping(&side2).unwrap());
    /// assert!(!side3.clone().inverse().unwrap().overlapping(&side3).unwrap());
    /// assert!(!side4.clone().inverse().unwrap().overlapping(&side4).unwrap());
    /// ```
    pub fn inverse(self) -> Option<Self> {
        let hole = self.hole;
        unsafe {
            self.unref.inverse(hole.as_ref()).map(|unref| Self {hole,unref})
        }
    }

    /// # Errors
    ///
    /// - WBErrorHole::NotEqual:
    ///     Both sides refer to diffrent holes.
    ///
    /// # Examples
    ///
    /// ```
    /// use world_builder::*;
    ///
    /// let hole = Hole::new([
    ///     Point::new(1.,0.,0.),
    ///     Point::new(2.,0.,0.),
    ///     Point::new(3.,0.,0.),
    ///     Point::new(4.,0.,0.),
    /// ].into_iter()).unwrap();
    ///
    /// let hs = &HoleSizeFloat::from_f32;
    ///
    /// let side = SideFloat::new(&hole, HoleSizeFloat::from_f32(3.8), HoleSizeFloat::from_f32(0.4)).unwrap();
    /// assert!( side.overlapping(&SideFloat::new(&hole, hs(0.0), hs(0.1)).unwrap()).unwrap());
    /// assert!( side.overlapping(&SideFloat::new(&hole, hs(0.1), hs(2.0)).unwrap()).unwrap());
    /// assert!( side.overlapping(&SideFloat::new(&hole, hs(3.5), hs(0.5)).unwrap()).unwrap());
    /// assert!(!side.overlapping(&SideFloat::new(&hole, hs(1.5), hs(1.0)).unwrap()).unwrap());
    /// ```
    pub fn overlapping<H2>(&self, other: &SideFloat<H2>)
                           -> Result<bool, WBErrorHole>
    where H2: AsRef<Hole> {
        let hole = self.hole.as_ref();
        WBErrorHole::test_holes_equal(hole, other.hole.as_ref())?;
        unsafe {Ok(self.unref.overlapping(hole, &other.unref))}
    }

    /// # Safety
    ///
    /// Calling this method with sides that do not reference the same hole
    /// results in undefined behavior.
    pub unsafe fn overlapping_unchecked<H2>(&self, other: &SideFloat<H2>) -> bool
    where H2: AsRef<Hole> {
        let hole = self.hole.as_ref();
        #[cfg(debug_assertions)]
        WBErrorHole::test_holes_equal(hole, other.hole.as_ref()).unwrap();
        self.unref.overlapping(hole, &other.unref)
    }

    fn valid(&self) -> Result<(), WBErrorHole> {
        let max = HoleSizeFloat::from(self.hole.as_ref().length());
        if self.start() >= max
            || self.length() > max
            || self.length() == HoleSizeFloat::from(0u8) {
            return Err(WBErrorHole::InvalidSide(
                max.to_int_unchecked(),
                self.start(),
                self.length()
            ))
        }
        Ok(())
    }
}
impl<H> fmt::Debug for SideFloat<H>
where H: AsRef<Hole> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("SideFloat")
            .field("start", &self.start())
            .field("length", &self.length())
            .field("hole", &self.hole.as_ref())
            .finish()
    }
}
impl<H> PartialEq for SideFloat<H>
where H: AsRef<Hole> {
    fn eq(&self, other: &Self) -> bool {
        self.hole.as_ref() == other.hole.as_ref()
            && self.start() == other.start()
            && self.length() == other.length()
    }
}
