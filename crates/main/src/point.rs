use std::fmt;
use std::hash::{Hash, Hasher};
use std::sync::Arc;
use glam::Vec3;
use world_builder_macros::preprocess_pymethods;

mod direction;
mod plane;
pub use direction::*;
pub use plane::*;


/// # Examples
///
/// ```
/// use world_builder::Point;
///
/// let p1 = Point::new(0.,0.,0.);
/// assert!(p1 == p1);
/// assert!(p1 == p1.clone());
/// assert!(p1 != Point::new(1.,0.,0.));
/// assert!(p1 != Point::new(0.,0.,0.));
/// ```
#[cfg_attr(feature="pyo3", pyo3::pyclass(frozen,eq,ord,hash))]
#[derive(Clone)]
pub struct Point {
    data: Arc<Vec3>
}
#[preprocess_pymethods(feature="pyo3")]
impl Point {

    /// # Examples
    ///
    /// ```
    /// use world_builder::Point;
    ///
    /// Point::new(0.,0.,0.);
    /// Point::new(1.,4.,3.8);
    /// ```
    #[new]
    pub fn new(x:f32, y:f32, z:f32) -> Self {
        Self::from_vec(Vec3::new(x,y,z))
    }

    /// # Example
    ///
    /// ```
    /// use world_builder::Point;
    ///
    /// let p1 = Point::new(1.,2.,0.);
    /// let p2 = Point::new(2.,4.,4.);
    ///
    /// assert_eq!(p1.lerp(&p2, 0.5).pos(),  [1.5,  3.,  2.]);
    /// assert_eq!(p1.lerp(&p2, 0.25).pos(), [1.25, 2.5, 1.]);
    /// assert_eq!(p1.lerp(&p2, 0.), p1);
    /// assert_eq!(p1.lerp(&p2, 1.), p2);
    /// ```
    pub fn lerp(&self, p2: &Self, s: f32) -> Self {
        if s == 0. {
            return self.clone()
        }
        if s == 1. {
            return p2.clone()
        }
        self.new_lerp(p2, s)
    }

    /// # Example
    ///
    /// ```
    /// use world_builder::Point;
    ///
    /// let p1 = Point::new(1.,2.,0.);
    /// let p2 = Point::new(2.,4.,4.);
    ///
    /// assert_eq!(p1.new_lerp(&p2, 0.5).pos(),  [1.5,  3.,  2.]);
    /// assert_eq!(p1.new_lerp(&p2, 0.25).pos(), [1.25, 2.5, 1.]);
    /// assert_ne!(p1.new_lerp(&p2, 0.), p1);
    /// assert_ne!(p1.new_lerp(&p2, 0.), p2);
    /// assert_ne!(p1.new_lerp(&p2, 1.), p1);
    /// assert_ne!(p1.new_lerp(&p2, 1.), p2);
    /// ```
    pub fn new_lerp(&self, p2: &Self, s: f32) -> Self {
        Self::from_vec(Vec3::lerp(*self.pos_vec(), *p2.pos_vec(), s))
    }

    /// # Examples
    ///
    /// ```
    /// use world_builder::Point;
    ///
    /// let point = Point::new(1.,2.,4.);
    /// assert_eq!(point.pos(), [1.,2.,4.]);
    /// ```
    pub fn pos(&self) -> [f32;3] {
        (*self.data).into()
    }

    /// # Example
    ///
    /// ```
    /// use world_builder::Point;
    ///
    /// let a = Point::new(0.,0.,0.);
    /// let b = Point::new(0.,0.,0.);
    ///
    /// assert_eq!(a.uid(), a.uid());
    /// assert_ne!(a.uid(), b.uid());
    /// ```
    pub fn uid(&self) -> usize {
        Arc::as_ptr(&self.data) as usize
    }

    /// Display
    fn __str__(&self) -> String {
        format!("{}", self)
    }
    /// Debug
    fn __repr__(&self) -> String {
        format!("{:?}", self)
    }
}
impl Point {
    fn pos_vec(&self) -> &Vec3 {
        &self.data
    }
    fn from_vec(v: Vec3) -> Self {
        Self {data: Arc::new(v)}
    }
}
impl Hash for Point {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.uid().hash(state);
    }
}
impl PartialEq for Point {
    fn eq(&self, other: &Self) -> bool {
        PartialEq::eq(&self.uid(), &other.uid())
    }
}
impl Eq for Point {}
impl PartialOrd for Point {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        PartialOrd::partial_cmp(&self.uid(), &other.uid())
    }
}
impl Ord for Point {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        Ord::cmp(&self.uid(), &other.uid())
    }
}
impl fmt::Display for Point {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let pos = self.pos();
        f.debug_tuple("")
            .field(&pos[0])
            .field(&pos[1])
            .field(&pos[2])
            .finish()
    }
}
impl fmt::Debug for Point {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_tuple("Point")
            .field(&Arc::as_ptr(&self.data))
            .field(&self.pos())
            .finish()
    }
}


cfg_if::cfg_if!{ if #[cfg(feature = "pyo3")] {
    use pyo3::*;

    impl IntoPy<PyObject> for &Point {
        fn into_py(self, py: pyo3::prelude::Python<'_>) -> PyObject {
            self.clone().into_py(py)
        }
    }
}}
