use itertools::Itertools;

use crate::hole::*;
use crate::Point;

mod unref;
mod walker;
mod error;
pub use unref::*;
pub use walker::*;
pub use error::*;


pub struct TouchingGraph<H>
where H: AsRef<Hole> {
    holes: Vec<H>,
    unref: TouchingGraphUnref
}
impl<H> TouchingGraph<H>
where H: AsRef<Hole> {

    ///
    pub fn new(holes: Vec<H>) -> Self {
        Self {
            unref: TouchingGraphUnref::new(
                &holes.iter().map(|o| o.as_ref()).collect_vec()
            ),
            holes
        }
    }

    ///
    pub fn holes_len(&self) -> usize {
        self.unref.holes_len()
    }

    ///
    pub fn sides_len(&self, hole_index: usize) -> Option<HoleSize> {
        self.unref.sides_len(hole_index)
    }

    ///
    pub fn valid_index(&self, index: TouchingIndex) -> bool {
        self.unref.valid_index(index)
    }

    ///
    pub fn exposed_points(&self) -> Vec<Vec<Point>> {
        unsafe {
            self.unref.exposed_points(&self.holes.iter().map(|o| o.as_ref()).collect_vec())
        }
    }

    ///
    pub fn walker(&self, index: TouchingIndex) -> Option<TouchingWalker<H>> {
        TouchingWalker::new(self, index)
    }

    ///
    pub fn side(&self, index: TouchingIndex) -> Option<Side<H>>
    where H: Clone {
        self.unref.side(index).map(|side| {
            let hole = self.holes[index.hole_index].clone();
            unsafe {
                Side::from_unref(hole, side)
            }
        })
    }

    ///
    pub fn touching_index(&self, index: TouchingIndex) -> Option<TouchingIndex> {
        self.unref.touching_index(index)
    }

    ///
    pub fn sidemap(&self) -> Vec<SideMap<H, TouchingIndex>>
    where H: Clone {
        let holes = self.holes.iter().cloned();
        let data = self.unref.sidemap().into_iter();
        debug_assert!(holes.len() == data.len());
        Iterator::zip(holes, data).map(|(hole, sidemap)| {
            unsafe {SideMap::from_unref(hole, sidemap)}
        }).collect()
    }

    ///
    pub fn into_sidemap(self) -> Vec<SideMap<H, TouchingIndex>> {
        let holes = self.holes.into_iter();
        let data = self.unref.into_sidemap().into_iter();
        debug_assert!(holes.len() == data.len());
        Iterator::zip(holes, data).map(|(hole, sidemap)| {
            unsafe {SideMap::from_unref(hole, sidemap)}
        }).collect()
    }


    ///
    pub unsafe fn sides_len_unchecked(&self, index: usize) -> HoleSize {
        self.unref.sides_len_unchecked(index)
    }

    ///
    pub unsafe fn walker_unchecked(&self, index: TouchingIndex) -> TouchingUnrefWalker {
        self.unref.walker_unchecked(index)
    }

    ///
    pub unsafe fn side_unchecked(&self, index: TouchingIndex) -> Side<H>
    where H: Clone {
        debug_assert!(self.valid_index(index));
        Side::from_unref(
            self.holes.get_unchecked(index.hole_index).clone(),
            self.unref.side_unchecked(index)
        )
    }

    ///
    pub unsafe fn touching_index_unchecked(&self, index: TouchingIndex) -> TouchingIndex {
        self.unref.touching_index_unchecked(index)
    }

    ///
    pub fn _valid(&self) -> bool {
        self.unref._valid()
    }
}
impl<H> TouchingGraph<H>
where H: AsRef<Hole> {
    pub fn as_unref(&self) -> &TouchingGraphUnref {
        &self.unref
    }
    pub unsafe fn as_unref_mut(&mut self) -> &mut TouchingGraphUnref {
        &mut self.unref
    }
    pub fn into_unref(self) -> TouchingGraphUnref {
        self.unref
    }
    pub fn as_holes(&self) -> &Vec<H> {
        &self.holes
    }
    pub unsafe fn as_holes_mut(&mut self) -> &mut Vec<H> {
        &mut self.holes
    }
    pub fn into_holes(self) -> Vec<H> {
        self.holes
    }
    pub unsafe fn from_unref(holes: Vec<H>, unref: TouchingGraphUnref) -> Self {
        Self {holes, unref}
    }
    pub fn unref(&self) -> TouchingGraphUnref {
        self.as_unref().clone()
    }
    pub fn holes(&self) -> Vec<H>
    where H: Clone {
        self.as_holes().clone()
    }

    pub fn as_hole(&self, index: usize) -> Option<&H> {
        self.as_holes().get(index)
    }
    pub unsafe fn as_hole_mut(&mut self, index: usize) -> Option<&mut H> {
        self.as_holes_mut().get_mut(index)
    }
    pub fn into_hole(self, index: usize) -> Option<H> {
        if index >= self.holes.len() {
            return None
        }
        Some(self.into_holes().remove(index))
    }
    pub fn hole(&self, index: usize) -> Option<H>
    where H: Clone {
        self.as_hole(index).cloned()
    }
    pub unsafe fn as_hole_unchecked(&self, index: usize) -> &H {
        debug_assert!(index < self.holes.len());
        self.holes.get_unchecked(index)
    }
    pub unsafe fn as_hole_unchecked_mut(&mut self, index: usize) -> &mut H {
        debug_assert!(index < self.holes.len());
        self.holes.get_unchecked_mut(index)
    }
    pub unsafe fn into_hole_unchecked(self, index: usize) -> H {
        debug_assert!(index < self.holes.len());
        self.into_holes().remove(index)
    }
    pub unsafe fn hole_unchecked(&self, index: usize) -> H
    where H: Clone {
        self.as_hole_unchecked(index).clone()
    }
}
