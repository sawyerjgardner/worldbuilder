use std::mem::{self, MaybeUninit};
use itertools::Itertools;

mod common;
pub use common::*;


pub struct SimpleMap<K,V,I>
where I: Iterator<Item=(K,V)> {
    buffer: Vec<(K,V)>,
    iter: I
}
impl<K,V,I> SimpleMap<K,V,I>
where I: Iterator<Item=(K,V)> {
    pub fn from_iter(iter: I) -> Self {
        Self {buffer: Vec::new(), iter}
    }
    pub fn remove<Q>(&mut self, key: &Q) -> Option<V>
    where K: PartialEq<Q> {
        if let Some(index) = self.buffer.iter().position(|(k,_)| k == key) {
            return Some(self.buffer.remove(index).1)
        }
        self.search_iter(|(k,_)| k == key)
    }
    pub fn remove_any_entry(&mut self) -> Option<(K,V)> {
        self.buffer.pop().or_else(|| self.iter.next())
    }
    fn search_iter<F>(&mut self, f: F) -> Option<V>
    where F: Fn(&I::Item) -> bool {
        while let Some(x) = self.iter.next() {
            if f(&x) {
                return Some(x.1)
            }
            self.buffer.push(x);
        }
        None
    }
}


/// HACK: this saves needing to move every element one after creating the vector.
pub struct ShiftCollector<T> {
    vec: Vec<MaybeUninit<T>>
}
impl<T> ShiftCollector<T> {
    pub fn new() -> Self {
        Self { vec: vec![MaybeUninit::uninit()] }
    }
    pub fn len(&self) -> usize {
        self.vec.len() -1
    }
    pub fn push(&mut self, val: T) {
        self.vec.push(MaybeUninit::new(val))
    }
    pub fn extend<I>(&mut self, iter: I)
    where I: Iterator<Item=T> {
        for x in iter {
            self.push(x)
        }
    }
    pub fn into_vec(mut self) -> Vec<T> {
        self.move_end_to_start();
        self.assume_all_init()
    }
    pub fn last(&self) -> Option<&T> {
        if self.len() == 0 {
            return None
        }
        Some(unsafe {self.vec.last().unwrap().assume_init_ref()})
    }
    fn move_end_to_start(&mut self) {
        let first = unsafe {self.vec.pop().unwrap().assume_init()};
        self.vec.first_mut().unwrap().write(first);
    }
    fn assume_all_init(self) -> Vec<T>  {
        self.take().into_iter().map(|o| unsafe {
            MaybeUninit::assume_init(o)
        }).collect_vec()
    }
    fn take(mut self) -> Vec<MaybeUninit<T>> {
        let foo = mem::replace(&mut self.vec, Vec::new());
        mem::forget(self);
        foo
    }
}
impl<T> Drop for ShiftCollector<T> {
    fn drop(&mut self) {
        for x in self.vec.split_at_mut(1).1 {
            unsafe { x.assume_init_drop() }
        }
    }
}
