use crate::error::*;
use super::*;


///
#[derive(Clone)]
pub enum WBErrorTouching {
    /// "Touching Index is out of bounds (graph: {:?}, hole_index: {}, side_index: {})"
    OutOfBounds(*const TouchingGraphUnref, TouchingIndex),

    /// "Operation took too long"
    TimeOut,
}
impl WBError for WBErrorTouching {}
impl std::fmt::Debug for WBErrorTouching {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use WBErrorTouching::*;
        f.write_str(&match self {
            OutOfBounds(ptr, index) => format!(
                "Touching Index is out of bounds (graph: {:?}, hole_index: {}, side_index: {})",
                ptr,
                index.hole_index,
                index.side_index
            ),
            TimeOut => format!("Operation took too long")
        })
    }
}
