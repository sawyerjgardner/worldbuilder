use super::*;
use crate::Point;


///
#[derive(Clone)]
pub struct TouchingGraphUnref {
    pub(super) nodes: Vec<SideMapUnref<TouchingIndex>>
}
impl TouchingGraphUnref {

    ///
    pub fn new(holes: &[&Hole]) -> Self {
        let mut nodes = vec![SideMapUnref::new();holes.len()];
        for hole1i in 0..holes.len() {
            for hole2i in (hole1i+1)..holes.len() {
                for (side1, side2) in holes[hole1i].sides_touching(&holes[hole2i]) {
                    unsafe {
                        let side1i = nodes[hole1i].insert_unchecked(
                            side1.unref(),
                            TouchingIndex::new(hole2i, HoleSize::MAX)
                        );
                        for updatei in (usize::from(side1i)+1)..nodes[hole1i].len().into() {
                            let other_index = nodes[hole1i].value(updatei.into()).unwrap().clone();
                            let index_back = &mut nodes[other_index.hole_index]
                                .value_mut(other_index.side_index)
                                .unwrap();
                            if index_back.hole_index != hole1i {
                                panic!("{:?}", 0)
                            }
                            if index_back.side_index != HoleSize::from(updatei-1) {
                                panic!("{:?}", 0)
                            }
                            index_back.side_index += HoleSize::new(1);
                        }
                        let side2i = nodes[hole2i].insert_unchecked(
                            side2.unref(),
                            TouchingIndex::new(hole1i, side1i)
                        );
                        for updatei in (usize::from(side2i)+1)..nodes[hole2i].len().into() {
                            let other_index = nodes[hole2i].value(updatei.into()).unwrap().clone();
                            let index_back = &mut nodes[other_index.hole_index]
                                .value_mut(other_index.side_index)
                                .unwrap();
                            if index_back.hole_index != hole2i {
                                panic!("{:?}", 0)
                            }
                            if index_back.side_index != HoleSize::from(updatei-1) {
                                panic!("{:?}", 0)
                            }
                            index_back.side_index += HoleSize::new(1);
                        }
                        nodes[hole1i].value_mut(side1i).unwrap().side_index = side2i;
                    }
                }
            }
        }
        // FIXME: hole touching self
        for h in holes {
            if h.touching(h) {
                panic!("holes are touching themselfs")
            }
        }
        Self {nodes}
    }

    ///
    pub fn holes_len(&self) -> usize {
        self.nodes.len()
    }

    ///
    pub fn sides_len(&self, hole_index: usize) -> Option<HoleSize> {
        self.nodes.get(hole_index).map(|o| o.len())
    }

    ///
    pub fn valid_index(&self, index: TouchingIndex) -> bool {
        self.nodes.get(index.hole_index).is_some_and(|o| {
            index.side_index < o.len()
        })
    }

    ///
    pub unsafe fn exposed_points(&self, holes: &[&Hole]) -> Vec<Vec<Point>> {
        assert!(self._valid());
        let mut gaps = self.nodes.iter().zip(holes).map(|(map, hole)| {
            unsafe {map.gaps(hole)}
        }).collect_vec();
        for hole_i in 0..gaps.len() {
            let gapmap = gaps.get_mut(hole_i).unwrap();
            if gapmap.len() != HoleSize::new(0) {
                let val = gapmap.value_mut(gapmap.len() -HoleSize::new(1)).unwrap();
                if *val == self.nodes.get(hole_i).unwrap().len() {
                    *val = HoleSize::new(0)
                }
            }
        }
        let mut out = Vec::new();
        for hole_i in 0..gaps.len() {
            let hole = *holes.get(hole_i).unwrap();
            if self.nodes.get(hole_i).unwrap().is_empty() {
                out.push(hole.points().into());
                continue;
            }
            while let Some((side, side_i)) = gaps.get_mut(hole_i).unwrap().pop() {
                let mut points = side.end_points(hole).cloned().collect_vec();
                let end_point = side.first_point(hole);
                let end_index = TouchingIndex::new(hole_i, side_i);
                let Some(mut walker) = self.walker(end_index) else {
                    panic!(
                        "invalid walker: {:?} ({}, {:?})",
                        end_index,
                        self.holes_len(),
                        self.sides_len(end_index.hole_index),
                    )
                };
                let mut tries = 0;
                loop {
                    tries += 1;
                    walker.pivit_back();
                    let index = walker.index();
                    let gapsmap = gaps.get_mut(index.hole_index).unwrap();
                    if let Some(remove_index) = gapsmap.values().position(|o| *o == index.side_index) {
                        let (side, side_i) = gapsmap.remove(remove_index.into()).unwrap();
                        let hole = *holes.get(index.hole_index).unwrap();
                        assert!(side_i == index.side_index);
                        assert!(points.last().unwrap() == side.first_point(hole));
                        points.extend(side.end_points(hole).cloned());
                        continue;
                    }
                    if index == end_index {
                        break;
                    }
                    if tries >= 100 {
                        panic!("searching too long")
                    }
                }
                assert!(points.last().unwrap() == end_point);
                out.push(points);
            }
        }
        out
    }

    ///
    pub fn walker(&self, index: TouchingIndex) -> Option<TouchingUnrefWalker> {
        TouchingUnrefWalker::new(self, index)
    }

    ///
    pub fn side(&self, index: TouchingIndex) -> Option<SideUnref> {
        self.nodes.get(index.hole_index).and_then(|o| o.side(index.side_index))
    }

    ///
    pub fn touching_index(&self, index: TouchingIndex) -> Option<TouchingIndex> {
        self.nodes.get(index.hole_index).and_then(|o| o.value(index.side_index).cloned())
    }

    ///
    pub fn sidemap(&self) -> Vec<SideMapUnref<TouchingIndex>> {
        self.nodes.clone()
    }

    ///
    pub fn into_sidemap(self) -> Vec<SideMapUnref<TouchingIndex>> {
        self.nodes
    }


    ///
    pub unsafe fn sides_len_unchecked(&self, index: usize) -> HoleSize {
        self.nodes.get_unchecked(index).len()
    }

    ///
    pub unsafe fn walker_unchecked(&self, index: TouchingIndex) -> TouchingUnrefWalker {
        TouchingUnrefWalker::new_unchecked(self, index)
    }

    ///
    pub unsafe fn side_unchecked(&self, index: TouchingIndex) -> SideUnref {
        self.nodes.get_unchecked(index.hole_index).side_unchecked(index.side_index)
    }

    ///
    pub unsafe fn touching_index_unchecked(&self, index: TouchingIndex) -> TouchingIndex {
        *self.nodes.get_unchecked(index.hole_index).value_unchecked(index.side_index)
    }

    ///
    pub fn hole<'a>(&self, holes: &[&'a Hole], index: TouchingIndex) -> Option<&'a Hole> {
        holes.get(index.hole_index).cloned()
    }

    ///
    pub fn _valid(&self) -> bool {
        for node in &self.nodes {
            for index in node.values().cloned() {
                if !self.valid_index(index) {
                    return false
                }
            }
        }
        for i in 0..self.nodes.len() {
            for index in self.nodes[i].values().cloned() {
                let other_index = self.touching_index(index).unwrap();
                if self.touching_index(other_index).unwrap() != index {
                    return false
                }
                if other_index == index {
                    return false
                }
            }
        }
        true
    }
}
