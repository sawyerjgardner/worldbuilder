use super::*;


///
#[derive(Clone)]
pub struct TouchingWalker<'a, H>
where H: AsRef<Hole> {
    graph: &'a TouchingGraph<H>,
    index: TouchingIndex
}
impl<'a, H> TouchingWalker<'a, H>
where H: AsRef<Hole> {
    ///
    pub fn new(graph: &'a TouchingGraph<H>, index: TouchingIndex)
               -> Option<Self> {
        if !graph.valid_index(index) {
            return None
        };
        Some(Self {graph, index})
    }

    ///
    pub unsafe fn new_unchecked(graph: &'a TouchingGraph<H>,
                                index: TouchingIndex)
                                -> Self {
        debug_assert!(graph.valid_index(index));
        Self {graph, index}
    }

    ///
    pub fn index(&self) -> TouchingIndex {
        self.index.clone()
    }

    ///
    pub fn graph(&self) -> &'a TouchingGraph<H> {
        self.graph
    }

    ///
    pub fn side(&self) -> Side<H>
    where H: Clone {
        unsafe {
            self.graph.side_unchecked(self.index)
        }
    }

    ///
    pub fn back(&mut self) {
        let mut walker = unsafe {self.graph.unref.walker_unchecked(self.index)};
        walker.back();
        self.index = walker.index;
    }

    ///
    pub fn forword(&mut self) {
        let mut walker = unsafe {self.graph.unref.walker_unchecked(self.index)};
        walker.forword();
        self.index = walker.index;
    }

    ///
    pub fn jump(&mut self) {
        let mut walker = unsafe {self.graph.unref.walker_unchecked(self.index)};
        walker.jump();
        self.index = walker.index;
    }

    ///
    pub fn pivit_back(&mut self) {
        self.jump();
        self.forword();
    }

    ///
    pub fn pivit_frunt(&mut self) {
        self.jump();
        self.back();
    }
}
impl<H> std::fmt::Debug for TouchingWalker<'_, H>
where H: AsRef<Hole> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("TouchingWalker")
            .field("graph", &(self.graph as *const _))
            .field("hole_index", &self.index.hole_index)
            .field("side_index", &self.index.side_index)
            .finish()
    }
}


///
#[derive(Clone)]
pub struct TouchingUnrefWalker<'a> {
    graph: &'a TouchingGraphUnref,
    index: TouchingIndex
}
impl<'a> TouchingUnrefWalker<'a> {
    ///
    pub fn new(graph: &'a TouchingGraphUnref, index: TouchingIndex) -> Option<Self> {
        if !graph.valid_index(index) {
            return None
        };
        Some(Self {graph, index})
    }

    ///
    pub unsafe fn new_unchecked(graph: &'a TouchingGraphUnref,
                                index: TouchingIndex)
                                -> Self {
        debug_assert!(graph.valid_index(index));
        Self {graph, index}
    }

    ///
    pub fn index(&self) -> TouchingIndex {
        self.index.clone()
    }

    ///
    pub fn graph(&self) -> &'a TouchingGraphUnref {
        self.graph
    }

    ///
    pub fn side(&self) -> SideUnref {
        unsafe {
            self.graph.side_unchecked(self.index)
        }
    }

    ///
    pub fn back(&mut self) {
        let mut side_index = self.index.side_index;
        if side_index == HoleSize::new(0) {
            side_index = self.graph.nodes[self.index.hole_index].len();
        }
        self.index.side_index = side_index -HoleSize::new(1)
    }

    ///
    pub fn forword(&mut self) {
        let side_max = self.graph.nodes[self.index.hole_index].len();
        self.index.side_index = (self.index.side_index +HoleSize::new(1)).rem_euclid(side_max);
    }

    ///
    pub fn jump(&mut self) {
        unsafe {
            self.index = self.graph.touching_index_unchecked(self.index.clone())
        }
    }

    ///
    pub fn pivit_back(&mut self) {
        self.jump();
        self.forword();
    }

    ///
    pub fn pivit_frunt(&mut self) {
        self.jump();
        self.back();
    }
}
impl std::fmt::Debug for TouchingUnrefWalker<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("TouchingUnrefWalker")
            .field("graph", &(self.graph as *const _))
            .field("hole_index", &self.index.hole_index)
            .field("side_index", &self.index.side_index)
            .finish()
    }
}


///
#[cfg_attr(feature="pyo3", pyo3::pyclass())]
#[derive(Clone,Copy,Debug,PartialEq,Eq,PartialOrd,Ord)]
pub struct TouchingIndex {
    ///
    pub hole_index: usize,

    ///
    pub side_index: HoleSize
}
impl TouchingIndex {
    ///
    pub fn new(hole_index: usize, side_index: HoleSize) -> Self {
        Self {hole_index, side_index}
    }
}
#[cfg(feature="pyo3")]
#[pyo3::pymethods]
impl TouchingIndex {
    #[new]
    fn py_new(index: (usize, HoleSize)) -> Self {
        Self::new(index.0, index.1)
    }
    #[getter(hole_index)]
    fn py_hole_index(&self) -> usize {
        self.hole_index
    }
    #[setter(hole_index)]
    fn py_set_hole_index(&mut self, value: usize) {
        self.hole_index = value;
    }
    #[getter(side_index)]
    fn py_side_index(&self) -> HoleSize {
        self.side_index
    }
    #[setter(side_index)]
    fn py_set_side_index(&mut self, value: HoleSize) {
        self.side_index = value;
    }
    /// Clone
    #[pyo3(name="clone")]
    fn py_clone(&self) -> Self {
        self.clone()
    }
    /// Debug
    fn __repr__(&self) -> String {
        format!("{:?}", self)
    }
    /// Ord
    fn __richcmp__(&self, other: &Self, op: pyo3::class::basic::CompareOp) -> bool {
        op.matches(self.cmp(other))
    }
}
