use itertools::Itertools;
use std::fmt::Debug;


#[derive(Debug,Clone)]
pub struct CommonSegment {
    pub a_start: usize,
    pub b_start: usize,
    pub length: usize
}
impl CommonSegment {
    pub fn segments_loop<A,B>(a: A, b: B) -> Vec<Self>
    where A: ExactSizeIterator + DoubleEndedIterator + Clone,
          B: ExactSizeIterator + DoubleEndedIterator + Clone,
          A::Item: PartialEq<B::Item>,
          std::iter::Rev<A>: Clone,
          std::iter::Rev<B>: Clone {
        let a_max = a.len();
        let b_max = b.len();
        let mut middle_iter = CommonIter::new(a, b);
        if let Some(mut last) = middle_iter.next_back() {
            if let Some(mut first) = middle_iter.next() {
                // NOTE: fix seam from `a`
                if last.try_join_over_seam_a(&first, a_max, b_max) {
                    match middle_iter.next() {
                        Some(next) => first = next,
                        None => return vec![last]
                    }
                }
                // NOTE: fix seam from `b` (if at beginning)
                if last.try_join_over_seam_b(&first, a_max, b_max) {
                    let mut out = middle_iter.collect_vec();
                    out.push(last);
                    return out
                }
                // NOTE: fix seam from `b` (if in middle)
                let mut out = Vec::new();
                while let Some(next) = middle_iter.next() {
                    if first.try_join_over_seam_b(&next, a_max, b_max) {
                        out.push(first);
                        out.extend(middle_iter);
                        out.push(last);
                        return out
                    }
                    out.push(first);
                    first = next
                }
                // NOTE: fix seam from `b` (if at end)
                if first.try_join_over_seam_b(&last, a_max, b_max) {
                    out.push(first);
                    return out
                }
                // NOTE: no seam from `b`
                out.push(first);
                out.push(last);
                return out
            }
            return vec![last]
        }
        Vec::new()
    }

    pub fn first_segment<A,B>(a: A, b: B) -> Option<Self>
    where A: Iterator + Clone,
          B: Iterator + Clone,
          A::Item: PartialEq<B::Item> {
        find_match(a.clone(), b.clone()).map(|(first_index, other_index)| {
            let length = 1 +count_matching(
                a.skip(first_index+1),
                b.skip(other_index+1),
            );
            Self {a_start: first_index, b_start: other_index, length}
        })
    }

    fn continues_over_edge_a(&self, other: &Self, a_max: usize, b_max: usize) -> bool {
        self.a_start == 0 && other.a_start +other.length == a_max
            && (other.b_start +other.length) %b_max == self.b_start
    }
    fn continues_over_edge_b(&self, other: &Self, a_max: usize, b_max: usize) -> bool {
        self.b_start == 0 && other.b_start +other.length == b_max
            && (other.a_start +other.length) %a_max == self.a_start
    }
    fn _continues(&self, other: &Self, a_max: usize, b_max: usize) -> bool {
        (other.a_start +other.length) %a_max == self.a_start &&
            (other.b_start +other.length) %b_max == self.b_start
    }
    fn try_join_over_seam_a(&mut self, other: &Self, a_max: usize, b_max: usize) -> bool {
        if other.continues_over_edge_a(self, a_max, b_max) {
            self.length += other.length;
            return true
        }
        false
    }
    fn try_join_over_seam_b(&mut self, other: &Self, a_max: usize, b_max: usize) -> bool {
        if other.continues_over_edge_b(self, a_max, b_max) {
            self.length += other.length;
            return true
        }
        false
    }
    fn _try_join(&mut self, other: &Self, a_max: usize, b_max: usize) -> bool {
        if other._continues(self, a_max, b_max) {
            self.length += other.length;
            return true
        }
        false
    }
    pub fn reverse(self, a_max: usize, b_max: usize) -> Self {
        Self {
            a_start: a_max -self.a_start -self.length,
            b_start: b_max -self.b_start -self.length,
            length: self.length
        }
    }
}


pub struct CommonIter<A,B>
where A: Iterator + Clone,
      B: Iterator + Clone,
      A::Item: PartialEq<B::Item> {
    offset: usize,
    a: A,
    b: B
}
impl<A,B> CommonIter<A,B>
where A: Iterator + Clone,
      B: Iterator + Clone,
      A::Item: PartialEq<B::Item> {
    pub fn new(a: A, b: B) -> Self {
        Self {offset: 0, a, b}
    }

    fn shift_start(&mut self, value: usize) {
        (&mut self.a).dropping(value);
        self.offset += value
    }
    fn shift_end(&mut self, value: usize)
    where A: DoubleEndedIterator {
        (&mut self.a).dropping_back(value);
    }
}
impl<A,B> Iterator for CommonIter<A,B>
where A: Iterator + Clone,
      B: Iterator + Clone,
      A::Item: PartialEq<B::Item> {
    type Item = CommonSegment;
    fn next(&mut self) -> Option<Self::Item> {
        CommonSegment::first_segment(
            self.a.clone(),
            self.b.clone()
        ).map(|mut common| {
            let old_offset = self.offset;
            self.shift_start(common.a_start +common.length);
            common.a_start = common.a_start +old_offset;
            common
        })
    }
}
impl<A,B> DoubleEndedIterator for CommonIter<A,B>
where A: ExactSizeIterator + DoubleEndedIterator + Clone,
      B: ExactSizeIterator + DoubleEndedIterator + Clone,
      A::Item: PartialEq<B::Item> {
    fn next_back(&mut self) -> Option<Self::Item> {
        let a_max = self.a.len();
        let b_max = self.b.len();
        CommonSegment::first_segment(
            self.a.clone().rev(),
            self.b.clone().rev()
        ).map(|mut common| {
            self.shift_end(common.a_start +common.length);
            common = common.reverse(a_max, b_max);
            common.a_start += self.offset;
            common
        })
    }
}


fn find_match<A,B>(a: A, b: B) -> Option<(usize, usize)>
where A: Iterator,
      B: Iterator + Clone,
      A::Item: PartialEq<B::Item> {
    for (ai, current) in a.enumerate() {
        if let Some(bi) = b.clone().position(|o| current == o) {
            return Some((ai, bi))
        }
    }
    None
}


fn count_matching<A,B>(a: A, b: B) -> usize
where A: Iterator,
      B: Iterator,
      A::Item: PartialEq<B::Item> {
    a.zip(b).take_while(|(a,b)| a == b).count()
}
