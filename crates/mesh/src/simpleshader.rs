use super::{shader::*, Result};
use crate::IdentifyMesh;
use crate::SimpleMesh;
use crate::SimpleMesher;
use crate::UnlayeredMesh;
use crate::UnlayeredMesherTextured;


///
#[derive(Debug,Clone)]
pub struct SimpleShader<'texture> {
    texture: &'texture str
}
impl<'texture> SimpleShader<'texture> {
    ///
    pub fn new(texture: &'texture str) -> Self {
        Self {texture}
    }

    ///
    pub fn texture(&self) -> &'texture str {
        self.texture
    }
}
impl IdentifyMesh for SimpleShader<'_> {
    type MeshData<'external> = SimpleMesh<'external>;
}
impl<'external> Shader<SimpleMesh<'external>> for SimpleShader<'external> {
    type Mesher<'data> = SimpleMesher<'data, 'external>
    where Self: 'data,
          'external: 'data;

    fn mesher<'data>(
            &'data self, 
            data: &'data mut SimpleMesh<'external>
    ) -> Result<Self::Mesher<'data>> {
        Ok(data.mesher(self.texture()))
    }
}
impl<'texture> Shader<UnlayeredMesh<'texture>> for SimpleShader<'texture> {
    type Mesher<'data> = UnlayeredMesherTextured<'data, 'texture>
    where Self: 'data,
          'texture: 'data;

    fn mesher<'a>(
            &'a self, 
            mesh: &'a mut UnlayeredMesh<'texture>
    ) -> Result<Self::Mesher<'a>> {
        let mut mesher = mesh.mesher();
        let texture_id = mesher.quick_texture(self.texture());
        Ok(mesher.textured(texture_id))
    }
}
