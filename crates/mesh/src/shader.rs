use std::fmt::Debug;
use crate::Result;


///
pub trait Shader<MD> {
    ///
    type Mesher<'data>
    where Self: 'data,
          MD: 'data;

    ///
    fn mesher<'a>(&'a self, data: &'a mut MD) -> Result<Self::Mesher<'a>>;
}


///
pub trait MeshData<'external>: 'external + Send + Debug + IdentifyMesh<MeshData<'external>=Self> {
    ///
    fn default_box() -> Result<Box<Self>>;
}


///
pub trait IdentifyMesh {
    ///
    type MeshData<'a>: MeshData<'a>;
}