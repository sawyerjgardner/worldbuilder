use super::SimpleMeshLayer;
use crate::Face;
use crate::FaceVerts;
use crate::HasFaces;
use crate::HasVerts;
use crate::MesherFaces;
use crate::MesherVerts;
use crate::Vert;
use crate::VertPos;
use crate::VertUv;
use crate::VertNormal;
use crate::VertID;
use crate::FaceID;


///
pub struct SimpleMesher<'data, 'external> {
    mesh: &'data mut SimpleMeshLayer<'external>
}
impl<'data, 'external> SimpleMesher<'data, 'external> {
    ///
    pub fn new(mesh: &'data mut SimpleMeshLayer<'external>) -> Self {
        Self {mesh}
    }

    ///
    pub fn quick_vertice(&mut self, pos: [f32;3], uv: [f32;2], normal: [f32;3]) -> VertID {
        self.mesh.push_vertice(pos, uv, normal)
    }

    ///
    pub fn quick_face(&mut self, vert_ids: [VertID;3]) -> FaceID {
        self.mesh.push_face(vert_ids)
    }
}
impl<'a, 'external> HasFaces<'a> for SimpleMesher<'_, 'external> {
    type Face = SimpleMesherFace<'a, 'external>;
}
impl<'a, 'external> HasVerts<'a> for SimpleMesher<'_, 'external> {
    type Vert = SimpleMesherVert<'a, 'external>;
}
impl MesherFaces for SimpleMesher<'_, '_> {
    fn face(&mut self) -> <Self as HasFaces>::Face {
        SimpleMesherFace::new(&mut self.mesh)
    }
}
impl MesherVerts for SimpleMesher<'_, '_> {
    fn vert(&mut self) -> <Self as HasVerts>::Vert {
        SimpleMesherVert::new(&mut self.mesh)
    }
}


///
pub struct SimpleMesherVert<'shaderdata, 'external> {
    mesh: &'shaderdata mut SimpleMeshLayer<'external>,
    pos: [f32;3],
    uv: [f32;2],
    normal: [f32;3]
}
impl<'shaderdata, 'external> SimpleMesherVert<'shaderdata, 'external> {
    ///
    pub fn new(mesh: &'shaderdata mut SimpleMeshLayer<'external>) -> Self {
        Self {
            mesh, 
            pos: Default::default(), 
            uv: Default::default(),
            normal: Default::default()
        }
    }
}
impl Vert for SimpleMesherVert<'_,'_> {
    fn finish(self) -> crate::Result<VertID> {
        Ok(self.mesh.push_vertice(self.pos, self.uv, self.normal))
    }
}
impl VertPos for SimpleMesherVert<'_,'_> {
    fn pos(mut self, pos: [f32;3]) -> Self {
        self.pos = pos;
        self
    }
}
impl VertUv for SimpleMesherVert<'_,'_> {
    fn uv(mut self, uv: [f32;2]) -> Self {
        self.uv = uv;
        self
    }
}
impl VertNormal for SimpleMesherVert<'_,'_> {
    fn normal(mut self, normal: [f32;3]) -> Self {
        self.normal = normal;
        self    
    }
}


///
pub struct SimpleMesherFace<'shaderdata, 'external> {
    mesh: &'shaderdata mut SimpleMeshLayer<'external>,
    verts: [VertID;3],
}
impl<'shaderdata, 'external> SimpleMesherFace<'shaderdata, 'external> {
    ///
    pub fn new(mesh: &'shaderdata mut SimpleMeshLayer<'external>) -> Self {
        Self {mesh, verts: Default::default()}
    }
}
impl Face for SimpleMesherFace<'_, '_> {
    fn finish(self) -> crate::Result<FaceID> {
        Ok(self.mesh.push_face(self.verts))
    }
}
impl FaceVerts for SimpleMesherFace<'_, '_> {
    fn verts(mut self, verts: [VertID;3]) -> Self {
        self.verts = verts;
        self
    }
}