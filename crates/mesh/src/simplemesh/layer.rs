use crate::VertID;
use crate::FaceID;
use super::SimpleMesher;


///
#[derive(Clone,Debug)]
pub struct SimpleMeshLayer<'texture> {
    texture: &'texture str,
    verts: LayerVerts,
    faces: LayerFaces,
}
impl<'texture> SimpleMeshLayer<'texture> {
    ///
    pub fn new(texture: &'texture str) -> Self {
        Self {
            texture,
            verts: Default::default(),
            faces: Default::default(),
        }
    }

    ///
    pub fn texture(&self) -> &'texture str {
        &self.texture
    }

    ///
    pub fn positions(&self) -> &[[f32;3]] {
        self.verts.positions()
    }

    ///
    pub fn uvs(&self) -> &[[f32;2]] {
        self.verts.uvs()
    }

    ///
    pub fn normals(&self) -> &[[f32;3]] {
        self.verts.normals()
    }

    ///
    pub fn faces(&self) -> &[[VertID;3]] {
        self.faces.vert_ids()
    }

    ///
    pub fn push_vertice(&mut self,
                        pos: [f32;3],
                        uv: [f32;2],
                        normal: [f32;3]) -> VertID {
        self.verts.push(pos, uv, normal)
    }

    ///
    pub fn push_face(&mut self, vert_ids: [VertID;3]) -> FaceID {
        self.faces.push(vert_ids)
    }

    ///
    pub fn mesher<'data>(&'data mut self) -> SimpleMesher<'data, 'texture> {
        SimpleMesher::new(self)
    }
}


#[derive(Default,Clone,Debug)]
struct LayerVerts {
    positions: Vec<[f32;3]>,
    uvs: Vec<[f32;2]>,
    normals: Vec<[f32;3]>,
}
impl LayerVerts {
    pub fn positions(&self) -> &[[f32;3]] {
        &self.positions
    }
    pub fn uvs(&self) -> &[[f32;2]] {
        &self.uvs
    }
    pub fn normals(&self) -> &[[f32;3]] {
        &self.normals
    }
    pub fn push(&mut self,
                        pos: [f32;3],
                        uv: [f32;2],
                        normal: [f32;3]) -> VertID {
        let index = self.positions.len();
        self.positions.push(pos);
        self.uvs.push(uv);
        self.normals.push(normal);
        VertID(index)
    }
}


#[derive(Default,Clone,Debug)]
struct LayerFaces {
    vert_ids: Vec<[VertID;3]>
}
impl LayerFaces {
    pub fn vert_ids(&self) -> &[[VertID;3]] {
        &self.vert_ids
    }
    pub fn push(&mut self, vert_ids: [VertID;3]) -> FaceID {
        let index = self.vert_ids.len();
        self.vert_ids.push(vert_ids);
        FaceID(index)
    }
}
