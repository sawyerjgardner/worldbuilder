use crate::IdentifyMesh;
use crate::Result;
use crate::MeshData;
use crate::VertID;
use crate::FaceID;
use crate::TextureID;

mod mesher;
mod meshertextured;
pub use mesher::*;
pub use meshertextured::*;


///
#[derive(Clone,Default,Debug)]
pub struct UnlayeredMesh<'texture> {
    textures: Vec<&'texture str>,
    verts: UnlayeredVerts,
    faces: UnlayeredFaces
}
impl<'texture> UnlayeredMesh<'texture> {
    ///
    pub fn new() -> Self {
        Self::default()
    }

    ///
    pub fn textures(&self) -> &[&'texture str] {
        &self.textures
    }

    ///
    pub fn positions(&self) -> &[[f32;3]] {
        &self.verts.positions
    }

    ///
    pub fn uvs(&self) -> &[[f32;2]] {
        &self.verts.uvs
    }

    ///
    pub fn normals(&self) -> &[[f32;3]] {
        &self.verts.normals
    }

    ///
    pub fn face_verts(&self) -> &[[VertID;3]] {
        &self.faces.vert_ids
    }

    ///
    pub fn face_textures(&self) -> &[TextureID] {
        &self.faces.texture_ids
    }

    ///
    pub fn texture_id(&mut self, texture: &str) -> Option<TextureID> {
        self.textures.iter().position(|o| *o == texture).map(|o| TextureID(o))
    }

    ///
    pub fn texture_id_or_insert(&mut self, texture: &'texture str) -> TextureID {
        self.texture_id(texture).unwrap_or_else(|| {
            self.push_texture(texture)
        })
    }

    ///
    pub fn push_texture(&mut self, texture: &'texture str) -> TextureID {
        let index = self.textures.len();
        self.textures.push(texture);
        TextureID(index)
    }

    ///
    pub fn push_vertice(&mut self,
                        pos: [f32;3],
                        uv: [f32;2],
                        normal: [f32;3]) -> VertID {
        self.verts.push(pos, uv, normal)
    }

    ///
    pub fn push_face(&mut self, vert_ids: [VertID;3], texture_id: TextureID) -> FaceID {
        self.faces.push(vert_ids, texture_id)
    }

    ///
    pub fn mesher(&mut self) -> UnlayeredMesher<'_, 'texture> {
        UnlayeredMesher::new(self) 
    }
}
impl<'texture> MeshData<'texture> for UnlayeredMesh<'texture> {
    fn default_box() -> Result<Box<Self>> {
        Ok(Box::default())
    }
}
impl IdentifyMesh for UnlayeredMesh<'_> {
    type MeshData<'external> = UnlayeredMesh<'external>;
}


#[derive(Default,Clone,Debug)]
struct UnlayeredVerts {
    positions: Vec<[f32;3]>,
    uvs: Vec<[f32;2]>,
    normals: Vec<[f32;3]>
}
impl UnlayeredVerts {
    fn push(&mut self, pos: [f32;3], uv: [f32;2], normal: [f32;3]) -> VertID {
        let index = self.positions.len();
        self.positions.push(pos);
        self.uvs.push(uv);
        self.normals.push(normal);
        VertID(index)
    }
}


#[derive(Default,Clone,Debug)]
struct UnlayeredFaces {
    vert_ids: Vec<[VertID;3]>,
    texture_ids: Vec<TextureID>
}
impl UnlayeredFaces {
    pub fn push(&mut self, vert_ids: [VertID;3], texture_id: TextureID) -> FaceID {
        let index = self.vert_ids.len();
        self.vert_ids.push(vert_ids);
        self.texture_ids.push(texture_id);
        FaceID(index)
    }
}
