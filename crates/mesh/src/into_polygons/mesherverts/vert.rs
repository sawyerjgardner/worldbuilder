use crate::Result;


///
#[derive(Debug,Default,Clone,Copy)]
pub struct VertID(pub usize);


///
pub trait Vert {
    ///
    fn finish(self) -> Result<VertID>;
}


///
pub trait VertPos: Vert {
    ///
    fn pos(self, pos: [f32;3]) -> Self;
}


///
pub trait VertUv: Vert {
    ///
    fn uv(self, uv: [f32;2]) -> Self;
}


///
pub trait VertNormal: Vert {
    ///
    fn normal(self, normal: [f32;3]) -> Self;
}


///
pub trait VertColor: Vert {
    ///
    fn color(self, color: [f32;4]) -> Self;
}