mod vert;
pub use vert::*;


///
pub trait HasVerts<'a, T: sealed::Sealed = sealed::Bounds<&'a Self>> {
    ///
    type Vert: Vert;
}


///
pub trait MesherVerts: for<'a> HasVerts<'a> {
    ///
    fn vert(&mut self) -> <Self as HasVerts>::Vert;
}


mod sealed {
    pub trait Sealed: Sized {}
    pub struct  Bounds<T>(T);
    impl<T> Sealed for Bounds<T> {}
}