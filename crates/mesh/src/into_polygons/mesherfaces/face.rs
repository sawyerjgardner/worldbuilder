use crate::VertID;
use crate::Result;


///
#[derive(Debug,Default,Clone,Copy)]
pub struct FaceID(pub usize);


///
#[derive(Debug,Default,Clone,Copy)]
pub struct TextureID(pub usize);



///
pub trait Face {
    ///
    fn finish(self) -> Result<FaceID>;
}


///
pub trait FaceVerts: Face {
    ///
    fn verts(self, verts: [VertID;3]) -> Self;
}


///
pub trait FaceTexture: Face {
    ///
    fn texture(self, texture: TextureID) -> Self;
}