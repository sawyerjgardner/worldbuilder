use crate::VertID;
use crate::Result;

mod face;
pub use face::*;


///
pub trait HasFaces<'a, T: sealed::Sealed = sealed::Bounds<&'a Self>> {
    ///
    type Face: Face;
}


///
pub trait MesherFaces: for<'a> HasFaces<'a> {
    ///
    fn face(&mut self) -> <Self as HasFaces>::Face;

    ///
    fn polygon<I>(&mut self, mut iter: I) -> Result<()>
    where I: Iterator<Item=VertID>,
          for<'a> <Self as HasFaces<'a>>::Face: FaceVerts {
        let Some(pivit) = iter.next() else {return Ok(())};
        let Some(mut a) = iter.next() else {return Ok(())};
        while let Some(b) = iter.next() {
            self.face().verts([pivit,a,b]).finish()?;
            a = b
        }
        Ok(())
    }
}


mod sealed {
    pub trait Sealed: Sized {}
    pub struct  Bounds<T>(T);
    impl<T> Sealed for Bounds<T> {}
}