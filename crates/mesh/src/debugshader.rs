use super::*;


///
#[derive(Debug,Clone)]
pub struct DebugShader {
    pub color: [u8;3],
    pub alpha: u8
}
impl DebugShader {
    pub const RED: Self = Self {
        color: [255,0,0],
        alpha: 0
    };

    ///
    pub fn texture(&self) -> &'static str {
        "Debug"
    }
}
impl IdentifyMesh for DebugShader {
    type MeshData<'external> = SimpleMesh<'external>;
}
impl<'texture> Shader<SimpleMesh<'texture>> for DebugShader {
    type Mesher<'data> = SimpleMesher<'data, 'texture>
    where Self: 'data,
          'texture: 'data;

    fn mesher<'a>(
            &'a self, 
            mesh: &'a mut SimpleMesh<'texture>
    ) -> Result<Self::Mesher<'a>> {
        Ok(mesh.mesher(self.texture()))
    }
}
impl<'texture> Shader<UnlayeredMesh<'texture>> for DebugShader {
    type Mesher<'data> = UnlayeredMesherTextured<'data, 'texture>
    where Self: 'data,
          'texture: 'data;

    fn mesher<'a>(
            &'a self, 
            mesh: &'a mut UnlayeredMesh<'texture>
    ) -> Result<Self::Mesher<'a>> {
        let mut mesher = mesh.mesher();
        let texture_id = mesher.quick_texture(self.texture());
        Ok(mesher.textured(texture_id))
    }
}
