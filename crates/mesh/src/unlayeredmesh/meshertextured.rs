use super::UnlayeredMesher;
use super::UnlayeredMesherVert;
use super::UnlayeredMesherFace;
use crate::FaceTexture;
use crate::TextureID;
use crate::HasFaces;
use crate::MesherFaces;
use crate::FaceID;
use crate::HasVerts;
use crate::MesherVerts;
use crate::VertID;


///
pub struct UnlayeredMesherTextured<'mesh, 'texture> {
    ///
    pub mesher: UnlayeredMesher<'mesh, 'texture>,

    ///
    pub texture_id: TextureID
}
impl<'mesh, 'texture> UnlayeredMesherTextured<'mesh, 'texture> {
    ///
    pub fn new(mesher: UnlayeredMesher<'mesh, 'texture>, texture_id: TextureID) -> Self {
        Self {mesher, texture_id}
    }

    ///
    pub fn quick_vertice(&mut self, pos: [f32;3], uv: [f32;2], normal: [f32;3]) -> VertID {
        self.mesher.quick_vertice(pos, uv, normal)
    }

    ///
    pub fn quick_face(&mut self, vert_ids: [VertID;3]) -> FaceID {
        self.mesher.quick_face(vert_ids, self.texture_id)
    }
}
impl<'a, 'external> HasFaces<'a> for UnlayeredMesherTextured<'_, 'external> {
    type Face = UnlayeredMesherFace<'a, 'external>;
}
impl<'a, 'external> HasVerts<'a> for UnlayeredMesherTextured<'_, 'external> {
    type Vert = UnlayeredMesherVert<'a, 'external>;
}
impl MesherFaces for UnlayeredMesherTextured<'_, '_> {
    fn face(&mut self) -> <Self as HasFaces>::Face {
        self.mesher.face().texture(self.texture_id)
    }
}
impl MesherVerts for UnlayeredMesherTextured<'_, '_> {
    fn vert(&mut self) -> <Self as HasVerts>::Vert {
        self.mesher.vert()
    }
}