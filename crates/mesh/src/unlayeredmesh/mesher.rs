use super::UnlayeredMesh;
use super::UnlayeredMesherTextured;
use crate::TextureID;
use crate::HasFaces;
use crate::MesherFaces;
use crate::Face;
use crate::FaceVerts;
use crate::FaceID;
use crate::FaceTexture;
use crate::HasVerts;
use crate::MesherVerts;
use crate::Vert;
use crate::VertNormal;
use crate::VertPos;
use crate::VertUv;
use crate::VertID;


///
pub struct UnlayeredMesher<'mesh, 'texture> {
    mesh: &'mesh mut UnlayeredMesh<'texture>
}
impl<'mesh, 'texture> UnlayeredMesher<'mesh, 'texture> {
    ///
    pub fn new(mesh: &'mesh mut UnlayeredMesh<'texture>) -> Self {
        Self {mesh}
    }

    ///
    pub fn textured(self, texture_id: TextureID) -> UnlayeredMesherTextured<'mesh, 'texture> {
        UnlayeredMesherTextured::new(self, texture_id)
    }

    ///
    pub fn quick_texture(&mut self, texture: &'texture str) -> TextureID {
        self.mesh.texture_id_or_insert(texture)
    }

    ///
    pub fn quick_vertice(&mut self, pos: [f32;3], uv: [f32;2], normal: [f32;3]) -> VertID {
        self.mesh.push_vertice(pos, uv, normal)
    }

    ///
    pub fn quick_face(&mut self, vert_ids: [VertID;3], texture_id: TextureID) -> FaceID {
        self.mesh.push_face(vert_ids, texture_id)
    }
}
impl<'a, 'external> HasFaces<'a> for UnlayeredMesher<'_, 'external> {
    type Face = UnlayeredMesherFace<'a, 'external>;
}
impl<'a, 'external> HasVerts<'a> for UnlayeredMesher<'_, 'external> {
    type Vert = UnlayeredMesherVert<'a, 'external>;
}
impl MesherFaces for UnlayeredMesher<'_, '_> {
    fn face(&mut self) -> <Self as HasFaces>::Face {
        UnlayeredMesherFace::new(&mut self.mesh)
    }
}
impl MesherVerts for UnlayeredMesher<'_, '_> {
    fn vert(&mut self) -> <Self as HasVerts>::Vert {
        UnlayeredMesherVert::new(&mut self.mesh)
    }
}


///
pub struct UnlayeredMesherVert<'mesh, 'texture> {
    mesh: &'mesh mut UnlayeredMesh<'texture>,
    pos: [f32;3],
    uv: [f32;2],
    normal: [f32;3]
}
impl<'shaderdata, 'texture> UnlayeredMesherVert<'shaderdata, 'texture> {
    ///
    pub fn new(mesh: &'shaderdata mut UnlayeredMesh<'texture>) -> Self {
        Self {
            mesh, 
            pos: Default::default(), 
            uv: Default::default(),
            normal: Default::default()
        }
    }
}
impl Vert for UnlayeredMesherVert<'_,'_> {
    fn finish(self) -> crate::Result<VertID> {
        Ok(self.mesh.push_vertice(self.pos, self.uv, self.normal))
    }
}
impl VertPos for UnlayeredMesherVert<'_,'_> {
    fn pos(mut self, pos: [f32;3]) -> Self {
        self.pos = pos;
        self
    }
}
impl VertUv for UnlayeredMesherVert<'_,'_> {
    fn uv(mut self, uv: [f32;2]) -> Self {
        self.uv = uv;
        self
    }
}
impl VertNormal for UnlayeredMesherVert<'_,'_> {
    fn normal(mut self, normal: [f32;3]) -> Self {
        self.normal = normal;
        self    
    }
}


///
pub struct UnlayeredMesherFace<'mesh, 'texture> {
    mesh: &'mesh mut UnlayeredMesh<'texture>,
    verts: [VertID;3],
    texture_id: TextureID
}
impl<'mesh, 'texture> UnlayeredMesherFace<'mesh, 'texture> {
    ///
    pub fn new(mesh: &'mesh mut UnlayeredMesh<'texture>) -> Self {
        Self {
            mesh, 
            verts: Default::default(), 
            texture_id: Default::default()
        }
    }
}
impl Face for UnlayeredMesherFace<'_, '_> {
    fn finish(self) -> crate::Result<FaceID> {
        Ok(self.mesh.push_face(self.verts, self.texture_id))
    }
}
impl FaceVerts for UnlayeredMesherFace<'_, '_> {
    fn verts(mut self, verts: [VertID;3]) -> Self {
        self.verts = verts;
        self
    }
}
impl FaceTexture for UnlayeredMesherFace<'_, '_> {
    fn texture(mut self, texture: TextureID) -> Self {
        self.texture_id = texture;
        self
    }
}