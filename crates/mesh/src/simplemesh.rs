use crate::IdentifyMesh;
use crate::Result;
use crate::MeshData;

mod mesher;
mod layer;
pub use mesher::*;
pub use layer::*;


///
#[derive(Clone,Default,Debug)]
pub struct SimpleMesh<'texture> {
    pub layers: Vec<SimpleMeshLayer<'texture>>
}
impl<'texture> SimpleMesh<'texture> {
    ///
    pub fn new() -> Self {
        SimpleMesh {
            layers: Vec::new(),
        }
    }

    ///
    pub fn get<'a>(&'a self, texture: &str) -> Option<&'a SimpleMeshLayer<'texture>> {
        self.index(texture).map(|index| {
            &self.layers[index]
        })
    }

    ///
    pub fn get_mut<'a>(&'a mut self, texture: &str) -> Option<&'a mut SimpleMeshLayer<'texture>> {
        self.index(texture).map(|index| {
            &mut self.layers[index]
        })
    }

    ///
    pub fn get_or_default(&mut self, texture: &'texture str) -> &mut SimpleMeshLayer<'texture> {
        let index = self.index(texture).unwrap_or_else(|| {
                let index = self.layers.len();
                self.layers.push(SimpleMeshLayer::new(texture));
                index
        });
        &mut self.layers[index]
    }

    ///
    pub fn index(&self, texture: &str) -> Option<usize> {
        self.layers.iter().position(|o| o.texture() == texture)
    }

    ///
    pub fn mesher<'a>(&'a mut self, texture: &'texture str) -> SimpleMesher<'a, 'texture> {
        SimpleMesher::new(self.get_or_default(texture))
    }
}
impl IdentifyMesh for SimpleMesh<'_> {
    type MeshData<'external> = SimpleMesh<'external>;
}
impl<'texture> MeshData<'texture> for SimpleMesh<'texture> {
    fn default_box() -> Result<Box<Self>> {
        Ok(Box::default())
    }
}
