use std::fmt::Debug;
use std::any::TypeId;
use std::collections::HashMap;
use std::collections::hash_map::Entry;
use crate::Result;
use crate::Shader;
use crate::MeshData;
use crate::IdentifyMesh;


///
pub struct Mesh<'external> {
    shaderdata: HashMap<TypeId, Box<dyn MeshDataHelper<'external>>>
}
impl<'external> Mesh<'external> {
    ///
    pub fn new() -> Self {
        Self {shaderdata: HashMap::new()}
    }

    ///
    pub fn from_box<T>(meshdata: Box<T>) -> Self 
    where T: MeshData<'external>{
        Self {
            shaderdata: HashMap::from([(
                T::mesh_key(),
                Box::<dyn MeshDataHelper<'external>>::from(meshdata)
            )])
        } 
    }

    ///
    pub fn len(&self) -> usize {
        self.shaderdata.len()
    }

    ///
    pub fn is_empty(&self) -> bool {
        self.shaderdata.is_empty()
    }

    ///
    pub fn remove<T>(&mut self) -> Option<Box<T::MeshData<'external>>>
    where T: IdentifyMesh {
        self.shaderdata.remove(&T::mesh_key()).map(|data| {
            unsafe {
                Box::from_raw(Box::into_raw(data) as *mut T::MeshData<'external>)
            }
        })
    }

    ///
    pub fn try_insert<T>(&mut self, meshdata: Box<T>) -> Option<&mut T>
    where T: MeshData<'external> {
        let Entry::Vacant(entry) = self.shaderdata.entry(T::mesh_key()) else {
            return None
        };
        Some(unsafe {&mut *(
            entry.insert(meshdata).as_mut()
            as *mut dyn MeshDataHelper<'external> 
            as *mut T::MeshData<'external>
        )})
    }

    ///
    pub fn insert_force<T>(&mut self, meshdata: Box<T>) -> &mut T
    where T: MeshData<'external> {
        let newdata = match self.shaderdata.entry(T::mesh_key()) {
            Entry::Vacant(o) => o.insert(meshdata).as_mut(),
            Entry::Occupied(o) => {
                let databox = o.into_mut();
                std::mem::swap(
                    databox, 
                    &mut Box::<dyn MeshDataHelper<'external>>::from(meshdata)
                );
                databox.as_mut()
            },
        };
        unsafe {&mut *(
            newdata
            as *mut dyn MeshDataHelper<'external> 
            as *mut T::MeshData<'external>
        )}
    }

    ///
    pub fn get<T>(&self) -> Option<&T::MeshData<'external>>
    where T: IdentifyMesh {
        self.shaderdata.get(&T::mesh_key()).map(|data| {
            unsafe {&*(
                data.as_ref() 
                as *const dyn MeshDataHelper<'external> 
                as *const T::MeshData<'external>
            )}
        })
    }

    ///
    pub fn get_mut<T>(&mut self) -> Option<&mut T::MeshData<'external>>
    where T: IdentifyMesh {
        self.shaderdata.get_mut(&T::mesh_key()).map(|data| {
            unsafe {&mut *(
                data.as_mut() 
                as *mut dyn MeshDataHelper<'external> 
                as *mut T::MeshData<'external>
            )}
        })
    }

    ///
    pub fn get_or_default<T>(&mut self) -> Result<&mut T::MeshData<'external>>
    where T: IdentifyMesh {
        let data = match self.shaderdata.entry(T::mesh_key()) {
             Entry::Occupied(o) => o.into_mut(),
             Entry::Vacant(o) =>  o.insert(T::MeshData::<'external>::default_box()?),
        };
        Ok(unsafe {&mut *(
            data.as_mut() 
            as *mut dyn MeshDataHelper<'external> 
            as *mut T::MeshData<'external>
        )})
    }

    ///
    pub fn mesher<'mesh>(&'mesh mut self) -> BaseMesher<'mesh, 'external> {
        BaseMesher::new(self)
    }
}
impl<'external, T> From<T> for Mesh<'external> 
where T: MeshData<'external> {
    fn from(value: T) -> Self {
        Self::from_box(Box::new(value))
    }
}
impl Debug for Mesh<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut meshes = self.shaderdata.values();
        let Some(first) = meshes.next() else {
            return f.write_str("Mesh{}")
        };
        f.write_str("Mesh{ ")?;
        first.fmt(f)?;
        for mesh in meshes {
            f.write_str(", ")?;
            mesh.fmt(f)?;
        }
        f.write_str(" }")
    }
}


///
pub struct BaseMesher<'mesh, 'external> {
    mesh: &'mesh mut Mesh<'external>
}
impl<'mesh, 'external> BaseMesher<'mesh, 'external> {
    ///
    pub fn new(mesh: &'mesh mut Mesh<'external>) -> Self {
        Self {mesh}
    }

    ///
    pub fn shader<'data, T>(&'data mut self, shader: &'data T) -> Result<T::Mesher<'data>>
    where T: IdentifyMesh + Shader<<T as IdentifyMesh>::MeshData<'external>> {
        self.shader_for::<T, T>(shader)
    }

    ///
    pub fn shader_for<'data, MD, T>(&'data mut self, shader: &'data T) -> Result<T::Mesher<'data>>
    where MD: IdentifyMesh, 
          T: Shader<MD::MeshData<'external>> {
        shader.mesher(self.mesh.get_or_default::<MD>()?)
    }
}


trait MeshDataHelper<'external>: 'external + Send + Debug {}
impl<'external, T> MeshDataHelper<'external> for T
where T: MeshData<'external> {}


trait MeshKey: IdentifyMesh {
    fn mesh_key() -> TypeId {
        TypeId::of::<Self::MeshData<'static>>()
    }
}
impl<T> MeshKey for T 
where T: IdentifyMesh {}