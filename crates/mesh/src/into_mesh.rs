use crate::Result;
use crate::Mesh;
use crate::BaseMesher;


///
pub trait IntoMesh<'external> {
    ///
    fn msh<'mesh>(self, m: BaseMesher<'mesh, 'external>) -> Result<()>;

    ///
    fn into_mesh(self) -> Result<Mesh<'external>>
    where Self: Sized {
        let mut mesh = Mesh::new();
        self.msh(mesh.mesher())?;
        Ok(mesh)
    }
}


///
pub trait IntoDebugMesh<'external> {
    ///
    fn msh<'mesh>(self, m: BaseMesher<'mesh, 'external>) -> Result<()>;

    ///
    fn into_debugmesh(self) -> Result<Mesh<'external>>
    where Self: Sized {
        let mut mesh = Mesh::new();
        self.msh(mesh.mesher())?;
        Ok(mesh)
    }
}
