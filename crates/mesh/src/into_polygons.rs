use crate::Result;

mod mesherfaces;
mod mesherverts;
pub use mesherfaces::*;
pub use mesherverts::*;


///
pub trait IntoPolygons<T> {
    fn poly(self, mesher: T) -> Result<()>;
}