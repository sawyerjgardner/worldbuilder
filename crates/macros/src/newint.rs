use quote::*;
use proc_macro2::TokenStream as TokenStream2;
use proc_macro2::Ident as Ident2;
use proc_macro2::Span as Span2;
use syn::DeriveInput;

mod main_impl;
use main_impl::*;


pub fn int_inharit_stream(input: &DeriveInput, member_name: &TokenStream2, member_type: &TokenStream2) -> TokenStream2 {
    let mut out = stream_inharent_ops(input, member_name, [
        "Add",
        "Sub",
        "Mul",
        "Div",
        "Rem",
        "BitAnd",
        "BitOr",
        "BitXor"
    ].into_iter());
    // TODO: shl shr
    out.extend(stream_inharent_fmts(input, member_name, [
        "Display",
        "Debug",
        "Binary",
        "Octal",
        "LowerExp",
        "LowerHex",
        "UpperExp",
        "UpperHex",
    ].into_iter()));
    out.extend(stream_newint_main_impl(input, member_name, member_type));
    out.extend(stream_inharent_ect(input, member_name, member_type));
    out.extend(stream_inharent_froms(input, ["u8",].iter().cloned()));
    out.extend(stream_inharent_try_froms(input, member_type, [
        "bool",
        "u16",
        "u32",
        "u64",
        "u128",
        "i8",
        "i16",
        "i32",
        "i64",
        "i128",
        "isize",
    ].iter().cloned()));
    out.extend(stream_inharent_try_intos(input, member_name, member_type, [
        "u8",
        "u16",
        "u32",
        "u64",
        "u128",
        "i8",
        "i16",
        "i32",
        "i64",
        "i128",
        "isize",
    ].iter().cloned()));
    out.extend(stream_from_into_usize(input, member_name, member_type));
    out
}


pub fn stream_inharent_ops<'a, I>(input: &DeriveInput, member: &TokenStream2, trait_name_strs: I) -> TokenStream2
where I: Iterator<Item=&'a str> {
    let mut out = TokenStream2::new();
    for name in trait_name_strs {
        out.extend(stream_inharent_opexpr(input, member, name).into_iter());
        out.extend(stream_inharent_opassign(input, member, name).into_iter());
    }
    out
}

fn stream_inharent_opexpr(input: &DeriveInput, member: &TokenStream2, trait_name_str: &str) -> TokenStream2 {
    let trait_name = Ident2::new(trait_name_str, Span2::call_site().into());
    let trait_fn   = Ident2::new(&trait_name_str.to_lowercase(), Span2::call_site().into());
    let mut out = stream_inharent_opexpr_part(
        input,
        member,
        &trait_name,
        &trait_fn,
        quote!(),
        quote!(),
    );
    out.extend(stream_inharent_opexpr_part(
        input,
        member,
        &trait_name,
        &trait_fn,
        quote!(&),
        quote!(),
    ));
    out.extend(stream_inharent_opexpr_part(
        input,
        member,
        &trait_name,
        &trait_fn,
        quote!(),
        quote!(&)
    ));
    out.extend(stream_inharent_opexpr_part(
        input,
        member,
        &trait_name,
        &trait_fn,
        quote!(&),
        quote!(&)
    ));
    out
}

fn stream_inharent_opexpr_part<B,C>(input: &DeriveInput,
                                    member: &TokenStream2,
                                    trait_name: &Ident2,
                                    trait_fn: &Ident2,
                                    ref_a: B,
                                    ref_b: C) -> TokenStream2
where B: quote::ToTokens,
      C: quote::ToTokens{
    let (impl_generics, ty_generics, where_clause) = input.generics.split_for_impl();
    let name = &input.ident;
    let base_type = quote!(#name #ty_generics);
    quote!{
        impl #impl_generics std::ops::#trait_name <#ref_b #base_type> for #ref_a #base_type #where_clause {
            type Output = #base_type;
            fn #trait_fn(self, rhs: #ref_b #base_type) -> #base_type {
                #base_type {#member: std::ops::#trait_name::#trait_fn(self.#member, rhs.#member)}
            }
        }
    }
}

fn stream_inharent_opassign(input: &DeriveInput, member: &TokenStream2, trait_name_str: &str) -> TokenStream2 {
    let trait_name = Ident2::new(&format!("{}Assign", trait_name_str), Span2::call_site().into());
    let trait_fn   = Ident2::new(&format!("{}_assign", trait_name_str.to_lowercase()), Span2::call_site().into());
    let mut out = stream_inharent_opassign_part(
        input,
        member,
        &trait_name,
        &trait_fn,
        quote!(),
    );
    out.extend(stream_inharent_opassign_part(
        input,
        member,
        &trait_name,
        &trait_fn,
        quote!(&)
    ));
    // TODO: Saturating, wrapping
    out
}

fn stream_inharent_opassign_part<C>(input: &DeriveInput,
                                    member: &TokenStream2,
                                    trait_name: &Ident2,
                                    trait_fn: &Ident2,
                                    ref_b: C) -> TokenStream2
where C: quote::ToTokens{
    let (impl_generics, ty_generics, where_clause) = input.generics.split_for_impl();
    let name = &input.ident;
    let base_type = quote!(#name #ty_generics);
    quote!{
        impl #impl_generics std::ops::#trait_name <#ref_b #base_type> for #base_type #where_clause {
            fn #trait_fn(&mut self, rhs: #ref_b #base_type) {
                std::ops::#trait_name::#trait_fn(&mut self.#member, rhs.#member)
            }
        }
    }
}


pub fn stream_inharent_fmts<'a, I>(input: &DeriveInput, member: &TokenStream2, trait_names: I) -> TokenStream2
where I: Iterator<Item=&'a str> {
    let mut out = TokenStream2::new();
    for name in trait_names {
        out.extend(stream_inharent_fmt_part(input, member, &Ident2::new(name, Span2::call_site().into())).into_iter());
    }
    out
}

fn stream_inharent_fmt_part(input: &DeriveInput,
                            member: &TokenStream2,
                            fmt_name: &Ident2) -> TokenStream2 {
    let (impl_generics, ty_generics, where_clause) = input.generics.split_for_impl();
    let name = &input.ident;
    quote!{
        impl #impl_generics std::fmt::#fmt_name for #name #ty_generics #where_clause {
            fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
                std::fmt::#fmt_name::fmt(&self.#member, f)
            }
        }
    }
}


pub fn stream_inharent_ect(input: &DeriveInput, member_name: &TokenStream2, member_type: &TokenStream2) -> TokenStream2 {
    let (impl_generics, ty_generics, where_clause) = input.generics.split_for_impl();
    let name = &input.ident;
    let mut params_a = input.generics.params.clone();
    params_a.insert(0, syn::GenericParam::Lifetime(
        syn::LifetimeParam::new(
            syn::Lifetime::new("'a", Span2::call_site())
        )
    ));
    quote!{
        impl #impl_generics std::str::FromStr for #name #ty_generics #where_clause {
            type Err = <#member_type as std::str::FromStr>::Err;
            fn from_str(s: &str) -> Result<Self, <#member_type as std::str::FromStr>::Err> {
                std::str::FromStr::from_str(s).map(|o| Self(o))
            }
        }
        impl #impl_generics std::ops::Not for #name #ty_generics #where_clause {
            type Output = Self;
            fn not(self) -> Self {
                Self(std::ops::Not::not(self.#member_name))
            }
        }
        impl #impl_generics std::ops::Not for & #name #ty_generics #where_clause {
            type Output = #name #ty_generics;
            fn not(self) -> #name #ty_generics {
                #name(std::ops::Not::not(&self.#member_name))
            }
        }
        impl #impl_generics std::iter::Product<Self> for #name #ty_generics #where_clause {
            fn product<I>(iter: I) -> Self
            where I: Iterator<Item=Self> {
                Self(std::iter::Product::<#member_type>::product(iter.map(|o| o.#member_name)))
            }
        }
        impl <#params_a> std::iter::Product<&'a Self> for #name #ty_generics #where_clause {
            fn product<I>(iter: I) -> Self
            where I: Iterator<Item=&'a Self> {
                Self(std::iter::Product::<#member_type>::product(iter.map(|o| o.#member_name)))
            }
        }
        impl #impl_generics std::iter::Sum<Self> for #name #ty_generics #where_clause {
            fn sum<I>(iter: I) -> Self
            where I: Iterator<Item=Self> {
                Self(std::iter::Sum::<#member_type>::sum(iter.map(|o| o.#member_name)))
            }
        }
        impl <#params_a> std::iter::Sum<&'a Self> for #name #ty_generics #where_clause {
            fn sum<I>(iter: I) -> Self
            where I: Iterator<Item=&'a Self> {
                Self(std::iter::Sum::<#member_type>::sum(iter.map(|o| o.#member_name)))
            }
        }
    }
}


pub fn stream_inharent_froms<'a, I>(input: &DeriveInput, type_names: I) -> TokenStream2
where I: Iterator<Item=&'a str> {
    let mut out = TokenStream2::new();
    for name in type_names {
        out.extend(stream_inharent_from_part(input, Ident2::new(name, Span2::call_site().into())));
    }
    out
}

fn stream_inharent_from_part(input: &DeriveInput, from_type: Ident2) -> TokenStream2 {
    let (impl_generics, ty_generics, where_clause) = input.generics.split_for_impl();
    let name = &input.ident;
    quote!{
        impl #impl_generics From<#from_type> for #name #ty_generics #where_clause {
            fn from(value: #from_type) -> Self {
                Self(From::from(value))
            }
        }
    }
}


pub fn _stream_inharent_intos<'a, I>(input: &DeriveInput, member_name: &TokenStream2, type_names: I) -> TokenStream2
where I: Iterator<Item=&'a str> {
    let mut out = TokenStream2::new();
    for name in type_names {
        out.extend(_stream_inharent_into_part(input, member_name, Ident2::new(name, Span2::call_site().into())));
    }
    out
}

fn _stream_inharent_into_part(input: &DeriveInput, member_name: &TokenStream2, into_type: Ident2) -> TokenStream2 {
    let (impl_generics, ty_generics, where_clause) = input.generics.split_for_impl();
    let name = &input.ident;
    quote!{
        impl #impl_generics From<#name #ty_generics> for #into_type #where_clause {
            fn from(value: #name #ty_generics) -> Self {
                From::from(value.#member_name)
            }
        }
    }
}


pub fn stream_inharent_try_froms<'a, I>(input: &DeriveInput, member_type: &TokenStream2, type_names: I) -> TokenStream2
where I: Iterator<Item=&'a str> {
    let mut out = TokenStream2::new();
    for name in type_names {
        out.extend(stream_inharent_try_from_part(input, member_type, Ident2::new(name, Span2::call_site().into())));
    }
    out
}

fn stream_inharent_try_from_part(input: &DeriveInput, member_type: &TokenStream2, from_type: Ident2) -> TokenStream2 {
    let (impl_generics, ty_generics, where_clause) = input.generics.split_for_impl();
    let name = &input.ident;
    quote!{
        impl #impl_generics TryFrom<#from_type> for #name #ty_generics #where_clause {
            type Error = <#member_type as std::convert::TryFrom<#from_type>>::Error;
            fn try_from(value: #from_type) -> Result<Self, <#member_type as std::convert::TryFrom<#from_type>>::Error> {
                TryFrom::try_from(value).map(|o| Self(o))
            }
        }
    }
}


pub fn stream_inharent_try_intos<'a, I>(input: &DeriveInput, member_name: &TokenStream2, member_type: &TokenStream2, type_names: I) -> TokenStream2
where I: Iterator<Item=&'a str> {
    let mut out = TokenStream2::new();
    for name in type_names {
        out.extend(stream_inharent_try_into_part(input, member_name, member_type, Ident2::new(name, Span2::call_site().into())));
    }
    out
}

fn stream_inharent_try_into_part(input: &DeriveInput, member_name: &TokenStream2, member_type: &TokenStream2, into_type: Ident2) -> TokenStream2 {
    let (impl_generics, ty_generics, where_clause) = input.generics.split_for_impl();
    let name = &input.ident;
    quote!{
        impl #impl_generics std::convert::TryFrom<#name #ty_generics> for #into_type #where_clause {
            type Error = <Self as std::convert::TryFrom<#member_type>>::Error;
            fn try_from(value: #name #ty_generics) -> Result<Self, <Self as std::convert::TryFrom<#member_type>>::Error> {
                std::convert::TryFrom::try_from(value.#member_name)
            }
        }
    }
}


fn stream_from_into_usize(input: &DeriveInput, member_name: &TokenStream2, member_type: &TokenStream2) -> TokenStream2 {
    let (impl_generics, ty_generics, where_clause) = input.generics.split_for_impl();
    let name = &input.ident;
    quote!{
        impl #impl_generics std::convert::From<#name #ty_generics> for usize #where_clause {
            fn from(value: #name #ty_generics) -> usize {
                value.#member_name as usize
            }
        }
        impl #impl_generics std::convert::From<usize> for #name #ty_generics #where_clause {
            fn from(value: usize) -> Self {
                const _: () = assert!(
                    #member_type::BITS <= usize::BITS,
                    //"NewInt base type can not be bigger then usize"
                    "`HoleSize` can not be bigger then `usize`."
                );
                Self(value as #member_type)
            }
        }
    }
}
