use std::mem::swap;
use proc_macro::TokenStream;
use quote::quote;
use syn::DeriveInput;

mod newint;
mod newfixed;
use newint::*;
use newfixed::*;


///
#[proc_macro_derive(RefHole)]
pub fn refhole_macro(input: TokenStream) -> TokenStream {
    let input = syn::parse_macro_input!(input as DeriveInput);
    let name = input.ident.clone();
    let (impl_generics, ty_generics, where_clause) = input.generics.split_for_impl();

    let mut fields_iter = iter_fields(&input);
    let (f1, f2) = (fields_iter.next().unwrap(), fields_iter.next().unwrap());
    let hole_name = f1.ident.as_ref().map(|o| quote!(#o)).unwrap_or_else(|| quote!(0));
    let unref_name = f2.ident.as_ref().map(|o| quote!(#o)).unwrap_or_else(|| quote!(1));
    let hole_type = &f1.ty;
    let unref_type = &f2.ty;

    quote!{
        impl #impl_generics RefHole for #name #ty_generics #where_clause  {
            type HoleT = #hole_type;
            type Unref = #unref_type;

            fn as_unref(&self) -> &Self::Unref {
                &self.#unref_name
            }
            unsafe fn as_unref_mut(&mut self) -> &mut Self::Unref {
                &mut self.#unref_name
            }
            fn into_unref(self) -> Self::Unref {
                self.#unref_name
            }

            fn as_hole(&self) -> &Self::HoleT {
                &self.#hole_name
            }
            unsafe fn as_hole_mut(&mut self) -> &mut Self::HoleT {
                &mut self.#hole_name
            }
            fn into_hole(self) -> Self::HoleT {
                self.#hole_name
            }

            unsafe fn from_unref(#hole_name: Self::HoleT, #unref_name: Self::Unref) -> Self {
                Self {#hole_name, #unref_name}
            }
        }
    }.into()
}


/// Abstracts the size of an integer so that it can be changed at compile time.
///
/// This is a derive macro that implements methods and traits common across all
/// int types. Structs derived from this macro can only have a single feild, and
/// its type must be an unsigned integer. The type of this integer can be
/// safely changed at compile time without effecting any of the methods this
/// macro generates.
#[proc_macro_derive(NewInt)]
pub fn newint_macro(input: TokenStream) -> TokenStream {
    let input = syn::parse_macro_input!(input as DeriveInput);
    let f1 = iter_fields(&input).next().unwrap();
    let member_name = f1.ident.as_ref().map(|o| quote!(#o)).unwrap_or_else(|| quote!(0));
    let member_type = &f1.ty;
    int_inharit_stream(&input, &member_name, &quote!(#member_type)).into()
}


/// Abstracts the size of a fixed point number so that it can be changed at compile time.
///
/// This is a derive macro that implements methods and traits common across all
/// int types, as well as a few methods that only make sense for fixed point
/// numbers. Structs derived from this macro can only have a single feild, and
/// its type must be a fixed point type from the creat `fixed`. The size and
/// shape of this type can be changed at compile time without effecting any of
/// the methods this macro generates.
#[proc_macro_derive(NewFixed)]
pub fn newfloat_macro(input: TokenStream) -> TokenStream {
    let input = syn::parse_macro_input!(input as DeriveInput);
    let f1 = iter_fields(&input).next().unwrap();
    let member_name = f1.ident.as_ref().map(|o| quote!(#o)).unwrap_or_else(|| quote!(0));
    let member_type = &f1.ty;
    fixed_inharit_stream(&input, &member_name, &quote!(#member_type)).into()
}


fn iter_fields(input: &DeriveInput) -> syn::punctuated::Iter<'_, syn::Field> {
    use syn::Data::*;
    use syn::Fields::*;
    match &input.data {
        Struct(data) => match &data.fields {
            Named(fields) => fields.named.iter(),
            Unnamed(fields) => fields.unnamed.iter(),
            Unit => panic!("Not implamented for Unit Structs")
        } ,
        Enum(_) => panic!("Not implamented for Enums"),
        Union(_) => panic!("Not implamented for Unions")
    }
}


#[proc_macro_attribute]
pub fn preprocess_pymethods(args: TokenStream, input: TokenStream) -> TokenStream {
    let mut input_block = syn::parse_macro_input!(input as syn::ItemImpl);
    let mut items = Vec::new();
    swap(&mut items, &mut input_block.items);
    let mut processer = PymethodsProcessor::new(input_block);
    processer.push_iter(items.into_iter());
    processer.quote(args)
}


struct PymethodsProcessor {
    non_py_block: syn::ItemImpl,
    pymethods_block: syn::ItemImpl,
    cleaned_block: syn::ItemImpl,
}
impl PymethodsProcessor {
    fn new(blank_input: syn::ItemImpl) -> Self {
        Self {
            non_py_block: blank_input.clone(),
            pymethods_block: blank_input.clone(),
            cleaned_block: blank_input
        }
    }

    fn push_non_py(&mut self, item: syn::ImplItem) {
        self.non_py_block.items.push(item);
    }

    fn push_pymethod(&mut self, item: syn::ImplItemFn) {
        self.pymethods_block.items.push(syn::ImplItem::Fn(clear_non_pymethod_attr(item.clone())));
        self.cleaned_block.items.push(syn::ImplItem::Fn(clear_pymethod_attr(item)));
    }

    fn push(&mut self, item: syn::ImplItem) {
        if is_pymethod(&item) {
            let syn::ImplItem::Fn(itemfn) = item else {panic!()};
            self.push_pymethod(itemfn);
            return
        }
        self.push_non_py(item)
    }

    fn push_iter<I>(&mut self, items: I)
    where I: Iterator<Item=syn::ImplItem> {
        for item in items {
            self.push(item)
        }
    }

    fn quote(&self, condition: TokenStream) -> TokenStream {
        let npb = &self.non_py_block;
        let pmb = &self.pymethods_block;
        let cb = &self.cleaned_block;
        let condition: proc_macro2::TokenStream = condition.into();
        quote!(
            #npb
            #[cfg(#condition)]
            #[pyo3::pymethods]
            #pmb
            #[cfg(not(#condition))]
            #cb
        ).into()
    }
}


fn attribute_ident(attr: &syn::Attribute) -> Option<&syn::Ident> {
    if let syn::Meta::Path(path) = &attr.meta {
        return path.get_ident()
    }
    if let syn::Meta::List(metalist) = &attr.meta {
        return metalist.path.get_ident()
    }
    None
}

fn is_pymethod(item: &syn::ImplItem) -> bool {
    if let syn::ImplItem::Fn(_) = item {
        return true
    }
    false
}

fn clear_pymethod_attr(mut method: syn::ImplItemFn) -> syn::ImplItemFn {
    method.attrs.retain(|attr| {
        if let Some(ident) = attribute_ident(attr) {
            return !(
                ident == "new"
                    || ident == "staticmethod"
                    || ident == "pyo3"
            )
        }
        true
    });
    method
}

fn clear_non_pymethod_attr(method: syn::ImplItemFn) -> syn::ImplItemFn {
    method
}
