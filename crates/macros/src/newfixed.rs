use quote::*;
use proc_macro2::TokenStream as TokenStream2;
use proc_macro2::Ident as Ident2;
use proc_macro2::Span as Span2;
use syn::DeriveInput;
use super::newint::*;

mod main_impl;
use main_impl::*;


pub fn fixed_inharit_stream(input: &DeriveInput, member_name: &TokenStream2, member_type: &TokenStream2) -> TokenStream2 {
    let mut out = stream_inharent_ops(input, member_name, [
        "Add",
        "Sub",
        "Mul",
        "Div",
        "Rem",
        "BitAnd",
        "BitOr",
        "BitXor"
    ].into_iter());
    // TODO: shl shr
    out.extend(stream_inharent_fmts(input, member_name, [
        "Display",
        "Debug",
        "Binary",
        "Octal",
        "LowerExp",
        "LowerHex",
        "UpperExp",
        "UpperHex",
    ].into_iter()));
    out.extend(stream_newfixed_main_impl(input, member_name, member_type));
    out.extend(stream_inharent_ect(input, member_name, member_type));
    out.extend(stream_fixed_froms(input, member_type, [
        "u8",
        "usize",
    ].iter().cloned()));
    out.extend(stream_fixed_try_froms(input, member_type, [
        "u16",
        "u32",
        "u64",
        "u128",
        "i8",
        "i16",
        "i32",
        "i64",
        "i128",
        "isize",
    ].iter().cloned()));
    out.extend(stream_fixed_try_intos(input, member_name, member_type, [
        "u8",
        "u16",
        "u32",
        "u64",
        "u128",
        "usize",
        "i8",
        "i16",
        "i32",
        "i64",
        "i128",
        "isize",
    ].iter().cloned()));
    out
}


pub fn stream_fixed_froms<'a, I>(input: &DeriveInput, member_type: &TokenStream2, type_names: I) -> TokenStream2
where I: Iterator<Item=&'a str> {
    let mut out = TokenStream2::new();
    for name in type_names {
        out.extend(stream_fixed_from_part(input, member_type, Ident2::new(name, Span2::call_site().into())));
    }
    out
}

fn stream_fixed_from_part(input: &DeriveInput, member_type: &TokenStream2, from_type: Ident2) -> TokenStream2 {
    let (impl_generics, ty_generics, where_clause) = input.generics.split_for_impl();
    let name = &input.ident;
    quote!{
        impl #impl_generics From<#from_type> for #name #ty_generics #where_clause {
            fn from(value: #from_type) -> Self {
                Self(#member_type::from_num(value))
            }
        }
    }
}


pub fn stream_fixed_try_froms<'a, I>(input: &DeriveInput, member_type: &TokenStream2, type_names: I) -> TokenStream2
where I: Iterator<Item=&'a str> {
    let mut out = TokenStream2::new();
    for name in type_names {
        out.extend(stream_fixed_try_from_part(input, member_type, Ident2::new(name, Span2::call_site().into())));
    }
    out
}

fn stream_fixed_try_from_part(input: &DeriveInput, member_type: &TokenStream2, from_type: Ident2) -> TokenStream2 {
    let (impl_generics, ty_generics, where_clause) = input.generics.split_for_impl();
    let name = &input.ident;
    // FIXME: error
    quote!{
        impl #impl_generics TryFrom<#from_type> for #name #ty_generics #where_clause {
            type Error = ();
            fn try_from(value: #from_type) -> Result<Self, ()> {
                match #member_type::checked_from_num(value) {
                    Some(o) => Ok(Self(o)),
                    None => Err(())
                }
            }
        }
    }
}


pub fn stream_fixed_try_intos<'a, I>(input: &DeriveInput, member_name: &TokenStream2, member_type: &TokenStream2, type_names: I) -> TokenStream2
where I: Iterator<Item=&'a str> {
    let mut out = TokenStream2::new();
    for name in type_names {
        out.extend(stream_fixed_try_into_part(input, member_name, member_type, Ident2::new(name, Span2::call_site().into())));
    }
    out
}

fn stream_fixed_try_into_part(input: &DeriveInput, member_name: &TokenStream2, member_type: &TokenStream2, into_type: Ident2) -> TokenStream2 {
    let (impl_generics, ty_generics, where_clause) = input.generics.split_for_impl();
    let name = &input.ident;
    quote!{
        impl #impl_generics std::convert::TryFrom<#name #ty_generics> for #into_type #where_clause {
            type Error = ();
            fn try_from(value: #name #ty_generics) -> Result<Self, ()> {
                match #member_type::checked_to_num(value.#member_name) {
                    Some(o) => Ok(o),
                    None => Err(())
                }
            }
        }
    }
}
