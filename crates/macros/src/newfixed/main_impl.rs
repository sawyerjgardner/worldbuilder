use quote::*;
use syn::DeriveInput;
use proc_macro2::TokenStream as TokenStream2;

pub fn stream_newfixed_main_impl(input: &DeriveInput, member_name: &TokenStream2, member_type: &TokenStream2) -> TokenStream2 {
    let (impl_generics, ty_generics, where_clause) = input.generics.split_for_impl();
    let name = &input.ident;
    quote!{
        impl #impl_generics #name #ty_generics #where_clause {
            pub const BITS: u32 = #member_type::INT_NBITS +#member_type::FRAC_NBITS;
            pub const MIN: Self = Self(#member_type::MIN);
            pub const MAX: Self = Self(#member_type::MAX);
            pub fn abs_diff(self, other: Self) -> Self {
                Self(#member_type::abs_diff(self.#member_name, other.#member_name))
            }
            pub fn checked_add(self, other: Self) -> Option<Self> {
                #member_type::checked_add(self.#member_name, other.#member_name).map(|o| Self(o))
            }
            pub fn checked_div(self, other: Self) -> Option<Self> {
                #member_type::checked_div(self.#member_name, other.#member_name).map(|o| Self(o))
            }
            pub fn checked_div_euclid(self, other: Self) -> Option<Self> {
                #member_type::checked_div_euclid(self.#member_name, other.#member_name).map(|o| Self(o))
            }
            // FIXME:
            // pub fn checked_ilog(self, base: Self) -> Option<u32> {
            //     #member_type::checked_ilog(self.#member_name, base.#member_name)
            // }
            // pub fn checked_ilog10(self) -> Option<u32> {
            //     #member_type::checked_ilog10(self.#member_name)
            // }
            // pub fn checked_ilog2(self) -> Option<u32> {
            //     #member_type::checked_ilog2(self.#member_name)
            // }
            pub fn checked_mul(self, other: Self) -> Option<Self> {
                #member_type::checked_mul(self.#member_name, other.#member_name).map(|o| Self(o))
            }
            pub fn checked_neg(self) -> Option<Self> {
                #member_type::checked_neg(self.#member_name).map(|o| Self(o))
            }
            pub fn checked_next_multiple_of(self, other: Self) -> Option<Self> {
                #member_type::checked_next_multiple_of(self.#member_name, other.#member_name).map(|o| Self(o))
            }
            pub fn checked_next_power_of_two(self) -> Option<Self> {
                #member_type::checked_next_power_of_two(self.#member_name).map(|o| Self(o))
            }
            // FIXME:
            // pub fn checked_pow(self, other: u32) -> Option<Self> {
            //     #member_type::checked_pow(self.#member_name, other).map(|o| Self(o))
            // }
            pub fn checked_rem(self, other: Self) -> Option<Self> {
                #member_type::checked_rem(self.#member_name, other.#member_name).map(|o| Self(o))
            }
            pub fn checked_rem_euclid(self, other: Self) -> Option<Self> {
                #member_type::checked_rem_euclid(self.#member_name, other.#member_name).map(|o| Self(o))
            }
            pub fn checked_shl(self, rhs: u32) -> Option<Self> {
                #member_type::checked_shl(self.#member_name, rhs).map(|o| Self(o))
            }
            pub fn checked_shr(self, rhs: u32) -> Option<Self> {
                #member_type::checked_shr(self.#member_name, rhs).map(|o| Self(o))
            }
            pub fn checked_sub(self, other: Self) -> Option<Self> {
                #member_type::checked_sub(self.#member_name, other.#member_name).map(|o| Self(o))
            }
            pub fn count_ones(self) -> u32 {
                #member_type::count_ones(self.#member_name)
            }
            pub fn count_zeros(self) -> u32 {
                #member_type::count_zeros(self.#member_name)
            }
            // FIXME:
            // pub fn div_ceil(self, other: Self) -> Self {
            //     Self(#member_type::div_ceil(self.#member_name, other.#member_name))
            // }
            pub fn div_euclid(self, other: Self) -> Self {
                Self(#member_type::div_euclid(self.#member_name, other.#member_name))
            }
            pub fn from_be(self) -> Self {
                Self(#member_type::from_be(self.#member_name))
            }
            pub fn from_be_bytes(bytes: [u8; (Self::BITS/8) as usize]) -> Self {
                Self(#member_type::from_be_bytes(bytes))
            }
            pub fn from_le(self) -> Self {
                Self(#member_type::from_le(self.#member_name))
            }
            pub fn from_le_bytes(bytes: [u8; (Self::BITS/8) as usize]) -> Self {
                Self(#member_type::from_le_bytes(bytes))
            }
            pub fn from_ne_bytes(bytes: [u8; (Self::BITS/8) as usize]) -> Self {
                Self(#member_type::from_ne_bytes(bytes))
            }
            // FIXME
            // pub fn from_str_radix(src: &str, radix: u32) -> Result<Self, std::num::ParseIntError> {
            //     #member_type::from_str_radix(src, radix).map(|o| Self(o))
            // }
            // pub fn ilog(self, base: Self) -> u32 {
            //     #member_type::ilog(self.#member_name, base.#member_name)
            // }
            // pub fn ilog10(self) -> u32 {
            //     #member_type::ilog10(self.#member_name)
            // }
            // pub fn ilog2(self) -> u32 {
            //     #member_type::ilog2(self.#member_name)}
            pub fn is_power_of_two(self) -> bool {
                #member_type::is_power_of_two(self.#member_name)
            }
            pub fn leading_ones(self) -> u32 {
                #member_type::leading_ones(self.#member_name)
            }
            pub fn leading_zeros(self) -> u32 {
                #member_type::leading_zeros(self.#member_name)
            }
            pub fn next_multiple_of(self, other: Self) -> Self {
                Self(#member_type::next_multiple_of(self.#member_name, other.#member_name))
            }
            pub fn next_power_of_two(self) -> Self {
                Self(#member_type::next_power_of_two(self.#member_name))
            }
            pub fn overflowing_add(self, other: Self) -> (Self, bool) {
                let out = #member_type::overflowing_add(self.#member_name, other.#member_name);
                (Self(out.0), out.1)
            }
            pub fn overflowing_div(self, other: Self) -> (Self, bool) {
                let out = #member_type::overflowing_div(self.#member_name, other.#member_name);
                (Self(out.0), out.1)
            }
            pub fn overflowing_div_euclid(self, other: Self) -> (Self, bool) {
                let out = #member_type::overflowing_div_euclid(self.#member_name, other.#member_name);
                (Self(out.0), out.1)
            }
            pub fn overflowing_mul(self, other: Self) -> (Self, bool) {
                let out = #member_type::overflowing_mul(self.#member_name, other.#member_name);
                (Self(out.0), out.1)
            }
            pub fn overflowing_neg(self) -> (Self, bool) {
                let out = #member_type::overflowing_neg(self.#member_name);
                (Self(out.0), out.1)
            }
            // FIXME
            // pub fn overflowing_pow(self, other: u32) -> (Self, bool) {
            //     let out = #member_type::overflowing_pow(self.#member_name, other);
            //     (Self(out.0), out.1)
            // }
            // pub fn overflowing_rem(self, other: Self) -> (Self, bool) {
            //     let out = #member_type::overflowing_rem(self.#member_name, other.#member_name);
            //     (Self(out.0), out.1)
            // }
            // pub fn overflowing_rem_euclid(self, other: Self) -> (Self, bool) {
            //     let out = #member_type::overflowing_rem_euclid(self.#member_name, other.#member_name);
            //     (Self(out.0), out.1)
            // }
            pub fn overflowing_shl(self, rhs: u32) -> (Self, bool) {
                let out = #member_type::overflowing_shl(self.#member_name, rhs);
                (Self(out.0), out.1)
            }
            pub fn overflowing_shr(self, rhs: u32) -> (Self, bool) {
                let out = #member_type::overflowing_shr(self.#member_name, rhs);
                (Self(out.0), out.1)
            }
            pub fn overflowing_sub(self, other: Self) -> (Self, bool) {
                let out = #member_type::overflowing_sub(self.#member_name, other.#member_name);
                (Self(out.0), out.1)
            }
            // FIXME
            // pub fn pow(self, other: u32) -> Self {
            //     Self(#member_type::pow(self.#member_name, other))
            // }
            pub fn rem_euclid(self, other: Self) -> Self {
                Self(#member_type::rem_euclid(self.#member_name, other.#member_name))
            }
            pub fn reverse_bits(self) -> Self {
                Self(#member_type::reverse_bits(self.#member_name))
            }
            pub fn rotate_left(self, n: u32) -> Self {
                Self(#member_type::rotate_left(self.#member_name, n))
            }
            pub fn rotate_right(self, n: u32) -> Self {
                Self(#member_type::rotate_right(self.#member_name, n))
            }
            pub fn saturating_add(self, other: Self) -> Self {
                Self(#member_type::saturating_add(self.#member_name, other.#member_name))
            }
            pub fn saturating_div(self, other: Self) -> Self {
                Self(#member_type::saturating_div(self.#member_name, other.#member_name))
            }
            pub fn saturating_mul(self, other: Self) -> Self {
                Self(#member_type::saturating_mul(self.#member_name, other.#member_name))
            }
            // FIXME
            // pub fn saturating_pow(self, other: u32) -> Self {
            //     Self(#member_type::saturating_pow(self.#member_name, other))
            // }
            pub fn saturating_sub(self, other: Self) -> Self {
                Self(#member_type::saturating_sub(self.#member_name, other.#member_name))
            }
            pub fn swap_bytes(self) -> Self {
                Self(#member_type::swap_bytes(self.#member_name))
            }
            pub fn to_be(self) -> Self {
                Self(#member_type::to_be(self.#member_name))
            }
            pub fn to_be_bytes(self) -> [u8; (Self::BITS/8) as usize] {
                #member_type::to_be_bytes(self.#member_name)
            }
            pub fn to_le(self) -> Self {
                Self(#member_type::to_le(self.#member_name))
            }
            pub fn to_le_bytes(self) -> [u8; (Self::BITS/8) as usize] {
                #member_type::to_le_bytes(self.#member_name)
            }
            pub fn to_ne_bytes(self) -> [u8; (Self::BITS/8) as usize] {
                #member_type::to_ne_bytes(self.#member_name)
            }
            pub fn trailing_ones(self) -> u32 {
                #member_type::trailing_ones(self.#member_name)
            }
            pub fn trailing_zeros(self) -> u32 {
                #member_type::trailing_zeros(self.#member_name)
            }
            pub fn wrapping_add(self, other: Self) -> Self {
                Self(#member_type::wrapping_add(self.#member_name, other.#member_name))
            }
            pub fn wrapping_div(self, other: Self) -> Self {
                Self(#member_type::wrapping_div(self.#member_name, other.#member_name))
            }
            pub fn wrapping_div_euclid(self, other: Self) -> Self {
                Self(#member_type::wrapping_div_euclid(self.#member_name, other.#member_name))
            }
            pub fn wrapping_mul(self, other: Self) -> Self {
                Self(#member_type::wrapping_mul(self.#member_name, other.#member_name))
            }
            pub fn wrapping_neg(self) -> Self {
                Self(#member_type::wrapping_neg(self.#member_name))
            }
            // FIXME
            // pub fn wrapping_pow(self, exp: u32) -> Self {
            //     Self(#member_type::wrapping_pow(self.#member_name, exp))
            // }
            // pub fn wrapping_rem(self, other: Self) -> Self {
            //     Self(#member_type::wrapping_rem(self.#member_name, other.#member_name))
            // }
            // pub fn wrapping_rem_euclid(self, other: Self) -> Self {
            //     Self(#member_type::wrapping_rem_euclid(self.#member_name, other.#member_name))
            // }
            pub fn wrapping_shl(self, rhs: u32) -> Self {
                Self(#member_type::wrapping_shl(self.#member_name, rhs))
            }
            pub fn wrapping_shr(self, rhs: u32) -> Self {
                Self(#member_type::wrapping_shr(self.#member_name, rhs))
            }
            pub fn wrapping_sub(self, other: Self) -> Self {
                Self(#member_type::wrapping_sub(self.#member_name, other.#member_name))
            }

            pub fn ceil(self) -> Self {
                Self(#member_type::ceil(self.#member_name))
            }
            pub fn floor(self) -> Self {
                Self(#member_type::floor(self.#member_name))
            }
            pub fn fract(self) -> Self {
                Self(#member_type::frac(self.#member_name))
            }
            pub fn round(self) -> Self {
                Self(#member_type::round(self.#member_name))
            }
        }
    }
}
