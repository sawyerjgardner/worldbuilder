use std::ops::Deref;
use std::sync::Mutex;
use std::sync::MutexGuard;

use itertools::Itertools;
use pyo3::prelude::*;
use pyo3::exceptions::PyValueError;
use pyo3::PyTraverseError;
use pyo3::PyVisit;
use world_builder as wb;
use world_builder::RefHole;
use world_builder::SideFull;


mod tomypy;
mod pylibmesh;
use tomypy::*;

pub mod mesh {
    pub use super::pylibmesh::*;
}
pub use mesh::WorldBuilderPyModuleMesh;


struct PyHoleWrapper(Py<PyHole>);
impl PyHoleWrapper {
    fn clone_ref(&self, py: Python) -> Self {
        Self(self.0.clone_ref(py))
    }
}
impl AsRef<wb::Hole> for PyHoleWrapper {
    fn as_ref(&self) -> &wb::Hole {
        &self.0.get().0
    }
}
impl From<Py<PyHole>> for PyHoleWrapper {
    fn from(value: Py<PyHole>) -> Self {
        Self(value)
    }
}


#[pyclass(name="Hole", frozen)]
pub struct PyHole(Box<wb::Hole>);
#[pymethods]
impl PyHole {
    #[new]
    pub fn new(points: Vec<wb::Point>) -> PyResult<Self> {
        wb::Hole::new(points.into_iter()).to_mypy() 
    }
    pub fn length(&self) -> wb::HoleSize {
        self.0.length()
    }
    pub fn normal(&self) -> wb::Direction {
        self.0.normal().into()
    }
    pub fn duplicate(&self, other: &PyHole) -> bool {
        self.0.duplicate(&other.0)
    }
    pub fn covers(&self, other: &PyHole) -> bool {
        self.0.covers(&other.0)
    }
    pub fn stacked(&self, other: &PyHole) -> bool {
        self.0.stacked(&other.0)
    }
    pub fn touching(&self, other: &PyHole) -> bool {
        self.0.touching(&other.0)
    }
    pub fn reshape(slf: Py<Self>) -> PyHoleReshape {
        PyHoleReshape(wb::HoleReshape::new(PyHoleWrapper(slf)))
    }
    pub fn debug_mesh(&self) -> PyResult<mesh::PyMesh> {
        self.0.debug_mesh().to_mypy()  
    }
    /// Display
    pub fn __str__(&self) -> String {
        format!("{}", self.0)
    }
    /// Debug
    pub fn __repr__(&self) -> String {
        format!("{:?}", self.0)
    }
    /// SideFull
    pub fn points<'a>(&self) -> PyResult<Vec<wb::Point>> {
        Ok(self.0.points().into())
    }
    /// SideFull
    pub fn side_full(slf: Py<Self>) -> PySide {
        let unref = Box::as_ref(&slf.get().0).side_full().unref();
        unsafe {
            PySide(wb::Side::from_unref(slf.into(), unref))
        }
    }
    /// SideFull
    pub fn sides_stacked(slf: Py<Self>, other: Py<Self>, py: Python<'_>)
                         -> Vec<(PySide, PySide)> {
        wb::SideFull::sides_stacked(
            Box::as_ref(&slf.get().0), 
            Box::as_ref(&other.get().0)
        ).into_iter().map(|(side1, side2)| {
            (
                PySide(side1.reref_hole(PyHoleWrapper(slf.clone_ref(py))).unwrap()),
                PySide(side2.reref_hole(PyHoleWrapper(slf.clone_ref(py))).unwrap())
            )
        }).collect()
    }
    /// SideFull
    pub fn sides_touching<'a>(slf: Py<Self>, other: Py<Self>, py: Python<'_>)
                              -> Vec<(PySide, PySide)> {
        wb::SideFull::sides_touching(
            Box::as_ref(&slf.get().0), 
            Box::as_ref(&other.get().0)
        ).into_iter().map(|(side1, side2)| {
            (
                PySide(side1.reref_hole(PyHoleWrapper(slf.clone_ref(py))).unwrap()),
                PySide(side2.reref_hole(PyHoleWrapper(slf.clone_ref(py))).unwrap())
            )
        }).collect()
    }
    /// PartialEq
    pub fn __eq__(&self, other: &Self) -> bool {
        self.0 == other.0
    }
    /// PartialEq
    pub fn __ne__(&self, other: &Self) -> bool {
        self.0 != other.0
    }
    // /// IntoPolygons TODO:
    // fn poly(&self, mesher: Bound<PyAny>) -> PyResult<()> {
    //     Ok(())
    // }
    /// IntoDebugMesh
    fn debug_msh(&self, m: &mesh::PyBaseMesher, py: Python) -> PyResult<()> {
        m.with_real_mesher(py, |mesher| {
            let hole = Box::as_ref(&self.0);
            wb::mesh::IntoDebugMesh::msh(hole, mesher).to_mypy()
        })
    }
}
impl ToMypy for Box<wb::Hole> {
    type PyType = PyHole;
    fn to_mypy(self) -> Self::PyType {
        PyHole(self)
    }
}


#[pyclass(name="Side", frozen)]
pub struct PySide(wb::Side<PyHoleWrapper>);
#[pymethods]
impl PySide {
    #[new]
    pub fn new(hole: Py<PyHole>, start: wb::HoleSize, length: wb::HoleSize) -> PyResult<Self> {
        wb::Side::new(PyHoleWrapper(hole), start, length).to_mypy() 
    }
    pub fn start(&self) -> usize {
        self.0.start().into()
    }
    pub fn end(&self) -> usize {
        self.0.end().into()
    }
    pub fn length(&self) -> usize {
        self.0.length().into()
    }
    pub fn all_points(&self) -> Vec<wb::Point> {
        self.0.all_points().cloned().collect()
    }
    pub fn start_points(&self) -> Vec<wb::Point> {
        self.0.start_points().cloned().collect()
    }
    pub fn end_points(&self) -> Vec<wb::Point> {
        self.0.end_points().cloned().collect()
    }
    pub fn first_point(&self) -> wb::Point {
        self.0.first_point().clone()
    }
    pub fn last_point(&self) -> wb::Point {
        self.0.last_point().clone()
    }
    pub fn inverse(&self, py: Python) -> Option<Self> {
        let side = self.clone(py).0;
        side.inverse().to_mypy()
    }
    pub fn overlapping(&self, other: &Self) -> PyResult<bool> {
        self.0.overlapping(&other.0).to_mypy()
    }
    /// Clone
    pub fn clone(&self, py: Python) -> Self {
        let hole = self.0.as_hole().clone_ref(py);
        let side: wb::Side<_> = self.0.reref_hole(hole).unwrap();
        Self(side)
    }
    /// Refhole
    pub fn hole(&self, py: Python) -> Py<PyHole> {
        self.0.as_hole().0.clone_ref(py)
    }
    /// Display
    pub fn __str__(&self) -> String {
        format!("{}", self.0)
    }
    /// Debug
    pub fn __repr__(&self) -> String {
        format!("{:?}", self.0)
    }
}
impl ToMypy for wb::Side<PyHoleWrapper> {
    type PyType = PySide;
    fn to_mypy(self) -> Self::PyType {
        PySide(self)
    }
}


#[pyclass(name="SideFloat", frozen)]
pub struct PySideFloat(wb::SideFloat<PyHoleWrapper>);
#[pymethods]
impl PySideFloat {
    #[new]
    pub fn new(hole: Py<PyHole>, start: wb::HoleSizeFloat, length: wb::HoleSizeFloat) -> PyResult<Self> {
        wb::SideFloat::new(PyHoleWrapper(hole), start, length,).to_mypy()  
    }
    pub fn start(&self) -> wb::HoleSizeFloat {
        self.0.start()
    }
    pub fn end(&self) -> wb::HoleSizeFloat {
        self.0.end()
    }
    pub fn length(&self) -> wb::HoleSizeFloat {
        self.0.length()
    }
    pub fn round_in(&self, py: Python) -> Option<PySide> {
        self.clone(py).0.round_in().to_mypy()
    }
    pub fn round_out(&self, py: Python) -> PySide {
        self.clone(py).0.round_out().to_mypy()
    }
    pub fn round(&self, py: Python) -> Option<PySide> {
        self.clone(py).0.round().to_mypy()
    }
    pub fn point_start(&self) -> wb::Point {
        self.0.point_start()
    }
    pub fn point_end(&self) -> wb::Point {
        self.0.point_end()
    }
    pub fn make_point_start(&self) -> wb::Point {
        self.0.make_point_start()
    }
    pub fn make_point_end(&self) -> wb::Point {
        self.0.make_point_end()
    }
    pub fn inverse(&self, py: Python) -> Option<Self> {
        self.clone(py).0.inverse().to_mypy()
    }
    pub fn overlapping(&self, other: &Self) -> PyResult<bool> {
        self.0.overlapping(&other.0).to_mypy()
    }
    /// RefHole
    pub fn hole(&self, py: Python) -> Py<PyHole> {
        self.0.as_hole().0.clone_ref(py)
    }
    /// Clone
    pub fn clone(&self, py: Python) -> Self {
        let hole = self.0.as_hole().clone_ref(py);
        let side: wb::SideFloat<_> = self.0.reref_hole(hole).unwrap();
        Self(side)
    }
    /// Debug
    pub fn __repr__(&self) -> String {
        format!("{:?}", self.0)
    }
}
impl ToMypy for wb::SideFloat<PyHoleWrapper> {
    type PyType = PySideFloat;
    fn to_mypy(self) -> Self::PyType {
        PySideFloat(self) 
    }
}


#[pyclass(name="HoleMeta", frozen)]
pub struct PyHoleMeta(wb::HoleMeta<PyHoleWrapper, PyObject>);
#[pymethods]
impl PyHoleMeta {
    #[staticmethod]
    pub fn from_sides(py: Python, other_side_data: Vec<(Bound<PySide>, PyObject)>) -> PyResult<Vec<PyHoleMeta>> {
        wb::HoleMeta::from_sides(
            other_side_data.into_iter().map(|(side, obj)| {
                (side.get().clone(py).0, obj)
            }),
            |points| {
                Ok(PyHoleWrapper(Py::new(
                    py, 
                    wb::Hole::new(points.into_iter()).to_mypy()?
                )?))
            }
        ).to_mypy() 
    }
    #[staticmethod]
    pub fn from_positions(hole: Py<PyHole>, positions: Vec<(wb::HoleSize, PyObject)>) -> PyResult<PyHoleMeta> {
        wb::HoleMeta::from_positions(
            PyHoleWrapper(hole),
            positions.into_iter().map(|(pos, data)| (pos.into(),data))
        ).to_mypy()
    }
    #[staticmethod]
    pub fn from_lengths(hole: Py<PyHole>, start: wb::HoleSize, lengths: Vec<(wb::HoleSize, PyObject)>) -> PyResult<PyHoleMeta> {
        wb::HoleMeta::from_lengths(
            PyHoleWrapper(hole),
            start,
            lengths.into_iter()
        ).to_mypy() 
    }
    pub fn side(&self, index: usize, py: Python) -> PySide {
        unsafe {
            let hole = self.0.as_hole();
            let side = self.0.as_unref().side(hole.as_ref(), index);
            wb::Side::from_unref(hole.clone_ref(py), side).to_mypy()
        }
    }
    pub fn data(&self, index: usize, py: Python) -> PyObject {
        self.0.data(index).clone_ref(py)
    }
    pub fn len(&self) -> usize {
        self.0.len()
    }
    /// RefHole
    pub fn hole(&self, py: Python) -> Py<PyHole> {
        self.0.as_hole().0.clone_ref(py)
    }
    pub fn __traverse__(&self, visit: PyVisit<'_>) -> Result<(), PyTraverseError> {
        for x in 0..self.0.len() {
            visit.call(self.0.data(x))?
        };
        Ok(())
    }
}
impl ToMypy for wb::HoleMeta<PyHoleWrapper, PyObject> {
    type PyType = PyHoleMeta;
    fn to_mypy(self) -> Self::PyType {
        PyHoleMeta(self) 
    }
}


#[pyclass(name="HoleMetaFloat", frozen)]
pub struct PyHoleMetaFloat(wb::HoleMetaFloat<PyHoleWrapper, PyObject>);
#[pymethods]
impl PyHoleMetaFloat {
    #[staticmethod]
    pub fn from_positions(hole: Py<PyHole>, positions: Vec<(wb::HoleSizeFloat, PyObject)>) -> PyResult<PyHoleMetaFloat> {
        wb::HoleMetaFloat::from_positions(
            PyHoleWrapper(hole),
            positions.into_iter()
        ).to_mypy() 
    }
    #[staticmethod]
    pub fn from_lengths(hole: Py<PyHole>, start: wb::HoleSizeFloat, lengths: Vec<(wb::HoleSizeFloat, PyObject)>) -> PyResult<PyHoleMetaFloat> {
        wb::HoleMetaFloat::from_lengths(
            PyHoleWrapper(hole),
            start,
            lengths.into_iter()
        ).to_mypy() 
    }
    pub fn side(&self, index: usize, py: Python) -> PySideFloat {
        unsafe {
            let hole = self.0.as_hole();
            let side = self.0.as_unref().side(hole.as_ref(), index);
            wb::SideFloat::from_unref(hole.clone_ref(py), side).to_mypy()
        }
    }
    pub fn data(&self, index: usize, py: Python) -> PyObject {
        self.0.data(index).clone_ref(py)
    }
    pub fn len(&self) -> usize {
        self.0.len()
    }
    ///RefHole
    pub fn hole(&self, py: Python) -> Py<PyHole> {
        self.0.as_hole().0.clone_ref(py)
    }
    pub fn __traverse__(&self, visit: PyVisit<'_>) -> Result<(), PyTraverseError> {
        for x in 0..self.0.len() {
            visit.call(self.0.data(x))?
        };
        Ok(())
    }
}
impl ToMypy for wb::HoleMetaFloat<PyHoleWrapper, PyObject> {
    type PyType = PyHoleMetaFloat;
    fn to_mypy(self) -> Self::PyType {
        PyHoleMetaFloat(self)
    }
}


#[pyclass(name="SideMap")]
pub struct PySideMap(wb::SideMap<PyHoleWrapper, PyObject>);
#[pymethods]
impl PySideMap {
    #[new]
    pub fn new(hole: Py<PyHole>) -> Self {
        Self(wb::SideMap::new(PyHoleWrapper(hole)))
    }
    pub fn len(&self) -> wb::HoleSize {
        self.0.len()
    }
    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }
    pub fn is_full(&self) -> bool {
        self.0.is_full()
    }
    pub fn insert(&mut self, side: &PySide, value: PyObject) -> PyResult<wb::HoleSize> {
        self.0.insert(
            side.0.reref_hole_base::<wb::Side<_>>(),
            value
        ).to_mypy()
    }
    pub fn remove(&mut self, index: wb::HoleSize, py: Python) -> Option<(PySide, PyObject)> {
        unsafe {
            self.0.as_unref_mut().remove(index).map(|(side, data)| {
                (
                    wb::Side::from_unref(
                        self.0.as_hole().clone_ref(py), 
                        side
                    ).to_mypy(), 
                    data
                )
            })
        }
    }
    pub fn clear(&mut self) {
        self.0.clear()
    }
    pub fn pop(&mut self, py: Python) -> Option<(PySide, PyObject)> {
        unsafe {
            self.0.as_unref_mut().pop().map(|(side, data)| {
                (
                    wb::Side::from_unref(
                        self.0.as_hole().clone_ref(py), 
                        side
                    ).to_mypy(),
                    data
                )
            })
        }
    }
    pub fn pop_value(&mut self) -> Option<PyObject> {
        self.0.pop_value()
    }
    pub fn side_value(&self, index: wb::HoleSize, py: Python) -> Option<(PySide, PyObject)> {
        self.0.as_unref().side_value(index).map(|(side, data)| {
            (
                unsafe {
                    wb::Side::from_unref(
                        self.0.as_hole().clone_ref(py), 
                        side
                    ).to_mypy() 
                },
                data.clone_ref(py)
            )
        })
    }
    pub fn side(&self, index: wb::HoleSize, py: Python) -> Option<PySide> {
        self.0.as_unref().side(index).map(|side| {
            unsafe {
                wb::Side::from_unref(
                    self.0.as_hole().clone_ref(py), 
                    side
                ).to_mypy() 
            }
        })
    }
    pub fn value(&self, index: wb::HoleSize, py: Python) -> Option<PyObject> {
        self.0.value(index).map(|o| o.clone_ref(py))
    }
    pub fn gaps(&self, py: Python) -> Self {
        unsafe {
            let hole = self.0.as_hole();
            let gaps = self.0.as_unref().gaps(hole.as_ref());
            Self(wb::SideMap::from_unref(
                hole.clone_ref(py), 
                gaps.into_map(|o| o.into_py(py))
            ))
        }
    }
    pub fn map(&self, func: PyObject, py: Python) -> PyResult<Self> {
        unsafe {
            Ok(Self(wb::SideMap::from_unref(
                self.0.as_hole().clone_ref(py),
                self.0.as_unref().try_map(|o| {
                    func.call1(py, (o,))
                })? 
            )))
        }
    }
    pub fn apply_map(slf: PyRef<Self>, func: PyObject) -> PyResult<PyRef<Self>> {
        for (_, data) in slf.0.as_unref().iter() {
            func.call1(slf.py(), (data,))?;
        }
        Ok(slf)
    }
    pub fn iter(&self, py: Python) -> Vec<(PySide, PyObject)> {
        self.sides(py).into_iter().zip(self.values(py)).collect()
    }
    pub fn sides(&self, py: Python) -> Vec<PySide> {
        self.0.as_unref().sides().map(|o| {
            let hole = self.0.as_hole().clone_ref(py);
            unsafe {
                PySide(wb::Side::from_unref(hole, o))
            }
        }).collect()
    }
    pub fn values(&self, py: Python) -> Vec<PyObject> {
        self.0.values().map(|o| o.clone_ref(py)).collect()
    }
    //TODO: into_meta

    ///Clone
    pub fn clone(&self, py: Python) -> Self {
        unsafe {
            Self(wb::SideMap::from_unref(
                self.0.as_hole().clone_ref(py),
                self.0.as_unref().map(|o| o.clone_ref(py))
            ))
        }
    }
    ///RefHole
    pub fn hole(&self, py: Python) -> Py<PyHole> {
        self.0.as_hole().0.clone_ref(py)
    }
    pub fn __traverse__(&self, visit: PyVisit<'_>) -> Result<(), PyTraverseError> {
        for data in self.0.values() {
            visit.call(data)?
        };
        Ok(())
    }
}
impl ToMypy for wb::SideMap<PyHoleWrapper, PyObject> {
    type PyType = PySideMap;
    fn to_mypy(self) -> Self::PyType {
        PySideMap(self)
    }
}


#[pyclass(name="TouchingGraph", frozen)]
pub struct PyTouchingGraph(wb::TouchingGraph<PyHoleWrapper>);
#[pymethods]
impl PyTouchingGraph {
    #[new]
    pub fn new(holes: Vec::<Py<PyHole>>) -> Self {
        PyTouchingGraph (
            wb::TouchingGraph::new(
                holes.into_iter()
                     .map(|o| PyHoleWrapper(o))
                     .collect_vec()
            )
        )
    }
    pub fn holes_len(&self) -> usize {
        self.0.holes_len()
    }
    pub fn sides_len(&self, hole_index: usize) -> Option<wb::HoleSize> {
        self.0.sides_len(hole_index)
    }
    pub fn valid_index(&self, index: wb::TouchingIndex) -> bool {
        self.0.valid_index(index)
    }
    pub fn exposed_points(&self) -> Vec<Vec<wb::Point>> {
        self.0.exposed_points()
    }
    pub fn walker(slf: Py<PyTouchingGraph>, py: Python<'_>, index: wb::TouchingIndex) -> Option<PyTouchingWalker> {
        if !slf.borrow(py).valid_index(index) {
            return None;
        };
        Some(PyTouchingWalker {index, graph: slf})
    }
    pub fn side(&self, index: wb::TouchingIndex, py: Python) -> Option<PySide> {
        unsafe {
            let side = self.0.as_unref().side(index)?;
            let hole = self.0.as_hole(index.hole_index)?.clone_ref(py);
            Some(PySide(wb::Side::from_unref(hole, side)))
        }
    }
    pub fn touching_index(&self, index: wb::TouchingIndex) -> Option<wb::TouchingIndex> {
        self.0.touching_index(index)
    }
    pub fn sidemap(&self, py: Python<'_>) -> Vec<PySideMap> {
        let sidemaps = self.0.as_unref().sidemap();
        let holes = self.0.as_holes().into_iter();
        Iterator::zip(holes, sidemaps).map(|(hole, unref)| {
            unsafe {PySideMap(wb::SideMap::from_unref(
                hole.clone_ref(py), 
                unref.into_map(|o| o.into_py(py))
            ))}
        }).collect()
    }
    /// RefHole
    pub fn holes(&self, py: Python) -> Vec<Py<PyHole>> {
        self.0.as_holes().into_iter().map(|o| {
            o.0.clone_ref(py)
        }).collect()
    }
    pub fn hole(&self, py: Python, index: usize) -> Option<Py<PyHole>> {
        self.0.as_hole(index).map(|o| {
            o.0.clone_ref(py)
        })
    }
}
impl ToMypy for wb::TouchingGraph<PyHoleWrapper> {
    type PyType = PyTouchingGraph;
    fn to_mypy(self) -> Self::PyType {
        PyTouchingGraph(self)
    }
}


#[pyclass(name="TouchingWalker")]
pub struct PyTouchingWalker{
    index: wb::TouchingIndex,
    graph: Py<PyTouchingGraph>
}
#[pymethods]
impl PyTouchingWalker {
    #[new]
    pub fn new(graph: Py<PyTouchingGraph>, index: wb::TouchingIndex) -> Self {
        Self{index, graph}
    }
    pub fn index(&self) -> wb::TouchingIndex {
        self.index
    }
    pub fn graph(&self, py: Python) -> Py<PyTouchingGraph> {
        self.graph.clone_ref(py)
    }
    pub fn side(&self, py: Python<'_>) -> PySide {
        self.graph.borrow(py).side(self.index, py).unwrap()
    }
    pub fn back(&mut self, py: Python<'_>) {
        let graph = self.graph.borrow(py);
        let mut walker = graph.0.walker(self.index).unwrap();
        walker.back();
        self.index = walker.index()
    }
    pub fn forword(&mut self, py: Python<'_>) {
        let graph = self.graph.borrow(py);
        let mut walker = graph.0.walker(self.index).unwrap();
        walker.forword();
        self.index = walker.index()
    }
    pub fn jump(&mut self, py: Python<'_>) {
        let graph = self.graph.borrow(py);
        let mut walker = graph.0.walker(self.index).unwrap();
        walker.jump();
        self.index = walker.index()
    }
    pub fn pivit_back(&mut self, py: Python<'_>) {
        let graph = self.graph.borrow(py);
        let mut walker = graph.0.walker(self.index).unwrap();
        walker.pivit_back();
        self.index = walker.index()
    }
    pub fn pivit_frunt(&mut self, py: Python<'_>) {
        let graph = self.graph.borrow(py);
        let mut walker = graph.0.walker(self.index).unwrap();
        walker.pivit_frunt();
        self.index = walker.index()
    }
    /// Clone
    pub fn clone(&self, py: Python) -> Self {
        Self {
            index: self.index.clone(),
            graph: self.graph.clone_ref(py)
        }
    }
    /// Debug
    pub fn __repr__(&self, py: Python) -> String {
        format!("{:?}", self.graph.borrow(py).0.walker(self.index).unwrap())
    }
}


#[pyclass(name="HoleReshape")]
pub struct PyHoleReshape(wb::HoleReshape<PyHoleWrapper>);
#[pymethods]
impl PyHoleReshape {
    #[new]
    pub fn new(hole: Py<PyHole>) -> Self {
        PyHoleReshape(wb::HoleReshape::new(PyHoleWrapper(hole)))
    }
    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }
    pub fn insert<'a>(mut slf: PyRefMut<'a, Self>,
                      side: &PySideFloat,
                      points: Vec<wb::Point>)
                      -> PyResult<PyRefMut<'a, Self>> {
        let side = side.0.reref_hole_base();
        match slf.0.insert(side, points) {
            Ok(_)    => Ok(slf),
            Err(err) => Err(PyValueError::new_err(format!("{:?}", err)))
        }
    }
    pub fn apply(&self) -> Vec<wb::Point> {
        self.0.reref_hole_base::<wb::HoleReshape<_>>().apply()
    }
    /// RefHole
    pub fn hole(&self, py: Python) -> Py<PyHole> {
        self.0.as_hole().0.clone_ref(py)
    }
}


#[derive(Debug,Clone,Copy)]
pub struct StaticString(pub &'static str);
impl StaticString {
    pub fn new(string: &str) -> Self {
        Self(Self::get(string))
    }
    pub fn get(string: &str) -> &'static str {
        static STRING_CACHE: Mutex<Vec<&'static str>> = Mutex::new(Vec::new());
        let mut cache = STRING_CACHE.lock().unwrap();
        let index = cache.iter().position(|o| *o == string).unwrap_or_else(|| {
            let index = cache.len();
            // FIXME: MEMORY LEAK!
            cache.push(Box::<str>::leak(string.into()));
            index
        });
        cache[index]
    }
}
impl From<&'static str> for StaticString {
    fn from(value: &'static str) -> Self {
        StaticString(value)
    }
}
impl<'a> From<StaticString> for &'a str {
    fn from(value: StaticString) -> Self {
        value.0
    }
}
impl ToMypy for StaticString {
    type PyType = &'static str;
    fn to_mypy(self) -> Self::PyType {
        self.into() 
    }
}
impl<'py> FromPyObject<'py> for StaticString {
    fn extract_bound(ob: &Bound<'py, PyAny>) -> PyResult<Self> {
        Ok(StaticString::new(ob.extract::<&str>()?))
    }
}



pub struct WorldBuilderPyModule();
impl WorldBuilderPyModule {
    pub fn innit(m: &Bound<PyModule>) -> PyResult<()> {
        Self::set(m);
        Self::apply_attributes(m)
    }
    pub fn apply_attributes(m: &Bound<PyModule>) -> PyResult<()> {
        m.add_submodule(&WorldBuilderPyModuleMesh::new(m.py())?)?;
        m.add_class::<wb::Point>()?;
        m.add_class::<wb::Direction>()?;
        m.add_class::<wb::TouchingIndex>()?;
        m.add_class::<PyHole>()?;
        m.add_class::<PySide>()?;
        m.add_class::<PySideMap>()?;
        m.add_class::<PySideFloat>()?;
        m.add_class::<PyHoleMeta>()?;
        m.add_class::<PyHoleMetaFloat>()?;
        m.add_class::<PyTouchingGraph>()?;
        m.add_class::<PyTouchingWalker>()?;
        m.add_class::<PyHoleReshape>()?;
        Ok(())
    }
    pub fn get(py: Python) -> PyResult<Bound<PyModule>> {
        match MutexGuard::deref(&Self::get_lock()) {
            Some(module) => Ok(module.bind(py).clone()),
            None => Err(PyValueError::new_err("could not find would builder module"))
        }
    }
    fn set(m: &Bound<PyModule>) {
        Self::get_lock().replace(m.clone().unbind());
    }
    fn get_lock<'a>() -> MutexGuard<'a, Option<Py<PyModule>>> {
        static WORLD_BUILDER_PY_MODULE: Mutex<Option<Py<PyModule>>> = Mutex::new(None);
        WORLD_BUILDER_PY_MODULE.lock().unwrap()
    }
}


#[pymodule(name="WorldBuilder")]
fn world_builder_py(m: &Bound<PyModule>) -> PyResult<()> {
    WorldBuilderPyModule::innit(m)
}
