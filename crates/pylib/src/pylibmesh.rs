use pyo3::prelude::*;
use crate::WorldBuilderPyModule;


mod pyprotocol;
mod pymesh;
mod pysimplemesh;
mod pyunlayeredmesh;
mod pydebugshader;
pub use pyprotocol::*;
pub use pymesh::*;
pub use pysimplemesh::*;
pub use pyunlayeredmesh::*;
pub use pydebugshader::*;


pub struct WorldBuilderPyModuleMesh();
impl WorldBuilderPyModuleMesh {
    const NAME: &str = "mesh";
    pub fn new(py: Python) -> PyResult<Bound<PyModule>> {
        let m = PyModule::new_bound(py, Self::NAME)?;
        Self::apply_attributes(&m)?;
        Ok(m)
    }
    pub fn apply_attributes<'a, 'py>(m: &'a Bound<'py, PyModule>) -> PyResult<&'a Bound<'py, PyModule>> {
        m.add_class::<PrMeshDataTypeDummy>()?;
        m.add_class::<PrMeshDataDummy>()?;
        m.add_class::<PrIdentifyMeshDummy>()?;
        m.add_class::<PrShaderDummy>()?;
        m.add_class::<PrShaderAndIdentifyMeshDummy>()?;
        m.add_class::<PyMesh>()?;
        m.add_class::<PyBaseMesher>()?;
        m.add_class::<PySimpleMesh>()?;
        m.add_class::<PySimpleMeshLayer>()?;
        m.add_class::<PySimpleMesher>()?;
        m.add_class::<PySimpleMesherVert>()?;
        m.add_class::<PySimpleMesherFace>()?;
        m.add_class::<PySimpleShader>()?;
        m.add_class::<PrHasSimpleShaderMesherDummy>()?;
        m.add_class::<PyDebugShader>()?;
        m.add_class::<PrHasDebugShaderMesherDummy>()?;
        m.add_class::<PyUnlayeredMesh>()?;
        m.add_class::<PyUnlayeredMesher>()?;
        m.add_class::<PyUnlayeredMesherVert>()?;
        m.add_class::<PyUnlayeredMesherFace>()?;
        m.add_class::<PyUnlayeredMesherTextured>()?;
        Ok(m)
    }
    pub fn get(py: Python) -> PyResult<Bound<PyAny>> {
        WorldBuilderPyModule::get(py)?.getattr(Self::NAME)
    }
}