use pyo3::prelude::*;
use pyo3::exceptions::PyValueError;
use world_builder as wb;


pub trait ToMypy {
    type PyType;
    fn to_mypy(self) -> Self::PyType;
}
pub trait ToMypy1 {
    type PyType;
    fn to_mypy1(self, py: Python) -> Self::PyType;
}

impl<T> ToMypy1 for T 
where T: ToMypy {
    type PyType = <T as ToMypy>::PyType;
    fn to_mypy1(self, _py: Python) -> Self::PyType {
        self.to_mypy()
    }
}

impl ToMypy for PyErr {
    type PyType = PyErr;
    fn to_mypy(self) -> Self::PyType {
        self
    }
}
impl ToMypy for () {
    type PyType = ();
    fn to_mypy(self) -> Self::PyType {
        self
    }
}
impl ToMypy for bool {
    type PyType = bool;
    fn to_mypy(self) -> Self::PyType {
        self
    }
}
impl ToMypy for wb::HoleSize {
    type PyType = wb::HoleSize;
    fn to_mypy(self) -> Self::PyType {
        self
    }
}
impl ToMypy for wb::HoleSizeFloat {
    type PyType = wb::HoleSizeFloat;
    fn to_mypy(self) -> Self::PyType {
        self
    }
}
impl ToMypy for wb::mesh::FaceID {
    type PyType = usize;
    fn to_mypy(self) -> Self::PyType {
        self.0
    }
}
impl ToMypy for wb::mesh::VertID {
    type PyType = usize;
    fn to_mypy(self) -> Self::PyType {
        self.0
    }
}
impl ToMypy for wb::mesh::TextureID {
    type PyType = usize;
    fn to_mypy(self) -> Self::PyType {
        self.0
    }
}

impl<T> ToMypy for Option<T> 
where T: ToMypy {
    type PyType = Option<T::PyType>;
    fn to_mypy(self) -> Self::PyType {
        self.map(|o| o.to_mypy())
    }
}
impl<T,E> ToMypy for Result<T,E> 
where T: ToMypy,
      E: ToMypy<PyType=PyErr> {
    type PyType = PyResult<T::PyType>;
    fn to_mypy(self) -> Self::PyType {
        match self {
            Ok(val)  => Ok(val.to_mypy()),
            Err(err) => Err(err.to_mypy())
        }
    }
}
impl<T> ToMypy for Vec<T> 
where T: ToMypy {
    type PyType = Vec<T::PyType>;
    fn to_mypy(self) -> Self::PyType {
        self.into_iter().map(|o| o.to_mypy()).collect()
    }
}
impl<T, const N: usize> ToMypy for [T;N]
where T: ToMypy {
    type PyType = [T::PyType; N];
    fn to_mypy(self) -> Self::PyType {
        self.map(|o| o.to_mypy())
    }
}
impl<'a, T> ToMypy for &[T]
where T: ToMypy + Clone {
    type PyType = Vec<T::PyType>;
    fn to_mypy(self) -> Self::PyType {
        self.into_iter().map(|o| {
            o.clone().to_mypy()
        }).collect()
    }
}

impl ToMypy for wb::mesh::Error {
    type PyType = PyErr;
    fn to_mypy(self) -> Self::PyType {
        PyValueError::new_err(format!("{:?}", self))
    }
}
impl ToMypy for wb::WBErrorAny {
    type PyType = PyErr;
    fn to_mypy(self) -> Self::PyType {
        PyValueError::new_err(format!("{:?}", self))
    }
}
impl ToMypy for wb::WBErrorHole {
    type PyType = PyErr;
    fn to_mypy(self) -> Self::PyType {
        PyValueError::new_err(format!("{:?}", self))
    }
}
impl ToMypy for wb::WBErrorTouching {
    type PyType = PyErr;
    fn to_mypy(self) -> Self::PyType {
        PyValueError::new_err(format!("{:?}", self))
    }
}
impl ToMypy for wb::WBErrorInvalidHoleMeta {
    type PyType = PyErr;
    fn to_mypy(self) -> Self::PyType {
        PyValueError::new_err(format!("{:?}", self))
    }
}
impl ToMypy for wb::WBErrorBuildingHoleMeta<PyErr> {
    type PyType = PyErr;
    fn to_mypy(self) -> Self::PyType {
        use wb::WBErrorBuildingHoleMeta::*;
        match self {
            Meta(err) => PyValueError::new_err(format!("{:?}", err)),
            BuilderError(err) => err
        }
    }
}