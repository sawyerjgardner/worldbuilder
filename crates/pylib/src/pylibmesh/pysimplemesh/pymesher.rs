use pyo3::prelude::*;

use super::PySimpleMeshLayer;


#[pyclass(name="SimpleMesher")]
pub struct PySimpleMesher(PySimpleMeshLayer);
#[pymethods]
impl PySimpleMesher {
    #[new]
    pub fn new(mesh: &PySimpleMeshLayer, py: Python) -> Self {
        Self(mesh.clone_ref(py))
    }
    pub fn quick_vertice(&mut self, py: Python, pos: [f32;3], uv: [f32;2], normal: [f32;3]) -> PyResult<usize> {
        self.0.push_vertice(py, pos, uv, normal)
    }
    pub fn quick_face(&mut self, py: Python, vert_ids: [usize;3]) -> PyResult<usize> {
        self.0.push_face(py, vert_ids)
    }
    pub fn valid(&self, py: Python) -> bool {
        self.0.valid(py)
    }
    /// MesherFaces
    pub fn face(&self, py: Python) -> PySimpleMesherFace {
        PySimpleMesherFace::new(&self.0, py)
    }
    /// MesherVerts
    pub fn vert(&self, py: Python) -> PySimpleMesherVert {
        PySimpleMesherVert::new(&self.0, py)
    }
}


#[pyclass(name="SimpleMesherVert")]
pub struct PySimpleMesherVert {
    layer: PySimpleMeshLayer,
    pos: [f32;3],
    uv: [f32;2],
    normal: [f32;3]
}
#[pymethods]
impl PySimpleMesherVert {
    #[new]
    pub fn new(layer: &PySimpleMeshLayer, py: Python) -> Self {
        Self {
            layer: layer.clone_ref(py),
            pos: Default::default(),
            uv: Default::default(),
            normal: Default::default()
        }
    }
    pub fn pos(mut slf: PyRefMut<Self>, pos: [f32;3]) -> PyRefMut<Self> {
        slf.pos = pos;
        slf
    }
    pub fn uv(mut slf: PyRefMut<Self>, uv: [f32;2]) -> PyRefMut<Self> {
        slf.uv = uv;
        slf
    }
    pub fn normal(mut slf: PyRefMut<Self>, normal: [f32;3]) -> PyRefMut<Self> {
        slf.normal = normal;
        slf
    }
    pub fn finish(&mut self, py: Python) -> PyResult<usize> {
        self.layer.push_vertice(py, self.pos, self.uv, self.normal)
    }
    pub fn valid(&self, py: Python) -> bool {
        self.layer.valid(py)
    }
}


#[pyclass(name="SimpleMesherFace")]
pub struct PySimpleMesherFace {
    mesh: PySimpleMeshLayer,
    verts: [usize;3]
}
#[pymethods]
impl PySimpleMesherFace {
    #[new]
    pub fn new(mesh: &PySimpleMeshLayer, py: Python) -> Self {
        Self {
            mesh: mesh.clone_ref(py),
            verts: Default::default()
        }
    }
    pub fn verts(mut slf: PyRefMut<Self>, verts: [usize;3]) -> PyRefMut<Self> {
        slf.verts = verts;
        slf
    }
    pub fn finish(&mut self, py: Python) -> PyResult<usize> {
        self.mesh.push_face(py, self.verts)
    }
    pub fn valid(&self, py: Python) -> bool {
        self.mesh.valid(py)
    }
}