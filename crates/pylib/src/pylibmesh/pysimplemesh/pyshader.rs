use pyo3::prelude::*;
use world_builder::mesh as wbm;

use crate::StaticString;
use crate::WorldBuilderPyModuleMesh;


#[pyclass(name="SimpleShader")]
pub struct PySimpleShader(pub wbm::SimpleShader<'static>);
#[pymethods]
impl PySimpleShader {
    #[new]
    pub fn new(texture: StaticString) -> Self {
        Self(wbm::SimpleShader::new(texture.0))
    }
    pub fn texture(&self) -> &'static str {
        self.0.texture()
    }
    /// Debug
    pub fn __repr__(&self) -> PyResult<String> {
        Ok(format!("{:?}", self.0))
    }
    /// Clone
    pub fn clone(&self) -> Self {
        Self(self.0.clone())
    }
    /// PrIdentifyMesh
    #[staticmethod]
    pub fn identifymesh_meshdata(py: Python) -> PyResult<Bound<PyAny>> {
        WorldBuilderPyModuleMesh::get(py)?.getattr("SimpleMesh")
    }
    /// PrShader
    pub fn shader_mesher(slf: Py<PySimpleShader>, meshdata: PrHasSimpleShaderMesher) -> PyResult<Bound<PyAny>> {
        meshdata.simpleshader_mesher(slf)
    }
}


pub struct PrHasSimpleShaderMesher<'py>(Bound<'py, PyAny>);
impl<'py> PrHasSimpleShaderMesher<'py> {
    pub const PY_SIMPLESHADER_MESHER: &'static str = "_simpleshader_mesher";

    /// return a mesher for self
    pub fn simpleshader_mesher(&self, shader: Py<PySimpleShader>) -> PyResult<Bound<'py, PyAny>> {
        self.0.call_method1(Self::PY_SIMPLESHADER_MESHER, (shader,))
    }

    pub fn is_instance(obj: &Bound<PyAny>) -> bool {
        obj.getattr(Self::PY_SIMPLESHADER_MESHER).is_ok()
    }
}
impl<'py> FromPyObject<'py> for PrHasSimpleShaderMesher<'py> {
    fn extract_bound(ob: &Bound<'py, PyAny>) -> PyResult<Self> {
        Ok(Self(ob.clone()))
    }
}
#[pyclass(name="PrHasSimpleShaderMesher")]
pub struct PrHasSimpleShaderMesherDummy();