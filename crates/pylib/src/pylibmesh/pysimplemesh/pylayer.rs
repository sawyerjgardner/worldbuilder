use pyo3::prelude::*;
use pyo3::exceptions::PyValueError;
use world_builder::mesh as wbm;

use crate::mesh::PyMesh;
use crate::StaticString;
use crate::ToMypy;
use crate::ToMypy1;
use super::PySimpleMesh;
use super::PySimpleMesher;


#[pyclass(name="SimpleMeshLayer")]
pub struct PySimpleMeshLayer {
    mesh: PySimpleMesh,
    texture: &'static str
}
#[pymethods]
impl PySimpleMeshLayer {
    #[new]
    pub fn new(py: Python, texture: StaticString) -> PyResult<Self> {
        wbm::SimpleMeshLayer::new(texture.0).to_mypy1(py)
    }
    pub fn texture(&self) -> &'static str {
        self.texture
    }
    pub fn positions(&self, py: Python) -> PyResult<Vec<[f32;3]>> {
        self.with_real_layer(py, |layer| {
            Ok(layer.positions().into())
        })
    }
    pub fn uvs(&self, py: Python) -> PyResult<Vec<[f32;2]>> {
        self.with_real_layer(py, |layer| {
            Ok(layer.uvs().into())
        })
    }
    pub fn normals(&self, py: Python) -> PyResult<Vec<[f32;3]>> {
        self.with_real_layer(py, |layer| {
            Ok(layer.normals().into())
        })
    }
    pub fn faces(&self, py: Python) -> PyResult<Vec<[usize;3]>> {
        self.with_real_layer(py, |layer| {
            Ok(layer.faces().to_mypy())
        })
    }
    pub fn push_vertice(&mut self, 
                        py: Python,
                        pos: [f32;3],
                        uv: [f32;2],
                        normal: [f32;3]) 
            -> PyResult<usize> {
        self.with_real_layer(py, |layer| {
            Ok(layer.push_vertice(pos, uv, normal).to_mypy())
        })
    }
    pub fn push_face(&mut self, py: Python<'_>, vert_ids: [usize;3]) -> PyResult<usize> {
        self.with_real_layer(py, |layer| {
            Ok(layer.push_face(vert_ids.map(|o| wbm::VertID(o))).to_mypy())
        })
    }
    pub fn mesher(&self, py: Python) -> PySimpleMesher {
        PySimpleMesher::new(self, py)
    }
    pub fn as_mesh(&self, py: Python) -> Py<PyMesh> {
        self.mesh.as_mesh(py)
    }
    pub fn valid(&self, py: Python) -> bool {
        self.with_real_layer(py, |_| Ok(())).is_ok()
    }
    /// Clone
    #[pyo3(name="clone")]
    pub fn py_clone(&self, py: Python) -> PyResult<Self> {
        self.with_real_layer(py, |layer| {
            layer.clone().to_mypy1(py)
        })
    }
    /// Debug
    pub fn __repr__(&self, py: Python) -> PyResult<String> {
        self.with_real_layer(py, |layer| {
            Ok(format!("{:?}", layer))
        })
    }
}
impl PySimpleMeshLayer {
    pub fn clone_ref(&self, py: Python) -> Self {
        Self {
            mesh: self.mesh.clone_ref(py),
            texture: self.texture
        }
    }
    pub fn from_pymesh(mesh: Py<PyMesh>, texture: &'static str) -> Self {
        Self {mesh: PySimpleMesh(mesh), texture}
    }
    pub fn with_real_layer<F, R>(&self, py: Python, func: F) -> PyResult<R>
    where F: FnOnce(&mut wbm::SimpleMeshLayer<'static>) -> PyResult<R> {
        self.mesh.with_real_mesh(py, |mesh| {
            let Some(layer) = mesh.get_mut(&self.texture) else {
                return Err(PyValueError::new_err("TODO: missing layer"))
            };
            func(layer)
        })
    }
}
impl ToMypy1 for wbm::SimpleMeshLayer<'static> {
    type PyType = PyResult<PySimpleMeshLayer>;    
    fn to_mypy1(self, py: Python) -> Self::PyType {
        let texture = self.texture();
        let mut simplemesh = wbm::SimpleMesh::new();
        simplemesh.get_or_default(texture);
        Ok(PySimpleMeshLayer {mesh: simplemesh.to_mypy1(py)?, texture})
    }
}