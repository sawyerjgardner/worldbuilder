use pyo3::prelude::*;
use pyo3::exceptions::PyValueError;
use pyo3::types::PyType;
use world_builder::mesh as wbm;

use crate::StaticString;
use crate::ToMypy;
use crate::ToMypy1;
use super::PyDebugShader;
use super::PyMesh;

mod pylayer;
mod pymesher;
mod pyshader;
pub use pylayer::*;
pub use pymesher::*;
pub use pyshader::*;


#[pyclass(name="SimpleMesh")]
pub struct PySimpleMesh(Py<PyMesh>);
#[pymethods]
impl PySimpleMesh {
    #[new]
    pub fn new(py: Python) -> PyResult<Self> {
        wbm::SimpleMesh::new().to_mypy1(py)
    }
    pub fn get(&mut self, texture: &str, py: Python) -> PyResult<Option<PySimpleMeshLayer>> {
        self.with_real_mesh(py, |simplemesh| {
            Ok(simplemesh.get(texture).map(|_layer| {
                PySimpleMeshLayer::from_pymesh(
                    self.0.clone_ref(py), 
                    StaticString::get(texture)
                )
            })) 
        })
    }
    pub fn get_or_default(&mut self, texture: StaticString, py: Python) -> PyResult<PySimpleMeshLayer> {
        self.with_real_mesh(py, |simplemesh| {
            simplemesh.get_or_default(texture.0);
            Ok(PySimpleMeshLayer::from_pymesh(self.0.clone_ref(py), texture.0))
        })
    }
    pub fn index(&self, texture: &str, py: Python) -> PyResult<Option<usize>> {
        self.with_real_mesh(py, |simplemesh| {
            Ok(simplemesh.index(texture))
        })
    }
    pub fn mesher(&mut self, texture: StaticString, py: Python) -> PyResult<PySimpleMesher> {
        Ok(self.get_or_default(texture, py)?.mesher(py))
    }
    pub fn valid(&self, py: Python) -> bool {
        self.with_real_mesh(py, |_| Ok(())).is_ok()
    }
    /// Clone
    #[pyo3(name="clone")]
    pub fn py_clone(&self, py: Python) -> PyResult<Self> {
        self.with_real_mesh(py, |simplemesh| {
            simplemesh.clone().to_mypy1(py)
        })
    }
    /// Default
    #[staticmethod]
    pub fn default(py: Python) -> PyResult<Self> {
        Ok(Self(Py::new(py, PyMesh(wbm::SimpleMesh::default().into()))?))
    }
    /// Debug
    pub fn __repr__(&self) -> String {
        format!("{:?}", self.0)
    }
    /// PrIdentifyMesh
    #[classmethod]
    pub fn identifymesh_meshdata(cls: Bound<PyType>) -> Bound<PyType> {
        cls
    }
    /// PrMeshDataType
    #[staticmethod]
    pub fn _meshdata_remove<'py>(mesh: Bound<'py, PyMesh>) -> PyResult<Option<Self>> {
        let Some(meshdata) = mesh.borrow_mut().0.remove::<wbm::SimpleMesh>() else {
            return Ok(None)
        };
        meshdata.to_mypy1(mesh.py()).map(|o| Some(o))
    }
    #[staticmethod]
    pub fn _meshdata_get<'py>(mesh: Bound<'py, PyMesh>) -> Option<Self> {
        mesh.borrow().0.get::<wbm::SimpleMesh>().map(|_| {
            Self(mesh.unbind())
        })
    }
    #[staticmethod]
    pub fn _meshdata_get_or_default<'py>(mesh: Bound<'py, PyMesh>) -> PyResult<Self> {
        match mesh.borrow_mut().0.get_or_default::<wbm::SimpleMesh>() {
            Ok(_)    => Ok(Self(mesh.unbind())),
            Err(err) => Err(err.to_mypy())
        }
    }
    /// PrMeshData
    pub fn _meshdata_try_insert(&self, mesh: Bound<'_, PyMesh>) -> PyResult<Option<Self>> {
        if mesh.borrow_mut().0.get::<wbm::SimpleMesh>().is_some() {
            return Ok(None)
        };
        self._meshdata_insert_force(mesh).map(|o| Some(o))
    }
    pub fn _meshdata_insert_force(&self, mesh: Bound<'_, PyMesh>) -> PyResult<Self> {
        let Some(meshdata) = self.0.borrow_mut(mesh.py()).0.remove::<wbm::SimpleMesh>() else {
            return Err(PyValueError::new_err("TODO: missing simplemesh"))
        };
        mesh.borrow_mut().0.insert_force(meshdata);
        Ok(Self(mesh.unbind()))
    }
    pub fn as_mesh(&self, py: Python) -> Py<PyMesh> {
        self.0.clone_ref(py)
    }
    /// PrSimpleShaderMesher
    pub fn _simpleshader_mesher(&mut self, shader: &Bound<PySimpleShader>) -> PyResult<PySimpleMesher> {
        self.mesher(shader.borrow().0.texture().into(), shader.py()) 
    }
    /// PrDebugShaderMesher
    pub fn _debugshader_mesher(&mut self, shader: &Bound<PyDebugShader>) -> PyResult<PySimpleMesher> {
        self.mesher(shader.borrow().0.texture().into(), shader.py()) 
    }
}
impl PySimpleMesh {
    pub fn clone_ref(&self, py: Python) -> Self {
        Self(self.0.clone_ref(py))
    }
    pub fn with_real_mesh<F, R>(&self, py: Python, func: F) -> PyResult<R>
    where F: FnOnce(&mut wbm::SimpleMesh<'static>) -> PyResult<R> {
        let mesh = &mut self.0.borrow_mut(py).0;
        let Some(simple_mesh) = mesh.get_mut::<wbm::SimpleMesh>() else {
            return Err(PyValueError::new_err("TODO: missing simplemesh"))
        };
        func(simple_mesh)
    }
}
impl ToMypy1 for wbm::SimpleMesh<'static> {
    type PyType = PyResult<PySimpleMesh>;    
    fn to_mypy1(self, py: Python) -> Self::PyType {
        let mesh = wbm::Mesh::from(self);
        Ok(PySimpleMesh(Py::new(py, mesh.to_mypy())?))
    }
}