use pyo3::prelude::*;
use world_builder::mesh as wbm;
use crate::ToMypy;
use super::PrMeshData;
use super::PrIdentifyMesh;
use super::PrShader;


#[pyclass(name="Mesh")]
pub struct PyMesh(pub wbm::Mesh<'static>);
#[pymethods]
impl PyMesh {
    #[new]
    pub fn new() -> Self {
        PyMesh(wbm::Mesh::new())
    }
    #[staticmethod]
    pub fn from_as(meshdata: PrMeshData) -> PyResult<Bound<Self>> {
        meshdata.as_mesh()?.extract()
    }
    pub fn len(&self) -> usize {
        self.0.len()
    }
    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }
    pub fn remove<'py>(slf: Py<Self>, mesh_type: PrIdentifyMesh<'py>) -> PyResult<Bound<'py,PyAny>> {
        mesh_type.meshdata1()?.mesh_remove(slf)
    }
    pub fn try_insert<'py>(slf: Py<Self>, meshdata: PrMeshData<'py>) -> PyResult<Bound<'py,PyAny>> {
        meshdata.mesh_try_insert(slf)
    }
    pub fn insert_force<'py>(slf: Py<Self>, meshdata: PrMeshData<'py>) -> PyResult<Bound<'py,PyAny>> {
        meshdata.mesh_insert_force(slf)
    }
    pub fn get<'py>(slf: Py<Self>, mesh_type: PrIdentifyMesh<'py>) -> PyResult<Bound<'py,PyAny>> {
        mesh_type.meshdata1()?.mesh_get(slf)
    }
    pub fn get_or_insert<'py>(slf: Py<Self>, mesh_type: PrIdentifyMesh<'py>) -> PyResult<Bound<'py,PyAny>> {
        mesh_type.meshdata1()?.mesh_get_or_default(slf)
    }
    pub fn mesher(slf: Py<Self>) -> PyBaseMesher {
        PyBaseMesher(slf) 
    }
    /// Debug
    pub fn __repr__(&self) -> String {
        format!("{:?}", self.0)
    }
}
impl ToMypy for wbm::Mesh<'static> {
    type PyType = PyMesh;    
    fn to_mypy(self) -> Self::PyType {
        PyMesh(self)
    }
}


#[pyclass(name="BaseMesher")]
pub struct PyBaseMesher(Py<PyMesh>);
#[pymethods]
impl PyBaseMesher {
    #[new]
    pub fn new(mesh: Py<PyMesh>) -> Self {
        Self(mesh)
    }
    pub fn shader<'py>(&mut self, shader: PrShader<'py>) -> PyResult<Bound<'py,PyAny>> {
        let mesh_type = shader.0.extract::<PrIdentifyMesh>()?;
        self.shader_for(shader, mesh_type)
    }
    pub fn shader_for<'py>(&mut self, shader: PrShader<'py>, mesh_type: PrIdentifyMesh<'py>) -> PyResult<Bound<'py,PyAny>> {
        let mesh = self.0.clone_ref(shader.0.py());
        let meshdata = PyMesh::get_or_insert(mesh, mesh_type)?;
        shader.mesher(meshdata.unbind())
    }
}
impl PyBaseMesher {
    pub fn with_real_mesher<F, R>(&self, py: Python, func: F) -> R
    where F: FnOnce(wbm::BaseMesher<'_,'static>) -> R {
        let mesh = &mut self.0.borrow_mut(py).0;
        func(mesh.mesher())
    }
}