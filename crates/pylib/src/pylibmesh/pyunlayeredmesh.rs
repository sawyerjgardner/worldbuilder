use pyo3::prelude::*;
use pyo3::exceptions::PyValueError;
use pyo3::types::PyType;
use world_builder::mesh as wbm;

use super::PyMesh;
use super::PySimpleShader;
use super::PyDebugShader;
use crate::StaticString;
use crate::ToMypy;
use crate::ToMypy1;


mod pymesher;
mod pymeshertextured;
pub use pymesher::*;
pub use pymeshertextured::*;


#[pyclass(name="UnlayeredMesh")]
pub struct PyUnlayeredMesh(Py<PyMesh>);
#[pymethods]
impl PyUnlayeredMesh {
    #[new]
    pub fn new(py: Python) -> PyResult<Self> {
        wbm::UnlayeredMesh::new().to_mypy1(py)
    }
    pub fn textures(&self, py: Python) -> PyResult<Vec<&'static str>> {
        self.with_real_mesh(py, |unlayered_mesh| {
            Ok(unlayered_mesh.textures().into())
        })
    }
    pub fn positions(&self, py: Python) -> PyResult<Vec<[f32;3]>> {
        self.with_real_mesh(py, |unlayered_mesh| {
            Ok(unlayered_mesh.positions().into())
        })
    }
    pub fn uvs(&self, py: Python) -> PyResult<Vec<[f32;2]>> {
        self.with_real_mesh(py, |unlayered_mesh| {
            Ok(unlayered_mesh.uvs().into())
        })
    }
    pub fn normals(&self, py: Python) -> PyResult<Vec<[f32;3]>> {
        self.with_real_mesh(py, |unlayered_mesh| {
            Ok(unlayered_mesh.normals().into())
        })
    }
    pub fn face_verts(&self, py: Python) -> PyResult<Vec<[usize;3]>> {
        self.with_real_mesh(py, |unlayered_mesh| {
            Ok(unlayered_mesh.face_verts().to_mypy())
        })
    }
    pub fn face_textures(&self, py: Python) -> PyResult<Vec<usize>> {
        self.with_real_mesh(py, |unlayered_mesh| {
            Ok(unlayered_mesh.face_textures().to_mypy())
        })
    }
    pub fn texture_id(&self, py: Python, texture: &str) -> PyResult<Option<usize>> {
        self.with_real_mesh(py, |unlayered_mesh| {
            Ok(unlayered_mesh.texture_id(texture).to_mypy())
        })
    }
    pub fn texture_id_or_insert(&self, py: Python, texture: StaticString) -> PyResult<usize> {
        self.with_real_mesh(py, |unlayered_mesh| {
            Ok(unlayered_mesh.texture_id_or_insert(texture.0).to_mypy())
        })
    }
    pub fn push_texture(&self, py: Python, texture: StaticString) -> PyResult<usize> {
        self.with_real_mesh(py, |unlayered_mesh| {
            Ok(unlayered_mesh.push_texture(texture.0).to_mypy())
        })
    }
    pub fn push_vertice(&self, py: Python, pos: [f32;3], uv: [f32;2], normal: [f32;3]) -> PyResult<usize> {
        self.with_real_mesh(py, |unlayered_mesh| {
            Ok(unlayered_mesh.push_vertice(pos,uv,normal).to_mypy())
        })
    }
    pub fn push_face(&self, py: Python, vert_ids: [usize;3], texture_id: usize) -> PyResult<usize> {
        self.with_real_mesh(py, |unlayered_mesh| {
            Ok(unlayered_mesh.push_face(
                vert_ids.map(|o| wbm::VertID(o)), 
                wbm::TextureID(texture_id)
            ).to_mypy())
        })
    }
    pub fn mesher(&self, py: Python) -> PyUnlayeredMesher {
        PyUnlayeredMesher::new(py, &self)
    }
    pub fn valid(&self, py: Python) -> bool {
        self.with_real_mesh(py, |_| Ok(())).is_ok()
    }
    /// Clone
    #[pyo3(name="clone")]
    pub fn py_clone(&self, py: Python) -> PyResult<Self> {
        self.with_real_mesh(py, |unlayered_mesh| {
            unlayered_mesh.clone().to_mypy1(py)
        })
    }
    /// Default
    #[staticmethod]
    pub fn default(py: Python) -> PyResult<Self> {
        Ok(Self(Py::new(py, PyMesh(wbm::UnlayeredMesh::default().into()))?))
    }
    /// Debug
    pub fn __repr__(&self) -> String {
        format!("{:?}", self.0)
    }
    /// PrIdentifyMesh
    #[classmethod]
    pub fn identifymesh_meshdata(cls: Bound<PyType>) -> Bound<PyType> {
        cls
    }
    /// PrMeshDataType
    #[staticmethod]
    pub fn _meshdata_remove<'py>(mesh: Bound<'py, PyMesh>) -> PyResult<Option<Self>> {
        let Some(meshdata) = mesh.borrow_mut().0.remove::<wbm::UnlayeredMesh>() else {
            return Ok(None)
        };
        meshdata.to_mypy1(mesh.py()).map(|o| Some(o))
    }
    #[staticmethod]
    pub fn _meshdata_get<'py>(mesh: Bound<'py, PyMesh>) -> Option<Self> {
        mesh.borrow().0.get::<wbm::UnlayeredMesh>().map(|_| {
            Self(mesh.unbind())
        })
    }
    #[staticmethod]
    pub fn _meshdata_get_or_default<'py>(mesh: Bound<'py, PyMesh>) -> PyResult<Self> {
        match mesh.borrow_mut().0.get_or_default::<wbm::UnlayeredMesh>() {
            Ok(_)    => Ok(Self(mesh.unbind())),
            Err(err) => Err(err.to_mypy())
        }
    }
    /// PrMeshData
    pub fn _meshdata_try_insert(&self, mesh: Bound<'_, PyMesh>) -> PyResult<Option<Self>> {
        if mesh.borrow_mut().0.get::<wbm::UnlayeredMesh>().is_some() {
            return Ok(None)
        };
        self._meshdata_insert_force(mesh).map(|o| Some(o))
    }
    pub fn _meshdata_insert_force(&self, mesh: Bound<'_, PyMesh>) -> PyResult<Self> {
        let Some(meshdata) = self.0.borrow_mut(mesh.py()).0.remove::<wbm::UnlayeredMesh>() else {
            return Err(PyValueError::new_err("TODO: missing unlayeredmesh"))
        };
        mesh.borrow_mut().0.insert_force(meshdata);
        Ok(Self(mesh.unbind()))
    }
    pub fn as_mesh(&self, py: Python) -> Py<PyMesh> {
        self.0.clone_ref(py)
    }
    /// PrSimpleShaderMesher
    pub fn _simpleshader_mesher(&mut self, shader: &Bound<PySimpleShader>) -> PyResult<PyUnlayeredMesherTextured> {
        let py = shader.py();
        let texture = self.texture_id_or_insert(py, shader.borrow().0.texture().into())?;
        Ok(self.mesher(py).textured(py, texture))
    }
    /// PrDebugShaderMesher
    pub fn _debugshader_mesher(&mut self, shader: &Bound<PyDebugShader>) -> PyResult<PyUnlayeredMesherTextured> {
        let py = shader.py();
        let texture = self.texture_id_or_insert(py, shader.borrow().0.texture().into())?;
        Ok(self.mesher(py).textured(py, texture))
    }
}
impl PyUnlayeredMesh {
    pub fn clone_ref(&self, py: Python) -> Self {
        Self(self.0.clone_ref(py))
    }
    pub fn with_real_mesh<F, R>(&self, py: Python, func: F) -> PyResult<R>
    where F: FnOnce(&mut wbm::UnlayeredMesh<'static>) -> PyResult<R> {
        let mesh = &mut self.0.borrow_mut(py).0;
        let Some(unlayered_mesh) = mesh.get_mut::<wbm::UnlayeredMesh>() else {
            return Err(PyValueError::new_err("TODO: missing unlayeredmesh"))
        };
        func(unlayered_mesh)
    }
}
impl ToMypy1 for wbm::UnlayeredMesh<'static> {
    type PyType = PyResult<PyUnlayeredMesh>;    
    fn to_mypy1(self, py: Python) -> Self::PyType {
        let mesh = wbm::Mesh::from(self);
        Ok(PyUnlayeredMesh(Py::new(py, mesh.to_mypy())?))
    }
}