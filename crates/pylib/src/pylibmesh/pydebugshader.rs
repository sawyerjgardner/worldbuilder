use pyo3::prelude::*;
use world_builder::mesh as wbm;

use super::WorldBuilderPyModuleMesh;


#[pyclass(name="DebugShader")]
pub struct PyDebugShader(pub wbm::DebugShader);
#[pymethods]
impl PyDebugShader {
    #[staticmethod]
    #[pyo3(name="RED")]
    pub fn red() -> Self {
        Self(wbm::DebugShader::RED) 
    }
    pub fn texture(&self) -> &'static str {
        self.0.texture()
    }
    #[getter]
    pub fn color(&self) -> [u8;3] {
        self.0.color
    }
    #[setter]
    pub fn set_color(&mut self, val: [u8;3]) {
        self.0.color = val;
    }
    #[getter]
    pub fn alpha(&self) -> u8 {
        self.0.alpha
    }
    #[setter]
    pub fn set_alpha(&mut self, val: u8) {
        self.0.alpha = val;
    }
    /// Clone
    #[pyo3(name="clone")]
    pub fn py_clone(&self) -> Self {
        Self(self.0.clone())
    }
    /// Debug
    pub fn __repr__(&self) -> String {
        format!("{:?}", self.0)
    }
    /// PrIdentifyMesh
    #[staticmethod]
    pub fn identifymesh_meshdata(py: Python) -> PyResult<Bound<PyAny>> {
        WorldBuilderPyModuleMesh::get(py)?.getattr("SimpleMesh")
    }
    /// PrShader
    pub fn shader_mesher(slf: Py<PyDebugShader>, meshdata: PrHasDebugShaderMesher) -> PyResult<Bound<PyAny>> {
        meshdata.debugshader_mesher(slf)
    }
}


pub struct PrHasDebugShaderMesher<'py>(Bound<'py, PyAny>);
impl<'py> PrHasDebugShaderMesher<'py> {
    pub const PY_DEBUGSHADER_MESHER: &'static str = "_debugshader_mesher";

    /// return a mesher for self
    pub fn debugshader_mesher(&self, shader: Py<PyDebugShader>) -> PyResult<Bound<'py, PyAny>> {
        self.0.call_method1(Self::PY_DEBUGSHADER_MESHER, (shader,))
    }

    pub fn is_instance(obj: &Bound<PyAny>) -> bool {
        obj.getattr(Self::PY_DEBUGSHADER_MESHER).is_ok()
    }
}
impl<'py> FromPyObject<'py> for PrHasDebugShaderMesher<'py> {
    fn extract_bound(ob: &Bound<'py, PyAny>) -> PyResult<Self> {
        Ok(Self(ob.clone()))
    }
}
#[pyclass(name="PrHasDebugShaderMesher")]
pub struct PrHasDebugShaderMesherDummy();