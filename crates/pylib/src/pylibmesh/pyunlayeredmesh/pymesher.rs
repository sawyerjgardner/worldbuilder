use pyo3::prelude::*;

use crate::StaticString;
use super::PyUnlayeredMesh;
use super::PyUnlayeredMesherTextured;

#[pyclass(name="UnlayeredMesher")]
pub struct PyUnlayeredMesher(PyUnlayeredMesh);
#[pymethods]
impl PyUnlayeredMesher {
    #[new]
    pub fn new(py: Python, mesh: &PyUnlayeredMesh) -> Self {
        Self(mesh.clone_ref(py)) 
    }
    pub fn textured(&self, py: Python, texture_id: usize) -> PyUnlayeredMesherTextured {
        PyUnlayeredMesherTextured::new(py, &self.0, texture_id)
    }
    pub fn quick_texture(&self, py: Python, texture: StaticString) -> PyResult<usize> {
        self.0.with_real_mesh(py, |unlayered_mesh| {
            Ok(unlayered_mesh.push_texture(texture.0).0)
        })
    }
    pub fn quick_vertice(&self, py: Python, pos: [f32;3], uv: [f32;2], normal: [f32;3]) -> PyResult<usize> {
        self.0.push_vertice(py, pos, uv, normal)
    }
    pub fn quick_face(&self, py: Python, vert_ids: [usize;3], texture_id: usize) -> PyResult<usize> {
        self.0.push_face(py, vert_ids, texture_id)
    }
    pub fn valid(&self, py: Python) -> bool {
        self.0.valid(py)
    }
    /// MesherFaces
    pub fn face(&self, py: Python) -> PyUnlayeredMesherFace {
        PyUnlayeredMesherFace::new(&self.0, py)
    }
    /// MesherVerts
    pub fn vert(&self, py: Python) -> PyUnlayeredMesherVert {
        PyUnlayeredMesherVert::new(&self.0, py)
    }
}


#[pyclass(name="UnlayeredMesherVert")]
pub struct PyUnlayeredMesherVert {
    mesh: PyUnlayeredMesh,
    pos: [f32;3],
    uv: [f32;2],
    normal: [f32;3]
}
#[pymethods]
impl PyUnlayeredMesherVert  {
    #[new]
    pub fn new(mesh: &PyUnlayeredMesh, py: Python) -> Self {
        Self {
            mesh: mesh.clone_ref(py),
            pos: Default::default(),
            uv: Default::default(),
            normal: Default::default()
        }
    }
    pub fn pos(mut slf: PyRefMut<Self>, pos: [f32;3]) -> PyRefMut<Self> {
        slf.pos = pos;
        slf
    }
    pub fn uv(mut slf: PyRefMut<Self>, uv: [f32;2]) -> PyRefMut<Self> {
        slf.uv = uv;
        slf
    }
    pub fn normal(mut slf: PyRefMut<Self>, normal: [f32;3]) -> PyRefMut<Self> {
        slf.normal = normal;
        slf
    }
    pub fn finish(&mut self, py: Python) -> PyResult<usize> {
        self.mesh.push_vertice(py, self.pos, self.uv, self.normal)
    }
    pub fn valid(&self, py: Python) -> bool {
        self.mesh.valid(py)
    }
}


#[pyclass(name="UnlayeredMesherFace")]
pub struct PyUnlayeredMesherFace {
    mesh: PyUnlayeredMesh,
    verts: [usize;3],
    pub(super) texture: usize
}
#[pymethods]
impl PyUnlayeredMesherFace {
    #[new]
    pub fn new(mesh: &PyUnlayeredMesh, py: Python) -> Self {
        Self {
            mesh: mesh.clone_ref(py),
            verts: Default::default(),
            texture: Default::default()
        }
    }
    pub fn verts(mut slf: PyRefMut<Self>, verts: [usize;3]) -> PyRefMut<Self> {
        slf.verts = verts;
        slf
    }
    pub fn texture(mut slf: PyRefMut<Self>, texture: usize) -> PyRefMut<Self> {
        slf.texture = texture;
        slf
    }
    pub fn finish(&mut self, py: Python) -> PyResult<usize> {
        self.mesh.push_face(py, self.verts, self.texture)
    }
    pub fn valid(&self, py: Python) -> bool {
        self.mesh.valid(py)
    }
}