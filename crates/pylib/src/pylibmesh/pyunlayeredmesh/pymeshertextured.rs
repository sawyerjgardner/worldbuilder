use pyo3::prelude::*;

use super::PyUnlayeredMesh;
use super::PyUnlayeredMesher;
use super::PyUnlayeredMesherFace;
use super::PyUnlayeredMesherVert;


#[pyclass(name="UnlayeredMesherTextured")]
pub struct PyUnlayeredMesherTextured{
    mesher: PyUnlayeredMesher,
    texture_id: usize
}
#[pymethods]
impl PyUnlayeredMesherTextured {
    #[new]
    pub fn new(py: Python, mesh: &PyUnlayeredMesh, texture_id: usize) -> Self {
        Self {
            mesher: PyUnlayeredMesher::new(py, mesh), 
            texture_id
        }
    }
    pub fn quick_vertice(&self, py: Python, pos: [f32;3], uv: [f32;2], normal: [f32;3]) -> PyResult<usize> {
        self.mesher.quick_vertice(py, pos, uv, normal)
    }
    pub fn quick_face(&self, py: Python, vert_ids: [usize;3]) -> PyResult<usize> {
        self.mesher.quick_face(py, vert_ids, self.texture_id)
    }
    pub fn valid(&self, py: Python) -> bool {
        self.mesher.valid(py)
    }
    /// MesherFaces
    pub fn face(&self, py: Python) -> PyUnlayeredMesherFace {
        let mut face = self.mesher.face(py);
        face.texture = self.texture_id;
        face
    }
    /// MesherVerts
    pub fn vert(&self, py: Python) -> PyUnlayeredMesherVert {
        self.mesher.vert(py)
    }
}