use pyo3::prelude::*;
use super::PyMesh;


// also implement PyIdentifyMesh:
pub struct PrMeshDataType<'py>(pub Bound<'py, PyAny>);
impl<'py> PrMeshDataType<'py> {
    pub const PY_MESH_REMOVE:         &'static str = "_meshdata_remove";
    pub const PY_MESH_GET:            &'static str = "_meshdata_get";
    pub const PY_MESH_GET_OR_DEFAULT: &'static str = "_meshdata_get_or_default";

    /// returns removed meshdata
    pub fn mesh_remove(&self, mesh: Py<PyMesh>) -> PyResult<Bound<'py, PyAny>> {
        self.0.call_method1(Self::PY_MESH_REMOVE, (mesh,))
    }

    /// returns meshdata in `mesh` or None
    pub fn mesh_get(&self, mesh: Py<PyMesh>) -> PyResult<Bound<'py, PyAny>> {
        self.0.call_method1(Self::PY_MESH_GET, (mesh,))
    }

    /// returns meshdata in `mesh`
    pub fn mesh_get_or_default(&self, mesh: Py<PyMesh>) -> PyResult<Bound<'py, PyAny>> {
        self.0.call_method1(Self::PY_MESH_GET_OR_DEFAULT, (mesh,))
    }

    pub fn is_instance(obj: &Bound<PyAny>) -> bool {
        PrIdentifyMesh::is_instance(obj) &&
            obj.getattr(Self::PY_MESH_REMOVE).is_ok() &&
            obj.getattr(Self::PY_MESH_GET).is_ok() &&
            obj.getattr(Self::PY_MESH_GET_OR_DEFAULT).is_ok()
    }
}
impl<'py> FromPyObject<'py> for PrMeshDataType<'py> {
    fn extract_bound(ob: &Bound<'py, PyAny>) -> PyResult<Self> {
        Ok(Self(ob.clone()))
    }
}
#[pyclass(name="PrMeshDataType")]
pub struct PrMeshDataTypeDummy();


pub struct PrMeshData<'py>(pub Bound<'py, PyAny>);
impl<'py> PrMeshData<'py> { 
    pub const PY_MESH_TRY_INSERT:     &'static str = "_meshdata_try_insert";
    pub const PY_MESH_INSERT_FORCE:   &'static str = "_meshdata_insert_force";
    pub const PY_AS_MESH:             &'static str = "as_mesh";

    /// returns meshdata as moved into `mesh`
    /// if `mesh` already contains an instance of `self`, return `None`
    pub fn mesh_try_insert(&self, mesh: Py<PyMesh>) -> PyResult<Bound<'py, PyAny>> {
        self.0.call_method1(Self::PY_MESH_TRY_INSERT, (mesh,))
    }

    /// returns meshdata moved into `mesh`
    /// if `mesh` already contains an instance of `self`, replace it
    pub fn mesh_insert_force(&self, mesh: Py<PyMesh>) -> PyResult<Bound<'py, PyAny>> {
        self.0.call_method1(Self::PY_MESH_INSERT_FORCE, (mesh,))
    }

    /// returns the mesh that contains `self`
    pub fn as_mesh(&self) -> PyResult<Bound<'py,PyAny>> {
        self.0.call_method0(Self::PY_AS_MESH)
    }

    pub fn is_instance(obj: &Bound<PyAny>) -> bool {
        obj.getattr(Self::PY_MESH_TRY_INSERT).is_ok() &&
            obj.getattr(Self::PY_MESH_INSERT_FORCE).is_ok() &&
            obj.getattr(Self::PY_AS_MESH).is_ok()
    }
}
impl<'py> FromPyObject<'py> for PrMeshData<'py> {
    fn extract_bound(ob: &Bound<'py, PyAny>) -> PyResult<Self> {
        Ok(Self(ob.clone()))
    }
}
#[pyclass(name="PrMeshData")]
pub struct PrMeshDataDummy();


pub struct PrIdentifyMesh<'py>(pub Bound<'py, PyAny>); 
impl<'py> PrIdentifyMesh<'py> {
    pub const PY_MESHDATA: &'static str = "identifymesh_meshdata";

    /// returns Meshdata type
    pub fn meshdata(&self) -> PyResult<Bound<'py, PyAny>> {
        self.0.call_method0(Self::PY_MESHDATA)
    }

    pub fn meshdata1(&self) -> PyResult<PrMeshDataType<'py>> {
        self.meshdata()?.extract()
    }

    pub fn is_instance(obj: &Bound<PyAny>) -> bool {
        obj.getattr(Self::PY_MESHDATA).is_ok()
    }
}
impl<'py> FromPyObject<'py> for PrIdentifyMesh<'py> {
    fn extract_bound(ob: &Bound<'py, PyAny>) -> PyResult<Self> {
        Ok(Self(ob.clone()))
    }
}
#[pyclass(name="PrIdentifyMesh")]
pub struct PrIdentifyMeshDummy();


pub struct PrShader<'py>(pub Bound<'py, PyAny>); 
impl<'py> PrShader<'py> {
    pub const PY_MESHER: &'static str = "shader_mesher";

    /// returns shader mesher
    pub fn mesher(&self, meshdata: PyObject) -> PyResult<Bound<'py, PyAny>> {
        self.0.call_method1(Self::PY_MESHER, (meshdata,))
    }

    pub fn is_instance(obj: &Bound<PyAny>) -> bool {
        obj.getattr(Self::PY_MESHER).is_ok()
    }
}
impl<'py> FromPyObject<'py> for PrShader<'py> {
    fn extract_bound(ob: &Bound<'py, PyAny>) -> PyResult<Self> {
        Ok(Self(ob.clone()))
    }
}
#[pyclass(name="PrShader")]
pub struct PrShaderDummy();

#[pyclass(name="PrShaderAndIdentifyMesh")]
pub struct PrShaderAndIdentifyMeshDummy();
