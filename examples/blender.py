#!/usr/bin/env python3
from collections.abc import Sequence
import bpy
import numpy as np
import bpy.types as blender

import WorldBuilder as wb


class BlenderOBJData:
    pass



def get_blender_material(name: str) -> blender.Material:
    materials = bpy.data.materials
    if name not in materials.keys():
        return materials.new(name) #type: ignore
    return materials[name]



class LogFile:
    def __init__(self, text: blender.Text):
        self.text = text

    @staticmethod
    def from_name(name: str) -> 'LogFile':
        text = bpy.data.texts.get(name)
        if text is None:
            text = bpy.data.texts.new(name) #type: ignore
        return LogFile(text)

    def clear(self):
        self.text.clear()

    def write(self, text):
        self.text.write('{}\n'.format(text))


class BlenderMesh(BlenderOBJData):
    def __init__(self, mesh: blender.Mesh):
        self.mesh = mesh

    @staticmethod
    def from_name(name: str) -> 'BlenderMesh':
        mesh = bpy.data.meshes.get(name)
        if mesh is None:
            mesh = bpy.data.meshes.new(name) #type: ignore
        return BlenderMesh(mesh)

    @staticmethod
    def quick_new() -> 'BlenderMesh':
        global MESH_NAME_INDEX
        MESH_NAME_INDEX += 1
        return BlenderMesh.from_name('quick mesh #{}'.format(MESH_NAME_INDEX))

    def material_id(self, shader: str) -> int:
        id = self.mesh.materials.find(shader)
        if id == -1:
            id = len(self.mesh.materials)
            self.mesh.materials.append(get_blender_material(shader))
        return id

    def uv_layer(self, name = "UVMap") -> blender.MeshUVLoopLayer:
        layer = self.mesh.uv_layers.get(name)
        if layer is None:
            layer = self.mesh.uv_layers.new(name=name) #type: ignore
        return layer

    def clear(self):
        self.mesh.clear_geometry()
        self.mesh.materials.clear() #type: ignore

    def write_polygons(self, mesh_data: wb.SimpleMesh):
        positions: list[list[float]] = []
        faces: list[list[int]] = []
        face_offset = 0
        for layer in range(0,mesh_data.layers()):
            faces += [
                [x + face_offset for x in face]
                for face in mesh_data.faces(layer)
            ]
            poses = mesh_data.positions(layer)
            face_offset += len(poses)
            positions += poses
        self.mesh.from_pydata(positions, [], faces,)
        self.mesh.validate()

    def write_materials(self, mesh_data: wb.SimpleMesh):
        for polygon, material_index in zip(
                self.mesh.polygons,
                [
                    material
                    for material in [
                            self.material_id(mesh_data.texture(layer))
                            for layer in range(0,mesh_data.layers())
                    ]
                    for _ in mesh_data.faces(material)
                ]):
            polygon.material_index = material_index
        self.mesh.validate()

    def write_uvs(self, mesh_data: wb.SimpleMesh):
        uvs: list[list[float]] = []
        faces: list[list[int]] = []
        face_offset = 0
        for layer in range(0,mesh_data.layers()):
            faces += [
                [x + face_offset for x in face]
                for face in mesh_data.faces(layer)
            ]
            layer_uvs = mesh_data.uvs(layer)
            face_offset += len(layer_uvs)
            uvs += layer_uvs

        uv_layer = self.uv_layer()
        for polygon, face in zip(self.mesh.polygons, faces):
            for loop, vert_idx in zip(polygon.loop_indices, face):
                uv_layer.data[loop].uv = uvs[vert_idx]
        self.mesh.validate()

    def write(self, mesh_data: wb.SimpleMesh):
        self.clear()
        self.write_polygons(mesh_data)
        self.write_materials(mesh_data)
        self.write_uvs(mesh_data)

    def read(self) -> wb.SimpleMesh:
        mesh = wb.SimpleMesh()
        materials = [m.name for m in self.mesh.materials]
        for m in materials:
            mesh.push_layer(m)
        positions: list[list[float]] = [
            [*v.co.xyz,] #type:ignore
            for v in self.mesh.vertices
        ]

        uv_data = self.mesh.uv_layers[0].data
        layers = []

        for layer, material in enumerate(materials):
            faces_verts = [
                [
                    (
                        positions[vert_idx],
                        list(uv_data[loop_idx].uv) #type: ignore
                    )
                    for vert_idx, loop_idx in zip(p.vertices, p.loop_indices) #type: ignore
                ]
                for p in self.mesh.polygons
                if p.material_index == layer
            ]
            verts = [
                vert
                for face in faces_verts
                for vert in face
            ]
            for vert in verts:
                mesh.push_vertice(
                    layer,
                    vert[0],
                    vert[1],
                    [0.0, 0.0, 0.0]
                )
            faces = [
                [
                    verts.index(v)
                    for v in face
                ]
                for face in faces_verts
            ]
            for face in faces:
                pivit = face[0]
                for a, b in zip(face[1:], face[2:]):
                    mesh.push_face(layer, [pivit, a, b])
        return mesh


class BlenderCurve(BlenderOBJData):
    def __init__(self, curve: blender.Curve):
        self.curve = curve

    @staticmethod
    def from_name(name: str) -> 'BlenderCurve':
        curve = bpy.data.curves.get(name)
        if curve is None:
            curve = bpy.data.curves.new(name, type='CURVE') #type: ignore
        return BlenderCurve(curve)

    @staticmethod
    def quick_new() -> 'BlenderCurve':
        global CURVE_NAME_INDEX
        CURVE_NAME_INDEX += 1
        return BlenderCurve.from_name('quick curve #{}'.format(CURVE_NAME_INDEX))

    def clear(self):
        self.curve.splines.clear() #type: ignore

    def write_loop(self, points: Sequence[wb.Point]):
        self.curve.dimensions = '3D'
        self.curve.resolution_u = 1
        spline = self.curve.splines.new('POLY') #type: ignore
        spline.use_cyclic_u = True
        spline.points.add(len(points)-1)
        for spline_point, p in zip(spline.points, points):
            spline_point.co = (*p.pos(), 1)

    def write_line(self, points: Sequence[wb.Point]):
        self.curve.dimensions = '3D'
        self.curve.resolution_u = 1
        spline = self.curve.splines.new('POLY') #type: ignore
        spline.use_cyclic_u = False
        spline.points.add(len(points)-1)
        for spline_point, p in zip(spline.points, points):
            spline_point.co = (*p.pos(), 1)

    def write_hole(self, hole: wb.Hole):
        self.write_loop(hole.points())

    def read(self) -> list[wb.Hole]:
        splines = self.curve.splines
        holes = [
            wb.Hole([
                wb.Point(*y.co.xyz) #type: ignore
                for y in spline.points
            ])
            for spline in splines
        ]
        return holes


class BlenderOBJ:
    def __init__(self, obj: blender.Object):
        self.obj = obj

    @staticmethod
    def new(name, data) -> 'BlenderOBJ':
        obj = bpy.data.objects.new(name, data) #type: ignore
        bpy.context.scene.collection.objects.link(obj) #type: ignore
        return BlenderOBJ(obj)

    @staticmethod
    def assign(name, data) -> 'BlenderOBJ':
        obj = bpy.data.objects.get(name)
        if obj is None:
            return BlenderOBJ.new(name, data)
        obj.data = data
        return BlenderOBJ(obj)

    @staticmethod
    def from_mesh(mesh: wb.SimpleMesh, name: str) -> 'BlenderOBJ':
        data = BlenderMesh.quick_new()
        data.clear()
        data.write(mesh)
        return BlenderOBJ.assign(name, data.mesh)

    @staticmethod
    def from_holes(holes: list[wb.Hole], name: str) -> 'BlenderOBJ':
        curve = BlenderCurve.quick_new()
        curve.clear()
        for h in holes:
            curve.write_hole(h)
        return BlenderOBJ.assign(name, curve.curve)

    @staticmethod
    def from_holemeta(meta: wb.HoleMeta, name: str) -> 'BlenderOBJ':
        curve = BlenderCurve.quick_new()
        curve.clear()
        for i in range(meta.len()):
            curve.write_line(meta.side(i).all_points())
        return BlenderOBJ.assign(name, curve.curve)

    @staticmethod
    def from_holemetafloat(meta: wb.HoleMetaFloat, name: str) -> 'BlenderOBJ':
        curve = BlenderCurve.quick_new()
        curve.clear()
        for i in range(meta.len()):
            side: wb.SideFloat = meta.side(i) #type: ignore
            points = []
            if inside := side.round_in():
                points = inside.all_points()
            point_start = side.point_start()
            if len(points) == 0 or points[0] != point_start:
                points = [point_start] + points
            point_end = side.point_end()
            if len(points) == 0 or points[-1] != point_end:
                points += [point_end]
            curve.write_line(points)
        return BlenderOBJ.assign(name, curve.curve)
    @staticmethod
    def from_sidemap(sidemap: wb.SideMap, name: str) -> 'BlenderOBJ':
        curve = BlenderCurve.quick_new()
        curve.clear()
        for side in sidemap.sides():
            curve.write_line(side.all_points())
        return BlenderOBJ.assign(name, curve.curve)
    @staticmethod
    def from_touchinggrophs(graphs: list[wb.TouchingGraph], name: str) -> 'BlenderOBJ':
        curve = BlenderCurve.quick_new()
        curve.clear()
        for graph in graphs:
            holes = graph.holes()
            centers_points = []
            for hole in holes:
                x,y,z = 0.0,0.0,0.0
                for [x2,y2,z2] in [p.pos() for p in hole.points()]:
                    x += x2
                    y += y2
                    z += z2
                length = hole.length()
                centers_points += [
                    wb.Point(
                        x/length,
                        y/length,
                        z/length
                    )
                ]
            data = graph.sidemap()
            for holei in range(len(holes)):
                for index in data[holei].values():
                    otheri = index.hole_index
                    curve.write_line([centers_points[holei], centers_points[otheri]])
        return BlenderOBJ.assign(name, curve.curve)




DEBUG = LogFile.from_name('WorldBuilder Debug')
DEBUG.clear()
MESH_NAME_INDEX = 0
CURVE_NAME_INDEX = 0


def blender_curve_to_holes(obj_name) -> list[wb.Hole]:
    splines = bpy.data.objects[obj_name].data.splines #type: ignore
    holes = [
        wb.Hole([
            wb.Point(*y.co.xyz)
            for y in spline.points
        ])
        for spline in splines
    ]
    return holes



def test():
    global DEBUG
    base = BlenderMesh.from_name("base mesh").read()
    BlenderOBJ.from_mesh(base, "base remesh")
    # BlenderOBJ.from_holes(base.holes(), "base holes")

    # base_touching = [
    #     wb.TouchingGraph(layer.holes())
    #     for layer in base.layers
    # ]
    # base_touching_holes = [
    #     wb.Hole(hole_points)
    #     for touching in base_touching
    #     for hole_points in touching.exposed_points()
    # ]
    # base_touching_touching = wb.TouchingGraph(base_touching_holes)
    # base_touching_touching_holes = [
    #     wb.Hole(hole_points)
    #     for hole_points in base_touching_touching.exposed_points()
    # ]

    # BlenderOBJ.from_touchinggrophs(base_touching, "base layers lines")
    # BlenderOBJ.from_touchinggrophs([wb.TouchingGraph(base.holes()),], "base touching lines")
    # BlenderOBJ.from_holes(base_touching_holes, "base touching outline")
    # BlenderOBJ.from_holes(base_touching_touching_holes, "base outline")
