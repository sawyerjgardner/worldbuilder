{{ "{}".format(name) | escape | underline}}


.. currentmodule:: {{ module }}

.. autoclass:: {{ objname }}
   {% block attributes %}
   {% if attributes %}
   {% for item in attributes %}
   .. autoattribute:: {{ item }}
   {%- endfor %}
   {% endif %}
   {% endblock %}

   {% block methods %}
   {% if methods %}
   .. rubric:: {{ _('Methods') }}
   {% for item in methods %}
   .. automethod:: {{ item }}
   {%- endfor %}
   {% endif %}
   {% endblock %}
