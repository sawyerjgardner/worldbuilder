.. WorldBuilder documentation master file, created by
   sphinx-quickstart on Sun Aug  6 18:15:05 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to WorldBuilder's documentation!
========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Foo
===

.. autosummary::
   :toctree: apidoc
   :template: custom-module-template.rst
   :recursive:

   WorldBuilder

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
